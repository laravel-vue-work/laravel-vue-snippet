<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Site;
use App\Models\User;
use App\Models\Asset;
use App\Models\Module;
use App\Models\SubSite;
use App\Models\Category;
use App\Models\Inspection;
use App\Models\ModuleField;
use App\Models\SubSiteUser;
use App\Models\Company;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\AssetActivity;
use App\Models\AssetDocument;
use App\Models\FieldInputType;
use App\Http\Traits\GeneralTrait;
use App\Mail\AssetReportIssueMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\SubSiteUserPermission;
use App\Models\CategoryModuleFieldValue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Database\Eloquent\Builder;



class SitePanelController extends Controller
{
    use GeneralTrait;
    /**
     * Method to return the max file size limit set in the php.ini file for file size validation at frontend.
     */
    public function fileSizeLimit()
    {
        $maxFileSize = ini_get('upload_max_filesize');
        $maxFileSize = str_replace('M', '', $maxFileSize);
        $maxFileSize = $maxFileSize * 1024 * 1024;
        return $this->returnSuccessMessage(null, $maxFileSize);
    }
    /**
     * function to get vertical sidebar menu options category modulewise
     */
    public function getSitePanelVerticalMenu(Request $request)
    {
        try {
            $ssupCategoryId = SubSiteUserPermission::where('ssup_user_id', $request->userId)->where('ssup_sub_site_id', $request->subSiteId)->distinct()->pluck('ssup_category_id')->toArray();
            $ordSSUPcategoryId = Category::whereIn('category_id', $ssupCategoryId)->where('category_status', 'Y')->orderBy('category_name', 'asc')->pluck('category_id')->toArray();

            $data = [];
            foreach ($ordSSUPcategoryId as $key => $value) {
                $children = [];
                // fetching module category wise
                $modules  = SubSiteUserPermission::where('ssup_user_id', $request->userId)->where('ssup_sub_site_id', $request->subSiteId)->where('ssup_category_id', $value)->distinct()->pluck('ssup_module_id')->toArray();
                $ordModulesId = Module::whereIn('module_id', $modules)->where('module_status', 'Y')->orderBy('module_name', 'asc')->pluck('module_id')->toArray();
                foreach ($ordModulesId as $index => $moduleId) {
                    $module = Module::find($moduleId);
                    if ($module) {
                        $children[] = [
                            'title' => $module->module_name,
                            'route' => [
                                'name'   => 'category-module',
                                'params' => [
                                    'id' => $module->module_id
                                ]
                            ],
                            'meta' => [
                                'module_id' => $module->module_id
                            ]
                        ];
                    }
                }
                $data[] = [
                    'title'     => Category::where('category_id', $value)->first()->category_name,
                    'icon'      => 'LayersIcon',
                    'children'  => $children
                ];
            }
            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    public function getClientPanelCategory(Request $request)
    {

        $ssupCategoryId = SubSiteUserPermission::where('ssup_user_id', $request->user_id)->where('ssup_sub_site_id', $request->sub_site_id)->distinct()->pluck('ssup_category_id')->toArray();
        $categoryList = [];
        foreach ($ssupCategoryId as $key => $value) {
            $categoryExit = Category::where('category_id', $value)->where('category_status', "Y")->exists();
            if($categoryExit){
                $categoryList[] =  array(
                    "value" => Category::where('category_id', $value)->first()->category_id,
                    "label" => Category::where('category_id', $value)->first()->category_name,
                );

            }
        }
        return $categoryList;
    }

    public function getClientPanelModule(Request $request){
        if($request->asset_id && $request->asset_id > 0){
            $exitModule = Asset::find($request->asset_id[0])->asset_module_id;
        }
        $ssupModuleId = SubSiteUserPermission::where('ssup_user_id', $request->user_id)->where('ssup_sub_site_id', $request->sub_site_id)->where('ssup_category_id', $request->category_id)->distinct()->pluck('ssup_module_id')->toArray();
        $moduleList = [];
        foreach ($ssupModuleId as $key => $value) {
            if($value != $exitModule){
                $moduleList[] =  array(
                    "value" => Module::where('module_id', $value)->first()->module_id,
                    "label" => Module::where('module_id', $value)->first()->module_name,
                );
            }

        }
        return $moduleList;
    }

    public function getSitesAndSubSitesList()
    {
        try {
            $siteOptions = $this->getSiteAndSubSiteList(Auth::user()->id)['sites'];
            $subSiteOptions = $this->getSiteAndSubSiteList(Auth::user()->id)['subSites'];

            $response = [
                'sites'          => $siteOptions,
                'subSites'       => $subSiteOptions,
            ];

            return $this->returnSuccessMessage(null, $response);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oops! Something went wrong, Please try again...');
        }
    }
    public function getSitePanelAssets(Request $request)
    {
        try {
            $timeZone = $request->timeZone;
            // Validating the module id : is module assigned to that user of that subsite
            if (!$request->user_id) {
                // Validating the module id : is module assigned to that user of that subsite
                $ssup = SubSiteUserPermission::where('ssup_user_id', Auth::user()->id)->where('ssup_sub_site_id', $request->sub_site_id)->where('ssup_module_id', $request->module_id)->exists();
                if (!$ssup) {
                    return $this->returnError(403, 'Access Forbidden');
                }
            }
            $expiryMFids = $this->getExpiryMFids();

            $data = [];
            $perPage  = $request->perPage;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'asset_id';
            $sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status   = $request->status;
            $moduleId = $request->module_id;

            if($request->user_id){
                $assets = Asset::select('assets.asset_id', 'assets.asset_uid', 'assets.asset_image', 'assets.asset_latitude', 'assets.asset_longitude', 'assets.created_at')->with('values')->where('assets.asset_module_id', $request->module_id)->where('assets.asset_sub_site_id', $request->sub_site_id)->where(function($query) use ($request){
                    $query->where('assets.asset_user_id', $request->user_id);
                });
            } else {
                $assets = Asset::select('assets.asset_id', 'assets.asset_uid', 'assets.asset_image', 'assets.asset_latitude', 'assets.asset_longitude', 'assets.created_at')
                            ->with('values')
                            ->where('assets.asset_module_id', $request->module_id)
                            ->where('assets.asset_sub_site_id', $request->sub_site_id);
            }

            $assets = $assets->with('inspections', function($query){
                $query->latest();
            });

            $allowDuplicate = Module::find($request->module_id)->module_allow_duplicates == 'Y' ? true : false;

            if ($search) {
                $assets = $assets->where(function ($masterQuery) use ($search) {
                    $masterQuery->orWhere('assets.asset_uid', 'like', '%' . $search . '%');
                    $masterQuery->orWhereHas('values', function (Builder $query) use ($search) {
                        $query->where('cmfv_value_text', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_mediumtext', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_longtext', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_int', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_double', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_decimal', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_char', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_varchar', 'like', '%' . $search . '%');
                    });
                });
            }
            $assets = $assets->orderBy($sortBy, $sortDesc)->paginate($perPage);

            $pagination = [
                "total"        => $assets->total(),
                "current_page" => $assets->currentPage(),
                "last_page"    => $assets->lastPage(),
                "from"         => $assets->firstItem(),
                "to"           => $assets->lastItem()
            ];

            for ($key = 0; $key < count($assets); $key++) {
                $value = $assets[$key];
                $data[$key] = [];
                $values     = CategoryModuleFieldValue::where('cmfv_asset_id', $value->asset_id)->get();
                $data[$key]['asset_id']        = $value->asset_id;
                $data[$key]['asset_uid']       = $value->asset_uid;
                $data[$key]['asset_image']     = $value->asset_image ? $this->getStorageURL($value->asset_image) : null;
                $data[$key]['asset_latitude']  = $value->asset_latitude;
                $data[$key]['asset_longitude'] = $value->asset_longitude;
                $data[$key]['allow_duplicate'] = $allowDuplicate;
                $data[$key]['created_at']      = $this->getFormattedDate($this->convertTimeZone('UTC', $timeZone, $value->created_at));

                $status = 'Inspection Pending';
                if ($value->inspections->count() > 0) {
                    if ($value->inspections[0]->inspection_result == 'pass') {
                        $status = 'Pass';
                    } elseif ($value->inspections[0]->inspection_result == 'fail') {
                        $status = 'Fail';
                    } elseif ($value->inspections[0]->inspection_result == 'maintenance_required') {
                        $status = 'Maintenance Required';
                    }
                }
                $data[$key]['status'] = $status;

                $expiryDate = '';
                foreach ($values as $index => $val) {
                    // Expiry Asset Logic
                    if (in_array($val->cmfv_mf_id, $expiryMFids)) {
                        $expiryDate = $val->cmfv_value_date ? $this->getFormattedDate($val->cmfv_value_date) : '';
                    }
                    $data[$key]['expiry'] = $expiryDate;

                    if($val->moduleField == null) {
                        continue;
                    }
                    $fieldInput = FieldInputType::find(ModuleField::find($val->cmfv_mf_id)->mf_input_field_type_id);
                    $var  =  $this->getCMFVcolumnName($val->cmfv_mf_id);
                    if ($fieldInput->field_input_type_name == 'Color') {
                        $data[$key]['color'] = $val->$var;
                    } elseif ($fieldInput->field_input_type_name == 'Map') {
                        $data[$key]['latitude'] = $value->asset_latitude;
                        $data[$key]['longitude'] = $value->asset_longitude;
                    } elseif ($fieldInput->field_input_type_name == 'Time') {
                        $data[$key][$this->getSLUG(ModuleField::find($val->cmfv_mf_id)->mf_name)] = $val->$var ? $this->getFormattedTime($this->convertTimeZone('UTC', $timeZone, $val->$var)) : '';
                    } elseif ($fieldInput->field_input_type_name == 'Date') {
                        $data[$key][$this->getSLUG(ModuleField::find($val->cmfv_mf_id)->mf_name)] = $val->$var ? $this->getFormattedDate($val->$var) : '';
                    } elseif ($fieldInput->field_input_type_name == 'Date-Time') {
                        $data[$key][$this->getSLUG(ModuleField::find($val->cmfv_mf_id)->mf_name)] = $val->$var ? $this->getFormattedDateTime($this->convertTimeZone('UTC', $timeZone, $val->$var)) : '';
                    } elseif ($fieldInput->field_input_type_name == 'Boolean') {
                        $data[$key][$this->getSLUG(ModuleField::find($val->cmfv_mf_id)->mf_name)] = $val->$var == 'Y' ? 'Yes' : 'No';
                    } else {
                        $data[$key][$this->getSLUG(ModuleField::find($val->cmfv_mf_id)->mf_name)] = $val->$var;
                    }
                }
                $assets[$key] = $data[$key];
            }
            $response = [
                'assets'     => $assets,
                'pagination' => $pagination,
                'total'      => $assets->total()
            ];
            return $this->returnSuccessMessage(null, $response);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    public function getAllSubSites(Request $request){
        if(!$request['user_id'] && $request['site_id']){
            $subSites = SubSiteUser::select('ssu_sub_site_id')->where('ssu_site_id', $request['site_id'])->where('ssu_user_id', Auth::user()->id)->distinct()->pluck('ssu_sub_site_id')->toArray();
            //print_r($subSites);
            $subSiteOptions = [];
            foreach ($subSites as $key => $value) {
                $subsite = SubSite::find($value);
                if($subsite->sub_site_status == 'Y') {
                    $subSiteOptions[$key]['site']  = $subsite->site_id;
                    $subSiteOptions[$key]['label'] = $subsite->sub_site_name;
                    $subSiteOptions[$key]['value'] = $value;
                }
            }
            return ['subSites' => $subSiteOptions];
        }else if($request['site_id'] && $request['user_id']){
            $subSites = SubSiteUser::select('ssu_sub_site_id')->where('ssu_site_id', $request['site_id'])->where('ssu_user_id', $request['user_id'])->distinct()->pluck('ssu_sub_site_id')->toArray();
            //print_r($subSites);
            $subSiteOptions = [];
            foreach ($subSites as $key => $value) {
                $subsite = SubSite::find($value);
                if($subsite->sub_site_status == 'Y') {
                    $subSiteOptions[$key]['site']  = $subsite->site_id;
                    $subSiteOptions[$key]['label'] = $subsite->sub_site_name;
                    $subSiteOptions[$key]['value'] = $value;
                }
            }
            return ['subSites' => $subSiteOptions];

        }
    }


    /**
     * function to return boolean on the module has image
     */
    public function moduleInfo($modId)
    {
        try {
            $module = Module::leftjoin('category', 'modules.module_category_id', 'category.category_id')
                ->where('modules.module_id', $modId)
                ->first(['modules.*', 'category.category_name']);

            $data = [
                'module_has_image'       => $module->module_has_image == 'Y' ? true : false,
                'module_allow_duplicate' => $module->module_allow_duplicates == 'Y' ? true : false,
                'categoryName'           => $module->category_name,
                'moduleName'             => $module->module_name
            ];

            return $this->returnData('data', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request  $request
     * @return JsonResponse : SuccessMessage
     */
    public function storeAsset(Request $request)
    {
        try {

            $timeZone = $request->timeZone;

            $uid = $this->generateAssetUID($request->module_id);
            $site_id = SubSite::find($request->sub_site_id)->site_id;
            $asset = new Asset();
            $asset->asset_nfc_uuid    = Str::uuid()->toString();
            $asset->asset_uid         = $uid;
            $asset->asset_module_id   = $request->module_id;
            $asset->asset_site_id     = $site_id;
            $asset->asset_sub_site_id = $request->sub_site_id;
            $asset->asset_user_id     = $request->user_id ?? Auth::user()->id;
            $asset->created_by     =  $request->created_by ?? Auth::user()->id;
            $asset->company_id     =  $request->company_id ?? Auth::user()->company_id;

            if($request->has('image')) {
                $asset->asset_image = empty($request->image)|| $request->image=='null' ? null : $this->fileUpload($request->image, 'asset_image');
            }
            $asset->save();
            $requestValues = json_decode($request->values);
            // loop to get all keys values of request
            foreach ($requestValues as $key => $value) {
                $var  = $this->getCMFVcolumnName($value->mf_id);
                $cmfv = new CategoryModuleFieldValue();
                $cmfv->cmfv_asset_id = $asset->asset_id;
                $cmfv->cmfv_mf_id    = $value->mf_id;
                if (!is_null($value->val)) {
                    if($var == 'cmfv_value_time' || $var == 'cmfv_value_datetime') {
                        // Call convertTimeZone() function and convert the time
                        $value->val = $this->convertTimeZone($timeZone, 'UTC', $value->val);
                    }
                    $cmfv->$var          = $value->val;
                }
                $cmfv->save();
            }
            return $this->returnSuccessMessage('Asset created successfully');
        } catch(\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    public function generateAssetUID($modId)
    {
        $module = Module::find($modId)->module_name;
        $module = strtoupper(substr($module, 0, 3));
        $assetCount = Asset::where('asset_module_id', $modId)->withTrashed()->count();
        $assetCount = $assetCount + 1;
        $assetCount = str_pad($assetCount, 5, '0', STR_PAD_LEFT);
        return $module . " " . $assetCount;
    }
    /**
     * Below function is intended to duplicate the assets records
     */
    public function duplicateAsset(Request $request)
    {
        //try {
        $asset = Asset::find($request->assetId);

        // Generate Asset UID
        $module_id = $asset->asset_module_id;
        $newUID = $this->generateAssetUID($module_id);

        // Replicate Asset
        $newAsset = Asset::find($request->assetId)->replicate();
        $newAsset->asset_nfc_uuid = Str::uuid()->toString();
        $newAsset->asset_uid = $newUID;
        // Updating the user id with the current login user id & duplicate_from with the asset id
        $newAsset->asset_user_id = $request->user_id ?? Auth::user()->id;
        $newAsset->created_by = Auth::user()->id;
        $newAsset->asset_duplicate_from = $request->assetId;
        // Update created_at and updated_at
        $newAsset->created_at = Carbon::now();
        $newAsset->updated_at = Carbon::now();
        $newAsset->save();

        // Replicate Asset Values
        $assetValues = $asset->values;
        foreach ($assetValues as $key => $value) {
            $newAssetValue = $value->replicate();
            $newAssetValue->cmfv_asset_id = $newAsset->asset_id;
            // Update created_at and updated_at
            $newAssetValue->created_at = Carbon::now();
            $newAssetValue->updated_at = Carbon::now();
            $newAssetValue->save();
        }

        // Replicate Asset Documents
        $assetDocuments = $asset->documents;
        foreach ($assetDocuments as $key => $value) {
            $newAssetDocument = $value->replicate();
            $newAssetDocument->ad_asset_id = $newAsset->asset_id;
            // Update created_at and updated_at
            $newAssetDocument->created_at = Carbon::now();
            $newAssetDocument->updated_at = Carbon::now();
            $newAssetDocument->save();
        }
        return $this->returnSuccessMessage('Asset duplicated successfully');
        // } catch(\Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }

    public function moveAsset(Request $request) {
        try {

            if($request->selected > 0){
                $userId = $request->user_id ?? Auth::user()->id;
                //New Value Activity
                $newValue = 'Location -> '.Site::find($request->location)->site_name.' Area -> '.SubSite::find($request->area)->sub_site_name.' Category -> '.Category::find($request->category)->category_name.' Module -> '.Module::find($request->module)->module_name;

                foreach ($request->selected as $key => $value) {
                    // Generate Asset UID
                    $module_id[$key] = $request->module;
                    $newUID[$key] = $this->generateAssetUID($module_id[$key]);

                    //Old Value Activity
                    $oldAssetValue = Asset::where('asset_id', $value)->first(['asset_id','asset_site_id', 'asset_sub_site_id', 'asset_module_id']);
                    $oldValue = 'Location -> '.Site::find($oldAssetValue->asset_site_id)->site_name.' Area -> '.SubSite::find($oldAssetValue->asset_sub_site_id)->sub_site_name.' Category -> '.$oldAssetValue->module->category->category_name.' Module -> '.Module::find($oldAssetValue->asset_module_id)->module_name;

                    // Replicate Asset
                    $newAsset = Asset::find($value)->replicate();
                    //$newAsset->asset_nfc_uuid = Str::uuid()->toString();
                    $newAsset->asset_uid = $newUID[$key];
                    // Updating the user id with the current login user id & duplicate_from with the asset id
                    $newAsset->asset_user_id = $userId;
                    $newAsset->created_by = $userId;
                    $newAsset->asset_duplicate_from = NULL;

                    // Update asset_site_id, asset_sub_site_id, asset_module_id
                    $newAsset->asset_site_id = $request->location;
                    $newAsset->asset_sub_site_id = $request->area;
                    $newAsset->asset_module_id = $request->module;

                    // Update created_at and updated_at
                    $newAsset->created_at = Carbon::now();
                    $newAsset->updated_at = Carbon::now();
                    $newAsset->save();

                    // Update Asset Document
                    AssetDocument::where('ad_asset_id', $oldAssetValue->asset_id)->update(['ad_asset_id' => $newAsset->asset_id]);

                    // Update Asset Activity
                    AssetActivity::where('aa_asset_id', $oldAssetValue->asset_id)->update(['aa_asset_id' => $newAsset->asset_id]);

                    // Update Asset Inspection
                    Inspection::where('inspection_asset_id', $oldAssetValue->asset_id)->update(['inspection_asset_id' => $newAsset->asset_id]);

                    $activity   = new AssetActivity();
                    $activity->aa_asset_id   = $newAsset->asset_id;
                    $activity->aa_cmfv_id    = null;
                    $activity->aa_type       = 'move_to';
                    $activity->aa_old_value  = $oldValue;
                    $activity->aa_new_value  = $newValue;
                    $activity->aa_updated_by = $userId;
                    $activity->save();

                    // Delete old asset
                    $oldAssetValue->delete();
                }
            }

            return $this->returnSuccessMessage('Asset Move successfully');
        } catch(\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    public function getCMFVcolumnName($mfId)
    {
        $type = FieldInputType::find(ModuleField::find($mfId)->mf_input_field_type_id)->field_input_type;
        $var  = 'cmfv_value_' . $type;
        return $var;
    }

    /**
     * function to delete asset document (multiple possible in array)
     * @param Request $request : $assetDocumentIdArray
     * @return JsonResponse : SuccessMessage
     */
    public function deleteBatchAssets(Request $request)
    {
        try {
            /**
             * to delete an asset, we have to follow below procedures...
             * 1. delete asset document
             * 2. delete asset inspection
             * 3. delete asset activity
             * 4. delete asset values
             * 5. delete asset
             */
            $assetIdArray = json_decode($request->assetIdArray);
            foreach ($assetIdArray as $assetId) {
                $asset = Asset::find($assetId);

                // 1. deleting asset documents
                $assetDocuments = AssetDocument::where('ad_asset_id', $assetId)->get();
                foreach ($assetDocuments as $assetDocument) {
                    // $this->deleteFile($assetDocument->ad_file);          // TODO
                    $assetDocument->delete();
                }

                // 2. deleting asset inspection
                $assetInspections = Inspection::where('inspection_asset_id', $assetId)->get();
                foreach ($assetInspections as $assetInspection) {
                    $assetInspection->delete();
                }

                // 3. deleting asset activity
                $assetActivity = AssetActivity::where('aa_asset_id', $assetId)->get();
                foreach ($assetActivity as $activity) {
                    $activity->delete();
                }

                // 4. deleting asset values
                $assetValues = CategoryModuleFieldValue::where('cmfv_asset_id', $assetId)->get();
                foreach ($assetValues as $assetValue) {
                    $assetValue->delete();
                }

                // 5. deleting asset
                $asset->delete();
            }

            $message = '';
            if (count($assetIdArray) == 1) {
                $message = 'Asset deleted successfully';
            } else {
                $message = 'Assets deleted successfully';
            }
            return $this->returnSuccessMessage($message);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to get asset documents list
     * @param Request $request
     * @return JsonResponse : Documents JSON
     */
    public function getAssetDocuments(Request $request)
    {
        try {

            $data = [];
            $perPage  = $request->perPage;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'ad_id';
            if ($sortBy == 'raised') {
                $sortBy = 'created_at';
            }
            $sortDesc = ($request->sortDesc == 'true' || $request->sortDesc == true) ? 'desc' : 'asc';
            $status   = $request->status;
            $documents = AssetDocument::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as ad_date"), DB::raw("TIME_FORMAT(created_at, '%H:%i') as ad_time"))->where('ad_asset_id', $request->asset_id);

            if ($search) {
                $documents = $documents->where(function ($query) use ($search) {
                    $query->where('ad_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('ad_type', 'LIKE', '%' . $search . '%')
                        ->orWhere('ad_size', 'LIKE', '%' . $search . '%')
                        ->orWhere('ad_link', 'LIKE', '%' . $search . '%');
                });
            }

            $documents = $documents->orderBy($sortBy, $sortDesc)->paginate($perPage);
            foreach($documents as $key=>$doc) {
                $documents[$key]->ad_date = $this->getFormattedDate($this->convertTimeZone('UTC', $request->timeZone, $documents[$key]->created_at));
                $documents[$key]->ad_time = $this->getFormattedTime($this->convertTimeZone('UTC', $request->timeZone, $documents[$key]->created_at));
            }
            $pagination = [
                "total"        => $documents->total(),
                "current_page" => $documents->currentPage(),
                "last_page"    => $documents->lastPage(),
                "from"         => $documents->firstItem(),
                "to"           => $documents->lastItem()
            ];
            $data = [
                'documents'  => $documents,
                'pagination' => $pagination,
                'total'      => $documents->total()
            ];
            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to get asset details
     * @param $assetId
     * @return JsonResponse : AssetDetail
     */
    public function getAssetDetails(Request $request, $assetId, $flag = 0)
    {
        try {
            $timeZone = $request->timeZone;
            $asset    = Asset::find($assetId);
            $moduleId = $asset->asset_module_id;
            // Call function to add newly added fields in asset values
            $this->addNewFieldsInAssetValues($moduleId, $assetId);
            $inspectionScheduleRGBY =  Module::find($moduleId)->module_inspection_schedule_rgby;
            $values   = CategoryModuleFieldValue::where('cmfv_asset_id', $assetId)->get();
            $data = [];
            $data = [
                'asset_id'           => $assetId,
                'hasActivity'        => Module                                                         :: find($moduleId)->module_has_activity == 'Y' ? true: false,
                'image'              => $asset->asset_image ? $this->getStorageURL($asset->asset_image): null,
                'latitude'           => $asset->asset_latitude,
                'longitude'          => $asset->asset_longitude,
                'inspectionSchedule' => $inspectionScheduleRGBY == 'Y' ? 'RGBY' : Module::find($moduleId)->module_inspection_schedule,
            ];
            foreach ($values as $index => $val) {
                $fieldInput = FieldInputType::find(ModuleField::find($val->cmfv_mf_id)->mf_input_field_type_id);
                // $type = $fieldInput->field_input_type;
                $var  = $this->getCMFVcolumnName($val->cmfv_mf_id);

                $assetVarValue = null;

                if ($fieldInput->field_input_type_name == 'Image') {
                    $assetVarValue = $val->$var ? $this->getStorageURL($val->$var) : null;
                } elseif ($fieldInput->field_input_type_name == 'Boolean') {
                    $assetVarValue = $val->$var == 'Y' ? true : false;
                } elseif ($fieldInput->field_input_type_name == 'Time') {
                    $assetVarValue = $val->$var ? $this->getFormattedTime($this->convertTimeZone('UTC', $timeZone, $val->$var)) : '';
                } elseif ($fieldInput->field_input_type_name == 'Date-Time') {
                    $assetVarValue = $val->$var ? $this->convertTimeZone('UTC', $timeZone, $val->$var) : '';
                } else {
                    $assetVarValue = $val->$var;
                }

                $assetValue = [
                    'cmfv_id'   =>  $val->cmfv_id,
                    'var'       =>  $var,
                    'value'     =>  $assetVarValue,
                ];

                $data[$this->getSLUG(ModuleField::find($val->cmfv_mf_id)->mf_name)] = $assetValue;
            }
            if ($flag == 1) {
                return $data;
            }

            // $data = array('name' => 'Bhautik','last_name' => 'Ajmera');
            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    /**
     * function to add newly added form fields to values table
     * @param $moduleId, $assetId
     * @return null
     */
    public function addNewFieldsInAssetValues($moduleId, $assetId)
    {
        $moduleFields = ModuleField::where('mf_module_id', $moduleId)->get();
        foreach ($moduleFields as $index => $field) {
            $fieldInput = FieldInputType::find($field->mf_input_field_type_id);
            $var = $this->getCMFVcolumnName($field->mf_id);
            $value = CategoryModuleFieldValue::where('cmfv_asset_id', $assetId)->where('cmfv_mf_id', $field->mf_id)->first();
            if (!$value) {
                $value = new CategoryModuleFieldValue();
                $value->cmfv_asset_id = $assetId;
                $value->cmfv_mf_id = $field->mf_id;
                if ($fieldInput->field_input_type_name == 'Image') {
                    $value->$var = null;
                } elseif ($fieldInput->field_input_type_name == 'Boolean') {
                    $value->$var = 'N';
                } elseif ($fieldInput->field_input_type_name == 'Numeric - Integer' || $fieldInput->field_input_type_name == 'Numeric - Double' || $fieldInput->field_input_type_name == 'Percentage') {
                    $value->$var = 0;
                } else {
                    $value->$var = null;
                }
                $value->save();
            }
        }
    }

    /**
     * function to save asset document
     * @param Request $request
     * @return JsonResponse : SuccessMessage
     */
    public function saveAssetDocument(Request $request)
    {
        // try {
        // $validator = Validator::make($request->all(), [
        //     'ad_file' =>  "max:5|mimes:jpg,png,jpeg,pdf,txt,doc,docx,mp4",
        // ]);
        // if ($validator->fails()) {
        //     // get all errors as single string
        // }
        $msg = '';
        if ($request->ad_id == 0) {
            $assetDocument = new AssetDocument();
            $assetDocument->ad_asset_id = $request->ad_asset_id;
            $assetDocument->ad_name     = $request->ad_name;
            $assetDocument->ad_type     = $request->ad_type == 'null' ? null : $request->ad_type;
            if ($request->ad_size) {
                $assetDocument->ad_size     = $request->ad_size;
            }
            $assetDocument->ad_link     = $request->ad_link == 'null' ? null : $request->ad_link;
            $assetDocument->ad_file     = empty($request->ad_file) || $request->ad_file == 'null' ? null : $this->fileUpload($request->ad_file, 'asset_document');
            $assetDocument->save();
            $msg = 'Asset document created successfully';
        } else {
            $assetDocument = AssetDocument::find($request->ad_id);
            $assetDocument->ad_asset_id = $request->ad_asset_id;
            $assetDocument->ad_name     = $request->ad_name;
            $assetDocument->ad_type     = $request->ad_type == 'null' ? null : $request->ad_type;
            if ($request->ad_size) {
                $assetDocument->ad_size     = $request->ad_size;
            }
            $assetDocument->ad_link     = $request->ad_link == 'null' ? null : $request->ad_link;
            if ($request->file_changed == 'true') {
                // unlink previous and add new
                $this->deleteFile($assetDocument->ad_file, 'asset_document');
                $assetDocument->ad_file     = $this->fileUpload($request->ad_file, 'asset_document');
            }
            $assetDocument->save();
            $msg = 'Asset document updated successfully';
        }
        return $this->returnSuccessMessage($msg);
        // } catch(\Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }

    /**
     * function to get asset document details by id
     * @param $assetDocumentId
     * @return JsonResponse : AssetDocument Data
     */
    public function getAssetDocument($assetDocumentId)
    {
        try {
            $assetDocument = AssetDocument::select('*')->where('ad_id', $assetDocumentId)->first();
            return $this->returnSuccessMessage(null, $assetDocument);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to delete asset document (multiple possible in array)
     * @param Request $request : $assetDocumentIdArray
     * @return JsonResponse : SuccessMessage
     */
    public function deleteBatchDocuments(Request $request)
    {
        try {
            $assetDocumentIdArray = json_decode($request->assetDocumentIdArray);
            foreach ($assetDocumentIdArray as $assetDocumentId) {
                $assetDocument = AssetDocument::find($assetDocumentId);
                if ($assetDocument->ad_file) {
                    $this->deleteFile($assetDocument->ad_file, 'asset_document');
                }
                $assetDocument->delete();
            }
            return $this->returnSuccessMessage('Asset document deleted successfully');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to get the module field list for assets CRUD Operation
     */
    public function getFieldsList(Request $request)
    {
        try {
            $fields = ModuleField::where('mf_module_id', $request->moduleId)->where('mf_status', 'Y')->get();
            $fieldsList = [];
            $defaultValue = [];
            foreach ($fields as $field) {
                $slug = $this->getSLUG($field->mf_name);
                $inputField = FieldInputType::find($field->mf_input_field_type_id);
                $type = $this->getHTMLtype($inputField->field_input_type);
                $key = $slug;
                if ($inputField->field_input_type_name == 'Image') {
                    $key = 'image';
                } elseif ($inputField->field_input_type_name == 'Color') {
                    $key = 'color';
                } elseif ($inputField->field_input_type_name == 'Map') {
                    $key = 'map';
                }
                $fieldsList[] = [
                    'id'         => $field->mf_id,
                    'mf_id'      => $field->mf_id,
                    'name'       => $field->mf_name,
                    'slug'       => $slug,
                    'key'        => $key,
                    'type'       => $type,
                    'isImage'    => $inputField->field_input_type_name == 'Image' ? true : false,
                    'isColor'    => $inputField->field_input_type_name == 'Color' ? true : false,
                    'isMap'      => $inputField->field_input_type_name == 'Map' ? true : false,
                    'isRequired' => $field->mf_is_required        == 'Y' ? true : false,
                    'onGrid'     => $field->mf_is_show_on_grid    == 'Y' ? true : false,
                    'onDetail'   => $field->mf_is_show_on_details == 'Y' ? true : false,
                    'isActive'   => $field->mf_status             == 'Y' ? true : false,
                ];
                $defaultValue[$slug] = null;
                if ($type == 'boolean') {
                    $defaultValue[$slug] = true;
                }
            }
            $responseData = [
                'fieldsList'   => $fieldsList,
                'defaultAsset' => $defaultValue
            ];
            return $this->returnSuccessMessage(null, $responseData);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    public function getHTMLtype($type)
    {
        // $fieldTypes = config('constant.fieldsType');
        // foreach ($fieldTypes as $key => $value) {
        //     if(in_array($type, $value)) {
        //         return $key;
        //     }
        // }
        if ($type == 'longtext' || $type == 'mediumtext') return 'textarea';
        if ($type == 'int') return 'number';
        if ($type == 'decimal' || $type == 'double') return 'decimal';
        if ($type == 'date') return 'date';
        if ($type == 'time') return 'time';
        if ($type == 'datetime') return 'datetime';
        if ($type == 'boolean') return 'boolean';

        return 'text';
    }
    public function getSLUG($name)
    {
        return strtolower(str_replace(' ', '_', $name));
    }

    /**
     * function to update the asset details
     */
    public function updateAsset(Request $request)
    {
        try {
            $timeZone = $request->timeZone;
            $requestValues = json_decode($request->values);
            $flag = 0;
            foreach ($requestValues as $key => $value) {
                $cmfv = CategoryModuleFieldValue::find($value->cmfv);
                $var = $value->var;
                if ($cmfv->$var == $value->val) {
                    $flag = $flag + 1;
                } else {
                    $type = null;
                    $field = FieldInputType::find(ModuleField::find($cmfv->cmfv_mf_id)->mf_input_field_type_id)->field_input_type_name;
                    if ($field == 'Image') {
                        $type = 'image';
                    } elseif ($field == 'Color') {
                        $type = 'color';
                    } elseif ($field == 'Boolean') {
                        if ($cmfv->$var == $value->val) {
                            continue;
                        }
                    } elseif ($field == 'Time') {
                        $type = 'time';
                        // Check if the time are equal :->
                        $oldTime = $cmfv->$var;
                        $convertedOldTime = $this->getFormattedTime($this->convertTimeZone('UTC', $timeZone, $oldTime));
                        $newTime = $value->val;
                        if($convertedOldTime == $newTime) {
                            continue;
                        }
                    } elseif ($field == 'Date-Time') {
                        $type = 'datetime';
                        // Check if the date time are equal :->
                        $oldDateTime = $cmfv->$var;
                        $convertedOldDateTime = $this->convertTimeZone('UTC', $timeZone, $oldDateTime);
                        $newDateTime = $value->val;
                        if($convertedOldDateTime == $newDateTime) {
                            continue;
                        }
                    }
                    // update the value here and make an entry in the asset activity table
                    $oldValue   = $cmfv->$var;
                    $newValue   = $value->val;
                    $cmfv->$var = $newValue;

                    // Change the value if it is date or date time
                    if ($field == 'Date') {
                        $oldValue = $oldValue ? $this->getFormattedDate($oldValue) : '';
                        $newValue = $newValue ? $this->getFormattedDate($newValue) : '';
                    }
                    elseif ($field == 'Date-Time') {
                        $oldValue = $oldValue ? $this->convertTimeZone($timeZone, 'UTC', $oldValue) : '';
                        $newValue = $newValue ? $this->convertTimeZone($timeZone, 'UTC', $newValue) : '';
                        $cmfv->$var = $newValue;
                    } elseif ($field == 'Time') {
                        $oldValue = $oldValue ? $this->convertTimeZone($timeZone, 'UTC', $oldValue) : '';
                        $newValue = $newValue ? $this->convertTimeZone($timeZone, 'UTC', $newValue) : '';
                        $cmfv->$var = $newValue;
                    } elseif ($field == 'Boolean') {
                        $oldValue = $oldValue == 'Y' ? 'Yes' : 'No';
                        $newValue = $newValue == 'Y' ? 'Yes' : 'No';
                    }
                    $this->addAssetActivity($request->asset_id, $value->cmfv, $type, $oldValue, $newValue);
                }
                $cmfv->save();
            }
            // Checking for image input
            if ($request->has('image')) {
                $asset = Asset::find($request->asset_id);
                $oldValue   = $asset->asset_image;
                $asset->asset_image = empty($request->image)|| $request->image == 'null' ? null : $this->fileUpload($request->image, 'asset_image');
                $asset->updated_by = Auth::user()->id;
                $asset->save();
                $newValue   = $asset->asset_image;
                $this->addAssetActivity($request->asset_id, null, 'image', $oldValue, $newValue);
            }else{
                $asset = Asset::find($request->asset_id);
                $asset->updated_by = Auth::user()->id;

                $asset->save();
            }
            // Create request instance with timezone
            $newRequest = new Request();
            $newRequest->request->add(['timeZone' => $timeZone]);

            $assetDetail = $this->getAssetDetails($newRequest, $request->asset_id, 1);
            return $this->returnSuccessMessage('Asset updated successfully', $assetDetail);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to insert arecord to asset activity table with given old and new value
     */
    public function addAssetActivity($assetId, $cmfvId, $type, $oldValue, $newValue)
    {
        try {
            $activity   = new AssetActivity();
            $activity->aa_asset_id   = $assetId;
            $activity->aa_cmfv_id    = $cmfvId;
            $activity->aa_type       = $type;
            $activity->aa_old_value  = $oldValue;
            $activity->aa_new_value  = $newValue;
            $activity->aa_updated_by = Auth::user()->id;
            $activity->save();
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to get the asset activity
     */
    public function getAssetActivity(Request $request)
    {
        try {
            $data = [];

            $timeZone = $request->timeZone;

            $assetActivity = AssetActivity::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as date"), DB::raw("TIME_FORMAT(created_at, '%H:%i') as time"))->with('values.moduleField', 'user')->where('aa_asset_id', $request->asset_id);

            $assetActivity = $assetActivity->orderBy('created_at', 'desc')->get();

            for ($i = 0; $i < count($assetActivity); $i++) {
                $oldValue = $assetActivity[$i]->aa_old_value;
                $newValue = $assetActivity[$i]->aa_new_value;
                $fieldName = "";
                if ($assetActivity[$i]->aa_type == 'image') {
                    $oldValue = $oldValue ? $this->getStorageURL($oldValue) : null;
                    $newValue = $newValue ? $this->getStorageURL($newValue) : null;
                    $fieldName = "Image";
                }else if($assetActivity[$i]->aa_type == 'move_to'){
                    $fieldName = "Move Asset";
                } else if($assetActivity[$i]->aa_type == 'time'){
                    $oldValue = $oldValue ? $this->getFormattedTime($this->convertTimeZone('UTC', $request->timeZone, $oldValue)) : '';
                    $newValue = $newValue ? $this->getFormattedTime($this->convertTimeZone('UTC', $request->timeZone, $newValue)) : '';
                } else if($assetActivity[$i]->aa_type == 'date'){
                    $oldValue = $oldValue ? $this->getFormattedDate($oldValue) : '';
                    $newValue = $newValue ? $this->getFormattedDate($newValue) : '';
                } else if($assetActivity[$i]->aa_type == 'datetime'){
                    $oldValue = $oldValue ? $this->getFormattedDateTime($this->convertTimeZone('UTC', $request->timeZone, $oldValue)) : '';
                    $newValue = $newValue ? $this->getFormattedDateTime($this->convertTimeZone('UTC', $request->timeZone, $newValue)) : '';
                }
                if($assetActivity[$i]->values){
                    $fieldName = $assetActivity[$i]->values->moduleField->mf_name;
                }

                $data[] = [
                    'aa_id'     => $assetActivity[$i]->aa_id,
                    'time'      => $this->getFormattedTime($this->convertTimeZone('UTC', $timeZone, $assetActivity[$i]->time)),
                    'date'      => $this->getFormattedDate($this->convertTimeZone('UTC', $timeZone, $assetActivity[$i]->date)),
                    'user'      => $assetActivity[$i]->user->first_name . " " . $assetActivity[$i]->user->last_name,
                    'field'     => $fieldName,
                    'old_value' => $oldValue,
                    'new_value' => $newValue,
                    'type'      => $assetActivity[$i]->aa_type,
                    'datetime'  => $assetActivity[$i]->created_at
                ];
            }

            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to get the asset activity
     */
    public function getInspection(Request $request)
    {
        try {
            $data = [];
            $perPage  = $request->perPage;
            $search   = $request->search;
            $page     = $request->page;
            if($request->sortBy == 'created_at_format'){
                $sortBy = 'created_at';
            }else if($request->sortBy == 'user_name.full_name'){
                $sortBy = 'created_by';
            }else{
                $sortBy   = isset($request->sortBy) ? $request->sortBy : 'inspection_id';
            }
            $sortDesc = ($request->sortDesc == 'true' || $request->sortDesc == true) ? 'desc' : 'asc';
            $assetInspection = Inspection::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as date"), DB::raw('TIME(created_at) as time'))->with('assetWiseInspection')->with('userName')->where('inspection_asset_id', $request->asset_id)->where('inspection_status', 'Y');
            if ($search) {
                $assetInspection = $assetInspection->where('inspection_note', 'LIKE', '%' . $search . '%')
                    ->orWhere('created_at', 'LIKE', '%' . $search . '%')
                    ->orWhere('inspection_result', 'LIKE', '%' . $search . '%')
                    ->orWhereHas('assetWiseInspection', function ($query) use ($search) {
                        $query->where('asset_uid', 'LIKE', '%' . $search . '%')
                            ->orWhere('created_at', 'LIKE', '%' . $search . '%');
                    });
            }

            $assetInspection = $assetInspection->orderBy($sortBy, $sortDesc)->paginate($perPage);
            foreach ($assetInspection as $key => $inspection) {
                // dd($assetInspection[$key]->date, $assetInspection[$key]->time, $this->getFormattedDate($this->convertTimeZone('UTC', $request->timeZone, $assetInspection[$key]->created_at)), $this->getFormattedTime($this->convertTimeZone('UTC', $request->timeZone, $assetInspection[$key]->created_at)));
                if ($inspection->inspection_image_1) {
                    $assetInspection[$key]->inspection_image_1 = !empty($inspection->inspection_image_1) ? $this->getStorageURL($inspection->inspection_image_1) : '';
                }

                if ($inspection->inspection_image_2) {
                    $assetInspection[$key]->inspection_image_2 = !empty($inspection->inspection_image_2) ? $this->getStorageURL($inspection->inspection_image_2) : '';
                }

                $assetInspection[$key]->date = $this->getFormattedDate($this->convertTimeZone('UTC', $request->timeZone, $assetInspection[$key]->created_at));
                $assetInspection[$key]->time = $this->getFormattedTime($this->convertTimeZone('UTC', $request->timeZone, $assetInspection[$key]->created_at));
            }

            // dd($assetInspection);

            $pagination = [
                "total"        => $assetInspection->total(),
                "current_page" => $assetInspection->currentPage(),
                "last_page"    => $assetInspection->lastPage(),
                "from"         => $assetInspection->firstItem(),
                "to"           => $assetInspection->lastItem()
            ];
            $data = [
                'inspection'   => $assetInspection,
                'pagination' => $pagination,
                'total'      => $assetInspection->total()
            ];
            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to delete asset inspection (multiple possible in array)
     * @param Request $request : $assetDocumentIdArray
     * @return JsonResponse : SuccessMessage
     */
    public function deleteBatchInspection(Request $request)
    {
        try {

            $inspectionIdArray = json_decode($request->inspectionIdArray);
            foreach ($inspectionIdArray as $inspectionId) {
                Inspection::where('inspection_id', $inspectionId)->delete();
            }

            $message = '';
            if (count($inspectionIdArray) == 1) {
                $message = 'Inspection deleted successfully';
            } else {
                $message = 'Inspections deleted successfully';
            }
            return $this->returnSuccessMessage($message);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to get the asset Detail on asset_nfc_uuid
     */
    public function getAssetInfo($uuid)
    {

        try {
            if ($uuid) {
                $assetInfo = [];
                $assetData = Asset::where('asset_nfc_uuid', $uuid)->first();
                if ($assetData) {

                    $siteData = Site::where('site_id', $assetData['asset_site_id'])->first();
                    $subSiteData = SubSite::where('sub_site_id', $assetData['asset_sub_site_id'])->first();
                    $userData = User::where('id', $siteData['created_by'])->first();
                    $assetInspection = Inspection::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as date"), DB::raw('TIME(created_at) as time'))->with('assetWiseInspection')->where('inspection_asset_id', $assetData['asset_id'])->latest()->take(3)->orderBy('created_at', 'DESC')->get();
                    $path = $userData->company_id ? Company::find($userData->company_id)->logo : null;

                    //Array create Display per Design
                    $assetInfo['asset_id'] = $assetData['asset_uid'];
                    $assetInfo['location'] = $siteData['site_name'];
                    $assetInfo['area'] = $subSiteData['sub_site_name'];
                    $assetInfo['asset_image'] = $assetData['asset_image'] ? $this->getStorageURL($assetData['asset_image']) : null;
                    $assetInfo['contact_number'] = $userData['contact_number'];
                    // $assetInfo['logo'] = $userData['client_company_logo'] ? $this->getStorageURL($userData['client_company_logo']) : null;
                    $assetInfo['logo'] = (!is_null($path))?$this->getStorageURL($path):null;
                    $assetInfo['inspection'] = $assetInspection;
                }

                return $this->returnSuccessMessage(null, $assetInfo);
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    public function submitReportIssue(Request $request)
    {
        if ($request->asset_nfc_uuid) {
            $reportImg = "";
            $assetInfo = [];
            $assetData = Asset::where('asset_nfc_uuid', $request->asset_nfc_uuid)->first();
            if ($assetData) {
                // Get company data
                $companyData = Company::find($assetData['company_id']);
                $siteData        = Site::where('site_id', $assetData['asset_site_id'])->first();
                $subSiteData     = SubSite::where('sub_site_id', $assetData['asset_sub_site_id'])->first();

                // Get root admin user data
                $rootAdminUserEmail  = User::select('email','first_name')->where('company_id', $assetData['company_id'])
                                            ->where('is_root', '0')
                                            ->whereHas('roles', function ($query) {
                                                $query->where('name', 'Admin');
                                            })
                                            ->first();

                // Get all admin data from company
                $adminUserEmailArray  = User::where('company_id', $assetData['company_id'])
                                            ->where('is_root', '1')
                                            ->whereHas('roles', function ($query) {
                                                $query->whereIn('name', ['Admin']);
                                            })
                                            ->pluck('email');

                $assetInspection = Inspection::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as date"), DB::raw('TIME(created_at) as time'))->with('assetWiseInspection')->where('inspection_asset_id', $assetData['asset_id'])->latest()->take(3)->orderBy('created_at', 'DESC')->get();

                //Array create Display per Design
                $assetInfo['asset_id']       = $assetData['asset_uid'];
                $assetInfo['location']       = $siteData['site_name'];
                $assetInfo['area']           = $subSiteData['sub_site_name'];
                $assetInfo['asset_image']    = $assetData['asset_image'] ? $this->getStorageURL($assetData['asset_image']) : null;
                $assetInfo['contact_number'] = $companyData['contact_no'];
                $assetInfo['logo']           = $companyData['logo'] ? $this->getStorageURL($companyData['logo']) : null;
                $assetInfo['inspection']     = $assetInspection;

                if ($request->file) {
                    $asset_image = empty($request->file) || $request->file == 'null' ? null : $this->fileUpload($request->file, 'asset_report');
                    $reportImg = $this->getStorageURL($asset_image);
                }

                // Send email to all company admin
                if ($request->asset_nfc_uuid && $request->message) {
                    $mail = new AssetReportIssueMail($rootAdminUserEmail['email'], $assetInfo['asset_id'], $assetInfo['location'], $assetInfo['area'], $request->message, $rootAdminUserEmail['first_name'], $reportImg);
                    Mail::to($rootAdminUserEmail['email'])->cc($adminUserEmailArray)->send($mail);

                    return $this->returnSuccessMessage("Asset Report Submit successfully.");
                }
            }
        }
    }
}
