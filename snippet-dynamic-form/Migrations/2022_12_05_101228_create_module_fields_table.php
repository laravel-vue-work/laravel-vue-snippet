<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_fields', function (Blueprint $table) {
            $table->id('mf_id');
            $table->unsignedBigInteger('mf_module_id');
            $table->unsignedBigInteger('mf_user_id');
            $table->unsignedBigInteger('mf_input_field_type_id');
            $table->string('mf_name', 50);
            $table->text('mf_default_val')->nullable();
            $table->enum('mf_is_show_on_grid', ['Y', 'N'])->default('Y')->comment('Y => Yes, N => No');
            $table->enum('mf_is_show_on_details', ['Y', 'N'])->default('Y')->comment('Y => Yes, N => No');
            $table->enum('mf_is_required', ['Y', 'N'])->default('Y')->comment('Y => Yes, N => No');
            $table->enum('mf_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key
            $table->foreign('mf_module_id')->references('module_id')->on('modules')->onDelete('cascade');
            $table->foreign('mf_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('mf_input_field_type_id')->references('field_input_type_id')->on('field_input_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_fields');
    }
};
