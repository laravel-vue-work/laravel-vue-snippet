<?php

namespace App\Models;

use App\Models\FieldInputType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ModuleField extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'mf_id';
    protected $table      = 'module_fields';

    protected $fillable = [
        'mf_module_id', 'mf_user_id', 'company_id', 'mf_input_field_type_id', 'mf_name', 'mf_default_val', 'mf_is_show_on_grid', 'mf_is_show_on_details','mf_has_expiry_date', 'mf_is_required', 'mf_status', 'created_by', 'updated_by'
    ];

    /*
     *  Get field input type data
     */
    public function fieldInputType()
    {
        return $this->hasOne(FieldInputType::class, 'field_input_type_id', 'mf_input_field_type_id');
    }
}
