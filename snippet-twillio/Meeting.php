<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['id','scheduler_id','operator_id','app_user_id','full_name','meeting_date','start_time','slot_minute','country_code','phone','email','note','default_lang','user_timezone','operator_timezone','status','added_user_type','added_by'];

    /** 
     * get operator
     */
    public function operator()
    {
        return $this->hasOne('App\Managers','id','operator_id'); 
    }

    /** 
     * get user
     */
    public function user()
    {
        return $this->hasOne("App\AppUser","id","app_user_id");
    }

    public function schedule()
    {
        return $this->hasOne("App\Scheduler","id","scheduler_id");
    }
}
