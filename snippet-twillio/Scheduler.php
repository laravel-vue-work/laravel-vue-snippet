<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scheduler extends Model
{
    //
    use SoftDeletes;

    protected $fillable =['id','account_id','operator_id','from_date','start_time','end_time','time_slot','is_repeat','repeat_on_weeks','repeat_count','repeat_on_months','is_end','when_end','added_by'];

    /**
     * get operator
     */
    public function operator()
    {
        return $this->hasOne('App\Managers','id','operator_id'); 
    }

    /**
     * get account name
     */
    public function account() 
    {
        return $this->hasOne('App\Account','id','account_id'); 
    }
}
