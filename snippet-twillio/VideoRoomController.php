<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TwilioRestClient;
use TwilioJwtAccessToken;
use Illuminate\Support\Facades\DB;
use TwilioJwtGrantsVideoGrant;
use Twilio\Rest\Client as Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Auth;
use Mail;
use Carbon\Carbon;
use Validator;
use App\VideoCall;
use App\Managers;
use App\API;
use App\AppUser;
use App\AppUserLiveLocation;
use App\StatusSettings;
use Illuminate\Support\Facades\Crypt;
use Redirect;
use App\ParticipantDetails;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SessionExport;
use App\AppGeneralSetting;
use App\VideoAttachments;
use App\videoAttachmentsStatus;
use App\Country;
use App\MobileContent;
use Cookie;
use File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DataTables;

class VideoRoomController extends Controller {

    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct() {
//        $getLocationData = videocall::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
//                        ->where('operator_id', '=', 2)
//                        ->whereDate('created_at', '=', '2020-04-25')
//                        ->whereNull('deleted_at')
//                        ->groupBy('date','room_sid')
//                        ->orderBy('video_call.id','desc')
//                        ->get();
//
//        echo "<pre>";print_R($getLocationData);exit;
        $this->middleware('auth', ['except' => ['updateAttachmentStatus', 'addVideoAttachment', 'joinRoom', 'getCallDuration', 'getRoomStatus', 'saveRoomCreationTime','setAppUserLiveLocation', 'getLocationData',
                'addParticipant','getVideoCallBack','composVideos','storeVideos','disConnectParticipants','checkConposition','createUpdateRoom','save_snap','userMediaError']]);

        $keys = $this->getThirdPartyDetails();

        if ($keys && !empty($keys)) {
            $this->sid = $keys->twilio_account_sid;
            $this->token = $keys->twilio_account_token;
            $this->key = $keys->twilio_video_api_key;
            $this->secret = $keys->twilio_video_api_secret;
        }
    }

    public function getThirdPartyDetails() {
        $keys = API::get()->first();
        return $keys;
    }


    public function index(Request $request) {
        if($request->ajax()) { 
        if (Auth::user()->type == 'Top Manager') {
            // $sessions = VideoCall::join('app_user', 'video_call.app_user_id', '=', 'app_user.id')
            //         ->groupBy('video_call.room_sid')
            //         ->orderBy('video_call.id', 'desc')
            //         ->get(['video_call.*', 'app_user.name as app_user_name',
            //     'app_user.number as app_user_number']);
            if($request->ajax()) {
                $model =  VideoCall::with(['user','status','operator','account'])->groupBy('room_sid');
            }

            
        } else if (Auth::user()->type == 'manager') {
            $operatorIds = Managers::where('parent_manager_id', Auth::user()->id)->pluck('id')->toArray();
            // $sessions = VideoCall::join('app_user', 'video_call.app_user_id', '=', 'app_user.id')
            //         ->whereIn('video_call.operator_id', $operatorIds)
            //         ->groupBy('video_call.room_sid')
            //         ->orderBy('video_call.id', 'desc')
            //         ->get(['video_call.*', 'app_user.name as app_user_name',
            //     'app_user.number as app_user_number']);
            $model =  VideoCall::with(['user','status','operator','account'])->whereIn('operator_id',$operatorIds)->groupBy('room_sid');
        } else {
            $operatorId = Auth::user()->id;
            // $sessions = VideoCall::join('app_user', 'video_call.app_user_id', '=', 'app_user.id')
            //         ->where('video_call.operator_id', $operatorId)
            //         ->groupBy('video_call.room_sid')
            //         ->orderBy('video_call.id', 'desc')
            //         ->get(['video_call.*', 'app_user.name as app_user_name',
            //     'app_user.number as app_user_number']);
            $model =  VideoCall::with(['user','status','operator','account'])->where('operator_id', $operatorId)->groupBy('room_sid');
        }
        return DataTables::eloquent($model)->addColumn('edit_delete',function(VideoCall $session) {
                    $deleteUrl  = url('delete/session/'.$session->id); 
                    $editUrl = url('edit/session/'.$session->id);
                    return '<button class="btn-icon btn btn-danger mr-2  deleteOnElement" data-route="'.$deleteUrl.'" type="button"><i class="fa fa-trash"></i></button><a href="'.$editUrl.'" class="btn-icon btn btn-info"><i class="fa fa-edit"></i></a>';
                })->addColumn('operator',function(VideoCall $session){
                    if(isset($session->operator) && !empty($session->operator)) {
                        return $session->operator->first_name . " ". $session->operator->last_name;
                    } 
                    return "";
                })->addColumn('user',function(VideoCall $session){
                    if(isset($session->user) && !empty($session->user)) {
                        return $session->user->name;
                    } 
                    return "";
                })->addColumn('user_phone',function(VideoCall $session){
                    if(isset($session->user) && !empty($session->user)) {
                        return $session->user->number;
                    } 
                    return "";
                })->addColumn('status',function(VideoCall $session){
                    if(isset($session->status) && !empty($session->status)) {
                        try {
                            return '<span style="color:'.$session->status->color_code.'">'.$session->status->status_name.'</span>';
                        } catch(\Exception $e)  {
                           //s dd($session);
                        }
                    } 
                    return "";
                })->addColumn('account_name',function(VideoCall $session){
                    if(isset($session->account) && !empty($session->account)) {
                        return $session->account->name;
                    } 
                    return "";
                })->addColumn('duration',function(VideoCall $session){
                    if(isset($session->duration) && !empty($session->duration) ) {
                        return (new \App\Helpers\CommonHelper)->displayDuration(intval($session->duration));
                    } 
                    return "00:00";
                })->addColumn('created_at_text',function($session){
                    return (new \App\Helpers\CommonHelper)->formatDate($session->created_at);
                })->escapeColumns([])
	            ->make(true);
        }
        // foreach ($sessions as $session) {
        //     $manager = Managers::find($session->operator_id);
        //     $statusSettings = StatusSettings::find($session->status_id);
        //     $session->satus_name = ($statusSettings) ? $statusSettings->status_name : '';
        //     $session->color_code = ($statusSettings) ? $statusSettings->color_code : '';
        //     $session->operatorName = ($manager) ? $manager->first_name . ' ' . $manager->last_name : '';
        // }

        $data['page_title'] = 'Video Call';
        $data['link'] = 'Session';
        $data['menu'] = 'Session';
        $data['subMenu'] = 'Session';
        return view('videocall.index', $data);
    }

    public function createRoom(Request $request) {
        try {
            $getAccountData = (new \App\Helpers\CommonHelper)->getAccountData();
            // if(!empty($getAccountData) && !empty($getAccountData->id)) {
            //     //dd($request->cookie('locale'.auth()->user()->id));
            //     $mobile_content = MobileContent::where('module_name', "Join Session SMS Text")->where('account_id',$getAccountData->id)->first();
            //     $dynamicField = "content_text_".$request->cookie('locale'.auth()->user()->id);
            //     if(empty($mobile_content) || !isset($mobile_content[$dynamicField]) || empty($mobile_content[$dynamicField]) ){
            //         \Session::flash('error', __('Mobile content is missing for your account.'));
            //         return redirect()->route('sessionListing');
            //     }
            // } else {
            //     \Session::flash('error', __('Oppps! something went wrong while start new session, please try again ...'));
            //     return redirect()->route('sessionListing');
            // }
            $this->middleware('auth');
            $host = $request->getHttpHost();
            $rules = array(
                'phone_number' => 'required|numeric',
                'name' => 'required',
                'country_code' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->messages();
                return Redirect::back()->withErrors($errors);
            }
            $randomRoomName = 'room_' . str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT) . "_" .base64_encode($request->phone_number);
           
            $client = new Client($this->sid, $this->token);
            $exists = $client->video->rooms->read(['uniqueName' => $randomRoomName, 'status' => 'completed', 'in-progress']);
            
            $recordParticipantsOnConnect = true;
            if($getAccountData){
                $recordParticipantsOnConnect = ($getAccountData->require_rec == 1)?true:false;
            }
            
            if (empty($exists)) {
                $createdRoom = $client->video->rooms->create([
                    'uniqueName' => $randomRoomName,
                    'type' => 'group',
                    'recordParticipantsOnConnect' => $recordParticipantsOnConnect,
                    "type" => 'group-small'
                ]);
                $roomSid = $createdRoom->sid;
                $type = 'operator';

                if($roomSid != ''){
                    $appUserId = $this->registerAppUser($request->name, $request->country_code . $request->phone_number);
                    $this->addCallLog($appUserId, $roomSid,Auth::user()->id,$randomRoomName, $request->app_user_lang,"op_".Auth::user()->id);

                    $joinSessionOption = array();
                    $userEmail = '';
                    if(isset($request->share_option)){
                    	$joinSessionOption = $request->share_option;
                    	$userEmail = $request->userEmail;
                    }
                   // if(isset($host) && $host == 'admin.jastok.com'){
                        $this->sendSMS($request->name, $roomSid, $appUserId, $request->country_code, $request->phone_number,$request->app_user_lang,$joinSessionOption, $userEmail);
                    //}
                    Cookie::forget('locale' . Auth::user()->id);
                    Cookie::queue('locale' . Auth::user()->id, $request->app_user_lang, 43800);
                    return redirect()->action('VideoRoomController@joinRoom', [
                                'type' => $type,
                                'roomSID' => $roomSid,
                                'customerID' => $appUserId
                    ]);

                }
            } else {
                return redirect()->back()->with('duplicate', 'Enter Name is already exists,Please try with different!');
            }
        } catch (\Exception $e) {
            if ($e->getCode() == 21211) {
                \Session::flash('error', __('Phone Number is not valid, Please enter valid Phone Number ...'));
                VideoCall::where('room_sid',$roomSid)->where('app_user_id',$appUserId)->delete();
                AppUser::find($appUserId)->delete();
                return redirect()->route('sessionListing');
            } else {
                \Session::flash('error', __('Oppps! something went wrong while start new session, please try again ...'));
                \Log::info('line number 246'.$e);
                return redirect()->route('sessionListing');
            }
        }
    }

    public function sendSMS($name,$roomSid,$appUserId,$countryCode,$phoneNumber,$app_user_lang = '',$joinSessionOption,$userEmail = ''){
         /*  Message Send code */
        
        try{    
            $videoData = VideoCall::where('room_sid',$roomSid)->first();
            if(!empty($videoData) && (isset($videoData->account_id) && !empty($videoData->account_id)) ) {
                $settings = MobileContent::where('module_name', "Join Session SMS Text")->where('account_id',$videoData->account_id)->first();
                // $dynamicField = "content_text_".request()->cookie('locale'.auth()->user()->id);
                // if(empty($mobile_content) || !isset($mobile_content[$dynamicField]) || empty($mobile_content[$dynamicField]) ){
                //      $settings = MobileContent::where('id',3)->first();
                // }
            } else {
                $settings = MobileContent::where('id',3)->first();
            }
            $language  = \App\Language::where('slug',$videoData->app_user_lang)->first();
            $string = "";
            if(!empty($language)) {
                $sdata = $settings->content->where('language_id',$language->id)->first();
                $string = !empty($sdata) ? $sdata->value : '';
            }

            if(empty($string)) {
                $settings = MobileContent::where('id',3)->first();
                $language  = \App\Language::where('slug',$videoData->app_user_lang)->first();
                $sdata = $settings->content->where('language_id',$language->id)->first();
                $string = !empty($sdata) ? $sdata->value : '';
            }

            // $string = $settings->content_text_english;
            // if (isset($app_user_lang) && $app_user_lang && Auth::user()) {

            //     if ($app_user_lang == "hebrew" && isset($settings->content_text_hebrew) && $settings->content_text_hebrew) {
            //         $string = $settings->content_text_hebrew;
            //     }
            //     if ($app_user_lang == "italian" && isset($settings->content_text_italian) && $settings->content_text_italian) {
            //         $string = $settings->content_text_italian;
            //     }
            //     if ($app_user_lang == "japanese" && isset($settings->content_text_japanese) && $settings->content_text_japanese) {
            //         $string = $settings->content_text_japanese;
            //     }
            //     if ($app_user_lang == "portuguese" && isset($settings->content_text_portuguese) && $settings->content_text_portuguese) {
            //         $string = $settings->content_text_portuguese;
            //     }
            //     if ($app_user_lang == "spanish" && isset($settings->content_text_spanish) && $settings->content_text_spanish) {
            //         $string = $settings->content_text_spanish;
            //     }
            //     if ($app_user_lang == "french" && isset($settings->content_text_french) && $settings->content_text_french) {
            //         $string = $settings->content_text_french;
            //     }
            //     if ($app_user_lang == "arabic" && isset($settings->content_text_arabic) && $settings->content_text_arabic) {
            //         $string = $settings->content_text_arabic;
            //     }
            //     if ($app_user_lang == "hindi" && isset($settings->content_text_hindi) && $settings->content_text_hindi) {
            //         $string = $settings->content_text_hindi;
            //     }
            //     if ($app_user_lang == "german" && isset($settings->content_text_german) && $settings->content_text_german) {
            //         $string = $settings->content_text_german;
            //     }
            //     if ($app_user_lang == "russian" && isset($settings->content_text_russian) && $settings->content_text_russian) {
            //         $string = $settings->content_text_russian;
            //     }
            //     if ($app_user_lang == "czech" && isset($settings->content_text_czech) && $settings->content_text_czech) {
            //         $string = $settings->content_text_czech;
            //     }
            //     if ($app_user_lang == "chinese" && isset($settings->content_text_chinese) && $settings->content_text_chinese) {
            //         $string = $settings->content_text_chinese;
            //     }
            // }
            
            if(empty($string)) return false;
            $originalText = $string;
            $string = strip_tags($string);
            $country_code = str_replace('+', '',$countryCode);
            $countryQuery = Country::query();
            $countryQuery->where('country_code', $country_code);
            $countryDetail = $countryQuery->get()->toArray();

            if (!empty($countryDetail)) {
                if (isset($countryDetail['twilio_from_name_enabled']) && $countryDetail['twilio_from_name_enabled'] && isset($settings->sms_from_name) && $settings->sms_from_name) {
                    $twilio_number = $settings->sms_from_name;
                } else {
                    if($countryCode == "+972"){
                        $twilio_number = "+972528903950";
                    }else if($countryCode == "+1"){
                        $twilio_number = "+18334730912";
                    }else{
                        $twilio_number = "+13475545217";
                    }    
                    //$twilio_number = $countryCode == "+972" ? "+972528903950" : "+13475545217";
                }
            }
            $string = str_replace("#full_name#", $name, $string);
            $url = url('/joinsession/customer/' . $roomSid.'/'.$appUserId);
            $url = (new \App\Helpers\CommonHelper)->get_tiny_url($url);

            $string = str_replace("#url#", $url, $string);

            $account_sid = "AC17445fd074a7db09c686f7aac185a37e";
            $auth_token = "fcb833ee7e007e43bb5046f3d1972e21";

            $client_new = new Client($account_sid, $auth_token);
            $direction = 'ltr';
            try {
                $languageData = \App\Language::where('slug',$app_user_lang)->first(); 
                $direction = !empty($languageData) ? $languageData->direction : 'ltr';
                if(!empty($languageData)) {
                    \App::setLocale($languageData->lang_attr);
                }
            } catch(\Exception $e) {
                
            }

            if(in_array(1, $joinSessionOption)  && $userEmail && in_array(3, $joinSessionOption)){
                $user['email'] = $userEmail;
                $user['name'] = $name;
                $user['url'] = $url;
                $user['subject'] = __("join session");
                $user['body'] = $string;
                $originalText = str_replace("#url#", $url, $originalText);
                $originalText = str_replace("#full_name#", $name, $originalText);
                Mail::send('videocall.email_join_session', ['user' => $user,'text'=> $originalText,'direction'=>$direction], function ($m) use ($user) {
                        //$m->from('noreply@jastok.com', 'Remote Doctor'); //company Email

                        $m->to($user['email'], $user['name'])->subject($user['subject']); // receiver Email
                });
                $messages=$client_new->messages->create($countryCode . $phoneNumber, array(
                    'from' => $twilio_number,
                    'body' => $string,
                    'statusCallback'=>'http://postb.in/1234abcd'
                    )
                );
            }
        	else if(in_array(1, $joinSessionOption) && $userEmail){
        		$user['email'] = $userEmail;
                $user['name'] = $name;
                $user['url'] = $url;
                $user['subject'] = __("join session");
                $user['body'] = $string;
                $originalText = str_replace("#url#", $url, $originalText);
                $originalText = str_replace("#full_name#", $name, $originalText);
                Mail::send('videocall.email_join_session', ['user' => $user,'text'=> $originalText,'direction'=>$direction], function ($m) use ($user) {
                        //$m->from('noreply@jastok.com', 'Remote Doctor'); //company Email

                        $m->to($user['email'], $user['name'])->subject($user['subject']); // receiver Email
                    });
        	}
            else if(in_array(3, $joinSessionOption)){

	            $string = htmlspecialchars_decode($string);
	            $string = html_entity_decode($string);
    	    	$messages=$client_new->messages->create($countryCode . $phoneNumber, array(
    	            'from' => $twilio_number,
    	            'body' => $string,
                    'statusCallback'=>'http://postb.in/1234abcd'
    	            )
    	        );
    	    }
    	    else{
        		$message = $client_new->messages->create("whatsapp:".$countryCode . $phoneNumber, array(
    		        'from' => "whatsapp:+14155238886",
    		        'body' => $string
    		        )
    			);
        	}
        $msg_status=$messages->status;
        if($msg_status=='queued')
        {
            // $videoData->status_id=74;
            $videoData->sms_id=$messages->sid;
            $videoData->msg_status="queued";
    
        }
            $videoData->save();
            $roomInfo['roomSID']=$roomSid;
            $roomInfo['operatorId']=Auth::user()->id;
            // $this->LogIncidents($roomInfo,0,"message sent to user","success");


        }

        catch(\Exception $e)
        {
            // $this->LogIncidents($roomInfo,1,"message sent to user",$e->getMessage());

        }
    }
    public function addParticipant(Request $request){
        $data = $error = array();
        $participantEmail = '';
        if(isset($request->userId) && !empty($request->userId)) {
            $userId= "cu_".$request->userId;
        } else {
            $userId = 0;
        }
        if(!$this->checkMaxParticipant($request->joinRoomId,$userId)) {
            return \Response::json(array(
                'error_max' => true
            ));
        }
        try {
            if($request->addParticipantType != 'appUserSide'){
                $this->middleware('auth');
                $userId= "op_".Auth::user()->id;
            } 
            
            $host = $request->getHttpHost();
            if(isset($request->joinRoomId) && $request->joinRoomId != ''){

                $appUserId = $this->registerAppUser($request->participantName, $request->countryCode . $request->phoneNumber);
                $roomSid = $request->joinRoomId;
                $appUserLang = $request->appUserLang;
                $videoCall = VideoCall::where('room_sid',$roomSid)->select('room_name','operator_id')->first();
                $randomRoomName =$videoCall->room_name;
                $appUserLang = (isset($request->appUserLang) && !empty($request->appUserLang)) ? $request->appUserLang : $videoCall->app_user_lang;
                $type = 'operator_customer';

                $this->addCallLog($appUserId, $roomSid,$videoCall->operator_id, $randomRoomName,$appUserLang,$userId);
                $joinSessionOption = array();
                $participantEmail = '';
                if(isset($request->participant_share_option) && count($request->participant_share_option) == 1){
                	$joinSessionOption = $request->participant_share_option;
                	$participantEmail = $request->participantEmail;
                }
                 // if(isset($host) && $host == 'admin.jastok.com'){
                    $this->sendSMS($request->participantName,$roomSid,$appUserId,$request->countryCode, $request->phoneNumber,$request->app_user_lang,$joinSessionOption, $participantEmail);
                 // }

                $this->joinRoom($type,$roomSid,$appUserId);
            }else{
                $error[] = "Oppps! No room found,please try again....";
            }
        } catch (\Exception $e) {
            if ($e->getCode() == 21211) {
                VideoCall::where('room_sid',$roomSid)->where('app_user_id',$appUserId)->delete();
                AppUser::find($appUserId)->delete();
                $error[] = "Enter Phone Number is not valid, Please entry valid Phone Number ...";
            } else {
                 $error[] = "Oppps! something went wrong while start new session, please try again...";
            }
        }
        return \Response::json(array(
                'error' => $error
        ));

    }

    /**
     * check user is already join or not \
     * @return identity number if yes 
     */
    public function isAlredyJoin($roomSID,$customerID,$type)
    {
        return $checkParticipant = ParticipantDetails::select('identity')->where('room_sid',$roomSID)->where('type',$type)->where('app_user_id',$customerID)->orderBy('id','asc')->first();
    }

    public function joinRoom($type, $roomSID,$customerID, $latitude = null, $longitude = null) {
        try{
            $record = VideoCall::where('room_sid', $roomSID)->where('app_user_id',$customerID)->where('call_status',1)->first();
            $mainOperatorID = 0;
            if(empty($record)) {
                return abort('404');
            }
            if(isset($record->operator_id)){
            	$mainOperatorID = $record->operator_id;
            }
            $number = '';
            $joinToken = $this->isAlredyJoin($roomSID,$customerID,$type);
            if(empty($joinToken)) {
                // if($type=='customer')
                // {
                //     $this->LogIncidents("",0, 'Jastok customer joined session'.$roomSID,"success");
                // }
                // if($type=='operator')
                // {
                //     $this->LogIncidents("",0, 'Jastok operator joined session'.$roomSID,"success");

                // }
                $identity = str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT);
            } else {
                $identity = $joinToken->identity;
            }
            $roomName = $record->room_name;
            $appUserLang = "english";
            
            if(isset($record->app_user_lang) && $record->app_user_lang != '')
            {
                $appUserLang = $record->app_user_lang;
            }
            $appUser = AppUser::find($record->app_user_id);
            if (!empty($record)) {
                if ($type == 'customer') {
                    $record->latitude = ($latitude != 'null') ? $latitude : null;
                    $record->longitude = ($longitude != 'null') ? $longitude : null;

                    if ($latitude != 'null' && $longitude != 'null') {
                        $record->address = $this->geolocationaddress($latitude, $longitude);
                    }

                    $record->app_user_join = 1;
                    //$record->call_start_date_time = now();
                    $record->save();

                    $live = new AppUserLiveLocation();
                    $live->app_user_id = $record->app_user_id;
                    $live->latitude = ($latitude != 'null') ? $latitude : null;
                    $live->longitude = ($longitude != 'null') ? $longitude : null;

                    if ($latitude != 'null' && $longitude != 'null') {
                        $live->address = $this->geolocationaddress($latitude, $longitude);
                    }

                    $live->ip_address = $_SERVER['REMOTE_ADDR'];
                    $live->save();

                    $addDevice = AppUser::find($record->app_user_id);
                    $addDevice->device_type = 'Desktop or Computer';

                    if ($this->isMobileDevice()) {
                        $addDevice->device_type = $_SERVER['HTTP_USER_AGENT'];
                    }

                    $addDevice->save();

                    $number = $addDevice->number;
                    
                }
                $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);

                $videoGrant = new VideoGrant();
                $videoGrant->setRoom($roomSID);

                $token->addGrant($videoGrant);
                if($type != 'operator_customer'){
                    // Store Participant
                    if(empty($joinToken)) {
                        $parDetails = new ParticipantDetails();
                        $parDetails->room_sid = $roomSID;
                        $parDetails->type = $type;
                        $parDetails->identity = $identity;
                        $parDetails->app_user_id = $customerID;
                       // $parDetails->join_token = $token;
                        $parDetails->join_time = Carbon::now()->subSeconds(10);
                        $parDetails->save();
                    } else {
                        $parDetails = $joinToken;
                    }
                    // Get Status
                    $settings   = \App\AppGeneralSetting::select(['approved_file_extensions','max_file_size_operator','max_file_size_user'])->first();
                    $status = [];
                    try { 
                        $status = (new \App\Helpers\CommonHelper)->getStatusSetting();
                    } catch(\Exception $e) {

                    }
                    //$status = StatusSettings::all();
                    if (Auth::user()) {
                        $callTime = "00:00";
                        if(isset($record->call_start_date_time) && !empty($record->call_start_date_time))  {
                            try { 
                                $sTime = Carbon::createFromFormat('Y-m-d H:i:s', $record->call_start_date_time);
                                $cTime = Carbon::createFromFormat('Y-m-d H:i:s', now());
                                $callTime  = $sTime->diffInSeconds($cTime);
                                $callTime = (new \App\Helpers\CommonHelper)->convertSecondToMinuteReadable($callTime);
                            } catch(\Exception $e) {
                                
                            }
                        }

                        $callIdolTime = "00:00";
                        if(isset($record->room_creation_time) && !empty($record->room_creation_time))  {
                            try { 
                                $rcTime = Carbon::createFromFormat('Y-m-d H:i:s', $record->room_creation_time);
                                $cTime = Carbon::createFromFormat('Y-m-d H:i:s', now());
                                $callIdolTime  = $rcTime->diffInSeconds($cTime);
                                $callIdolTime = (new \App\Helpers\CommonHelper)->convertSecondToMinuteReadable($callIdolTime);
                            } catch(\Exception $e) {
                                
                            }
                        }
                        $data['page_title'] = 'Video Call';
                        $participantOperator =  ParticipantDetails::select('identity')->where('room_sid',$roomSID)->where('type','customer')->orderBy('id','asc')->first();
                        VideoCall::where('switched_room_sid',$roomSID)->update(['is_room_switch'=>2]);
                        return view('videocall.room', [
                            'accessToken' => $token->toJWT(),
                            'roomName' => $roomName,
                            'appUserLang' => $appUserLang,
                            'data' => $data,
                            'roomSID' => $roomSID,
                            'customerID'=> $customerID,
                            'mainOperatorID'=> $mainOperatorID,
                            'status' => $status,
                            'appUser' => $appUser,
                            'page_title' => 'Video Call',
                            'settings' => $settings,
                            'startTime' => (isset($record->call_start_date_time) && !empty($record->call_start_date_time)) ? $record->call_start_date_time : '',
                            'roomCreationTime' => (isset($record->room_creation_time) && !empty($record->room_creation_time)) ? $record->room_creation_time : '',
                            'appUserJoin' => $record->app_user_join,
                            'callTime' => $callTime,
                            'callIdolTime' => $callIdolTime,
                            'allowLocation' => $record->allow_location,
                            'user_identity' => !empty($participantOperator) ? $participantOperator->identity: '',
                        ]);
                    } else {
                        
                        $mainUserRec = VideoCall::where('room_sid', $roomSID)->first();
                        $isMainUser  = 1;
                        if(!empty($mainUserRec) && $mainUserRec->app_user_id != $customerID) {
                            $isMainUser  = 0;
                        }
                        $participantOperator =  ParticipantDetails::select('identity')->where('room_sid',$roomSID)->where('type','operator')->first();
                        if($appUserLang == 'italian') 
                            \App::setLocale("it");
                        VideoCall::where('switched_room_sid',$roomSID)->update(['is_room_switch'=>3,'call_status'=>0]);
                        return view('app_user.app_user_join', [
                            'accessToken' => $token->toJWT(),
                            'roomName' => $roomName,
                            'appUserLang' => $appUserLang,
                            'roomSID' => $roomSID,
                            'customerID'=> $customerID,
                            'allowLocation'=>$record->allow_location,
                            'status' => $status,
                            'number' => $number,
                            'page_title' => 'Video Call',
                            'identity' => $identity,
                            'settings' => $settings,
                            'isMainUser' => $isMainUser,
                            'operator_identity' => !empty($participantOperator) ? $participantOperator->identity: '',
                        ]);
                    }
                }else{
                    echo $customerID;exit;
                }
                }else{
                echo 'No Record Found';
                exit;
            }
        }
        catch(\Exception $e){
            $this->LogIncidents("",1, 'Jastok session connect failed'.$roomSID, $e->getMessage());

        }
    }

    public function isMobileDevice() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    public function registerAppUser($name, $number) {
        $appUser = AppUser::where('number', $number)->first();

        if (empty($appUser)) {
            $appUser = new AppUser();
            $appUser->name = $name;
        }

        $appUser->online = 1;
        $appUser->number = $number;
        $appUser->save();

        return $appUser->id;
    }

    public function addCallLog($appUserId, $sid,$operatorId, $randomRoomName, $appUserlang ='',$addedBy = 0) {
        $getAccountData = (new \App\Helpers\CommonHelper)->getAccountData();
        $accountId = 0; 
        $recordingAllow = 1; 
        $locationAllow = 1;
        if(empty($getAccountData))  { 
            $videoData =  VideoCall::where('room_sid',$sid)->first();
            $accountId = $videoData->account_id; 
            $recordingAllow = $videoData->allow_recording; 
            $locationAllow = $videoData->allow_location;
        } else  {
            $accountId = $getAccountData->id; 
            $recordingAllow = $getAccountData->require_rec; 
            $locationAllow = $getAccountData->require_loc;
        }
        $obj = new VideoCall();
        $obj->operator_id = $operatorId;
        $obj->room_sid = $sid;
        $obj->room_name = $randomRoomName;
        $obj->app_user_id = $appUserId;
        $obj->call_status = 1;
        $obj->allow_recording = $recordingAllow  ;
        $obj->allow_location = $locationAllow;
        $obj->account_id = $accountId;
        $obj->app_user_lang = $appUserlang;
        $obj->added_by = $addedBy;
       // $obj->call_start_date_time = now();
        $obj->save();
        return $obj->id;
    }


    public function convertTimeToSecond($time = '') {
        if ($time) {
            $seconds = 0;
            $parts = explode(':', $time);

            if (count($parts) > 2) {
                $seconds += $parts[0] * 3600;
                $seconds += $parts[1] * 60;
                $seconds += $parts[2];
            } else {
                $seconds += $parts[0] * 60;
                $seconds += $parts[1];
            }

            return $seconds;
        }
    }

    public function getCallDuration(Request $request) {

        $roomSID = $request->input('roomSID');
        $calDur = $request->input('calDur');
        $operatorId = $request->input('operatorId');
        $endCall = 0;
        try {
             $this->updateDuration($request,'op');
        } catch(\Exception $e) {
            
        }
        if ($request->has('endCall')) {
            $endCall = $request->input('endCall');
        }
        $duration = $calDur;
        $obj = VideoCall::where('room_sid', $roomSID)->where('app_user_id',$operatorId)->first();
        try {
            if(!empty($obj)) {
                \App\Meeting::where('video_call_id',$obj->id)->update(['status'=>3]);
            }
        } catch(\Exception $e) {

        }
        if (!empty($obj)) {
            if (isset($calDur)) {
                $temp = $this->convertTimeToSecond($calDur);

                if(is_null($obj->duration)){
                    $obj->duration = $this->convertTimeToSecond($calDur);
                }

                if(!is_null($obj->duration) && $temp > $obj->duration){
                    $obj->duration = $temp;
                }

                $obj->call_end_date_time = now();
            }
            if ($endCall) {
                // if($request->closetab==1){
                // $this->LogIncidents("",0, 'Jastok operator left the page :'.$roomSID,"success");
                // }
                // else
                // {
                // $this->LogIncidents("",0, 'Jastok operator has ended the call :'.$roomSID,"success");

                // }
                $videoCallArray = VideoCall::where('room_sid', $roomSID)->where('operator_id',$obj->operator_id)->where('call_status',1)->orderBy('id','asc')->get()->toArray();
                $mainOperatorID = $mainAppUserID = 0;
                if(!empty($videoCallArray)){
                    $mainOperatorID = $videoCallArray[0]['operator_id'];
                    $mainAppUserID = $videoCallArray[0]['app_user_id'];

                    if($mainAppUserID != $operatorId){
                        VideoCall::where('room_sid', $roomSID)->where('operator_id',$obj->operator_id)->where('app_user_id',$operatorId)->update(['call_status'=> 0]);
                    }else{
                        VideoCall::where('room_sid', $roomSID)->where('operator_id',$obj->operator_id)->update(['call_status'=> 0]);
                    }
                }


                $appUserID = $obj->app_user_id;
                $obj->call_end_date_time = now();
                // $obj->call_status = 0;
            }
            $obj->save();
            if ($endCall) {
                if(isset($appUserID) && $appUserID){
                        $appUser = AppUser::find($appUserID);
                        $appUser->online = 0;
                        $appUser->save();
                }

                // Store Participant
                $client = new Client($this->sid, $this->token);
                $uri = "https://video.twilio.com/v1/Rooms/" . $roomSID . "/Participants";
                $response = $client->request("GET", $uri);
                $mediaLocation = $response->getContent();
                $participants = $mediaLocation['participants'];

                foreach ($participants as $key => $participant) {
                    $participantDetails = ParticipantDetails::where('room_sid', $roomSID)
                            ->where('identity', $participant['identity'])
                            ->first();

                    if (!empty($participantDetails) && $participantDetails) {
                        $participantDetails->participant_id = $participant['sid'];
                        $participantDetails->save();
                    } else {
                         $text = "Participant : $roomSID  <>".$participant['sid'];
                         Log::channel('customlog')->info($text);
                    }
                }
                if(!empty($videoCallArray)){
                    if($mainAppUserID == $operatorId){
                        $client->video->v1->rooms($obj->room_sid)->update("completed");

                        $session = VideoCall::where('room_sid', $obj->room_sid)->where('app_user_id',$request->operatorId)->first();
                        if (!empty($session)) {
                            if($session->allow_recording == 1){
                                // For Video Start
                                if(is_null($session->composition_sid)){
                                    $audio = $video = array();
                                    $parDetail = ParticipantDetails::where('room_sid',$session->room_sid)
                                                ->pluck('participant_id','type')->toArray();

                                    if(!empty($parDetail['operator']) && !empty($parDetail['customer'])){
                                    foreach($parDetail as $key => $value){
                                        $recordings = $client->video->v1->recordings
                                                            ->read(["groupingSid" => [$value]],
                                                                20
                                                            );
                                        foreach ($recordings as $record) {
                                            if($record->type == 'audio'){
                                                $audio[$key] = $record->sid;
                                            }else if($record->type == 'video'){
                                                $video[$key] = $record->sid;
                                            }
                                        }
                                    }
                                    } else {
                                        try {
                                        $text = "Twilio Missing participant op: " . (isset($parDetail['operator']) ? json_encode($parDetail['operator']) : '')  . "  <cust>". isset($parDetail['customer']) ? json_encode($parDetail['customer']) : '';
                                        Log::channel('customlog')->info($text);
                                        } catch(\Exception $e) {

                                        }
                                    }

                                if((count($audio) >= 2 && count($video) >= 2) || 1 == 1 ){
                                    DB::beginTransaction();
                                    try { 
                                        $composition = $client->video->compositions->create($roomSID, [
                                            'audioSources' => '*',
                                            'videoLayout' =>  array(
                                                'grid' => array (
                                                  'video_sources' => array('*'),
                                                )
                                              ),
                                            // 'videoLayout' =>  array(
                                            //                     'main' => array (
                                            //                       'z_pos' => 1,
                                            //                       'video_sources' => array($video['customer'])
                                            //                     ),
                                            //                     'pip' => array(
                                            //                       'z_pos' => 2,
                                            //                       'width' => 240,
                                            //                       'height' => 180,
                                            //                       'video_sources' => array($video['operator'])
                                            //                     )
                                            //                   ),
                                            'statusCallback' => $request->root().'/getVideoCallBack',
                                            'resolution' => '640x480',
                                            'format' => 'mp4'
                                        ]);
                                         $this->LogIncidents($roomSID,0, 'Video composing status :'.$composition->status,"video composed");
                                        $session->composition_sid    = $composition->sid;
                                        $session->composition_status = $composition->status;
                                        $session->save();
                                        DB::commit();
                                    } catch(\Exception $e) {
                                        DB::rollback();
                                        $this->LogIncidents($roomSID,1, 'Jastok Video Log Testing1'.$obj->id, $e->getMessage());
                                        $error = json_encode($e);
                                        $text = "Twilio response: on composition ".$e;
                                        Log::channel('customlog')->info($text);
                                    }

                                    }else{
                                        $text = "Twillio :  Video or Audio not found : ";
                                        Log::channel('customlog')->info($text);

                                        Mail::send('videocall.email_video_improper', ['roomSID' => $roomSID], function ($m) use ($roomSID) {
                                                //$m->from('noreply@jastok.com', 'Remote Doctor'); //company Email

                                                // config('constants.mail_from_email')
                                                $m->to('noreply@jastok.com',  env("MAIL_FROM_NAME"))->subject('Video Improper !'); // receiver Email
                                            });
                                        $this->LogIncidents($roomSID,1,'Jastok Video Log Testing1'.$obj->id, "video or audio not found");
                                    }
                                }
                            }
                        } else {
                            $text = "Empty Session date";
                            Log::channel('customlog')->info($text);
                        }
                    }
                }
            }
        }
        return \Response::json(array('duration' => $duration));
    }


    public function getVideoCallBack(Request $request){

        $data = json_encode($request->all());
        Log::channel('customlog')->info($data);

        if($request->Size && $request->CompositionSid) {
            $client    = new Client($this->sid, $this->token);
            $uri = "https://video.twilio.com/v1/Compositions/".$request->CompositionSid."/Media/?Ttl=3600";

            $response = $client->request("GET", $uri);
            $mediaLocation = $response->getContent();
            if(count($mediaLocation) == 1 && isset($mediaLocation['redirect_to'])){

                $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.mp4';
                $redirect = $mediaLocation['redirect_to'];
                $updatedData =  ['file' => $fileName,'composition_status'=>'Completed'];
                if(isset($request->Duration)) {
                    $updatedData['duration'] = $request->Duration;
                } 
                $this->LogIncidents($request->RoomSid,0, 'Video composing status :',"video composed  COMPLETED");
                $videoCall = VideoCall::where('room_sid',$request->RoomSid)->update($updatedData);
                Storage::disk('s3')->put($fileName, file_get_contents($redirect));
                $client->video->compositions($request->CompositionSid)->delete();
                $recordings = $client->video->v1->rooms($request->RoomSid)->recordings->read([], 20);
                foreach ($recordings as $record) {
                    $client->video->v1->recordings($record->sid)->delete();
                }
            }
        }
    }
    public function getRoomStatus(Request $request) {
        try {
            $status = '';

        	if (isset($request->calDur) && isset($request->roomSID) && isset($request->appUserId)) {

        		$call = VideoCall::where('room_sid', $request->roomSID)->where('app_user_id',$request->appUserId)->first();
                $redirect="";
                $meeting = \App\Meeting::where('video_call_id',$call->id)->where('app_user_id',$request->appUserId)->first();
                if(!empty($meeting)) {
                    $redirect  = route('success-meeting-screen',['meeting_id'=>$meeting->id]);
                }

                if (isset($request->calDur) && isset($request->roomSID) && isset($request->appUserId)) {
                    $call->duration = $this->convertTimeToSecond($request->calDur);
                    $call->call_end_date_time = now();
                    // $call->call_status = 0;
                    $call->save();
                }
                $appUserIdentity = '';
                try{ 
                    $participantOperator =  ParticipantDetails::select('identity')->where('room_sid',$request->roomSID)->where('type','customer')->orderBy('id','asc')->first();
                    if(!empty($participantOperator)) {
                        $appUserIdentity = $participantOperator->identity;
                    }
                } catch(\Exception $e) {
                }
        		if(isset($call->call_status) && $call->call_status == 0){
                    $status = "completed";
                    $this->updateDuration($request);
        			return \Response::json(array(
                        'status' => "completed",
                        'error' =>'',
                        'url' => $redirect,
		        	));
                    $this->LogIncidents("",0, 'Jastok room status COMPLETED'.$request->roomSID,"success");
                }
        	}

	        // $client = new Client($this->sid, $this->token);
	        // $rooms = $client->video->v1->rooms->read(["status" => "completed"]);
	        // $data = [];

	        // foreach ($rooms as $record) {
	        //     if ($record->sid == $request->roomSID) {
	        //         if (isset($request->calDur) && isset($request->roomSID) && isset($request->appUserId)) {
	        //             $call->duration = $this->convertTimeToSecond($request->calDur);
	        //             $call->call_end_date_time = now();
	        //             $call->call_status = 0;
	        //             $call->save();
	        //         }
	        //         $status = "completed";
	        //         break;
	        //     }
	        // }

	        if($status == "completed"){
				return \Response::json(array(
	                'status' => $status,
                        'attachFiles' => '',
                        'totalCount' => 0,
                    'error' =>'',
                    'url' => $redirect,
	        	));
	        }else{
	        	$totalCount = 0;
			    $query = VideoAttachments::leftJoin('video_attachments_status as vs', 'video_attachments.id', 'vs.video_attachments_id')
			            ->where('video_attachments.room_sid', $request->roomSID);
			    if (Auth::user()) {
		            $query->where('vs.operator_id', Auth::user()->id)
		                    ->where('vs.attachment_status', 0);
		        } else {
		            $countQue = clone $query;
		            $totalCount = $countQue->where('vs.app_user_id',$request->appUserId)
		                            ->where('vs.attachment_status', '!=', 2)->whereNull('is_snap')->count();

		            $query->where('vs.app_user_id',$request->appUserId);
		        }
		        $attachData = $query->whereNull('vs.deleted_at')->whereNull('video_attachments.deleted_at')
                                ->whereNull('video_attachments.is_snap')
		                        ->select('video_attachments.file', 'video_attachments.id', 'vs.id as vsId')
		                        ->get()->toArray();

		        if (Auth::user()) {
		            if (count($attachData) > 0) {
		                foreach ($attachData as $val) {
		                    $attachStatus = videoAttachmentsStatus::where('video_attachments_id', $val['id'])
		                            ->where('operator_id', Auth::user()->id)
		                            ->update(['attachment_status' => 1]);
		                }
		            }
		        }
                /*snap data*/
                $snapquery = VideoAttachments::leftJoin('video_attachments_status as vs', 'video_attachments.id', 'vs.video_attachments_id')
                        ->where('video_attachments.room_sid', $request->roomSID)->where('video_attachments.is_snap',1);
                if (Auth::user()) {
                    $snapquery->where('video_attachments.operator_id', Auth::user()->id)->where('vs.attachment_status', 0);
                }
                $attachsnapData = $snapquery->whereNull('vs.deleted_at')->whereNull('video_attachments.deleted_at')
                                ->select('video_attachments.file', 'video_attachments.id', 'vs.id as vsId','video_attachments.thumbnail')
                                ->get()->toArray();

                if (Auth::user()) {
                    if (count($attachsnapData) > 0) {
                        foreach ($attachsnapData as $val) {
                            $attachStatus = videoAttachmentsStatus::where('video_attachments_id', $val['id'])
                                    ->update(['attachment_status' => 1]);
                        }
                    }
                }
                /*snap data ends*/
                // Return App User Live Location Data
                $operatorId = $request->appUserId;
                $appUser    = null;
                $call = VideoCall::where('room_sid', $request->roomSID)
                                ->where('app_user_id',$operatorId)
                                ->first();

                if (!empty($call) && $call->app_user_join == 1) {
                    $lastRecord = AppUserLiveLocation::where('app_user_id', $call->app_user_id)
                            ->orderBy('created_at', 'desc')
                            ->first();

                    $appUser = $lastRecord;
                }

                
                

		        return \Response::json(array(
                    'status' => $status,
                    'url' => $redirect,
                    'attachFiles' => $attachData,
                    'attachSnaps' => $attachsnapData,
                    'totalCount'  => $totalCount,
                    'appUser'     => $appUser,
                    'error'       =>'',
                    'userMediaError'=>(isset($call->is_media_error)) ? $call->is_media_error : 0,
                    'isIos'=>(isset($call->is_ios)) ? $call->is_ios : 0,
                    'startTime'  => (isset($call->call_start_date_time) && !empty($call->call_start_date_time)) ? $call->call_start_date_time : 0,
                    'appUserIdentity' => $appUserIdentity,
                    'is_switched' => isset($call->is_room_switch) ? $call->is_room_switch :0,
                    'switched_url' => (isset($call->switched_room_sid) && !empty($call->switched_room_sid)) ? ((auth()->check()) ? route('joinScreen', ['type' => "operator",'roomSID' => $call->switched_room_sid,'customerID' => $call->app_user_id]) : route('joinScreen', ['type' => "customer",'roomSID' => $call->switched_room_sid,'customerID' => $call->app_user_id])) : '',
		        ));
	        }


        }catch (\Exception $e) {
            $this->LogIncidents("",1, 'Jastok room status Testing Roomsid : '.$request->roomSID, $e->getMessage());

            return \Response::json(array(
                'error' => $e->getMessage()
            ));
    }
    }

    public function saveRoomCreationTime(Request $request){
        try {
            if (isset($request->calDur) && isset($request->roomSID)) {
                $call = VideoCall::where('room_sid', $request->roomSID)->first();
                if($call->room_creation_time == NULL || $call->room_creation_time == ''){
                    $call->room_creation_time = now(); 
                    $call->save();
                }
            }
            //$this->LogIncidents(0, 'Jastok room creation time  Log Testing Room id'.$request->roomSID,"success");

            return \Response::json(array(
                    'error' =>'',
                    'roomSID' => $request->roomSID,
                    'appUserJoin' => $call->app_user_join,
                    'roomCreationTime'  => (isset($call->room_creation_time) && !empty($call->room_creation_time)) ? $call->room_creation_time : 0,
                ));

        }catch (\Exception $e) {
            $this->LogIncidents("",1, 'Jastok room creation time  Log Testing Room id'.$request->roomSID, $e->getMessage());

            return \Response::json(array(
                'error' => $e->getMessage()
            ));
        }
    }

    public function updateAttachmentStatus(Request $request) {
        $attachStatus = videoAttachmentsStatus::where('id', $request['videoAttachId'])
                ->update(['attachment_status' => 2]);
    }

    public function isAppUSerJoin(Request $request) {

        $roomSID = $request->input('roomSID');
        $oper_array = $request->input('oper_array');
        $appUserJoin = $appUserArry = $callArry = array();
        $location = [];
        $i = 0;
        foreach($oper_array as $valId){
            $call = VideoCall::where('room_sid', $roomSID)->where('app_user_id',$valId)->first();
            $location[$i][] = $call['latitude'];
            $location[$i][] = $call['longitude'];
            $location[$i][] = $call['address'];
            $appUser = AppUser::find($call->app_user_id);
            $appUser->number = $appUser->number;
            $val = false;

            $userjoin="not joined";
            if (!empty($call) && $call->app_user_join == 1) {
                $call->status_id = 2;
                $call->save();
                $val = true;
                $userjoin="user is joined";
            }
            $appUserJoin[$valId] = $val;
            $appUserJoin["media"] = $call->is_media_error;
            $appUserJoin["is_ios"] = $call->is_ios;
            $appUserArry[$valId] = $appUser;
            $callArry[$valId] = $call;

            $i++;
        }

        $image = 'http://www.gravatar.com/avatar/?d=identicon';
        $appUserIdentity = '';
        try {
            $participantUser = ParticipantDetails::select('identity')->where('room_sid',$roomSID)->where('type','customer')->orderBy('id','asc')->first();
            if(!empty($participantUser)) {
                $appUserIdentity = $participantUser->identity;
            }
            // $this->LogIncidents(0, 'Jastok room app user join Testing Room id : '.$request->roomSID.', user join status :'.$userjoin,"success");

        } catch(\Exception $e) {
            $this->LogIncidents("",1, 'Jastok room app user join Testing Room id : '.$request->roomSID.', user join status : '.$userjoin, $e->getMessage());
        }
        return \Response::json(array(
                    'appUserJoin' => $appUserJoin,
                    'appUser' => $appUserArry,
                    'call' => $callArry,
                    'image' => $image,
                    'getData' => $location,
                    'appUserIdentity' => $appUserIdentity
        ));

    }

    public function setAppUserLiveLocation(Request $request) {
        $roomSID = $request->input('roomSID');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $operatorId = $request->input('operatorId');

        $call = VideoCall::where('room_sid', $roomSID)->where('app_user_id',$operatorId)->first();
        if (!empty($call) && $call->app_user_join == 1) {
            $live = new AppUserLiveLocation();
            $live->app_user_id = $call->app_user_id;
            $live->latitude = $latitude;
            $live->longitude = $longitude;
            $live->address = $this->geolocationaddress($latitude, $longitude);
            $live->ip_address = $_SERVER['REMOTE_ADDR'];
            $live->save();

            return \Response::json(array(
                        'success' => true
            ));
        }

        return \Response::json(array('success' => false));
    }

    public function getAppUserLiveLocation(Request $request) {
        $roomSID = $request->input('roomSID');
        $operatorId = $request->input('operatorId');
        $call = VideoCall::where('room_sid', $roomSID)->where('app_user_id',$operatorId)->first();

        if (!empty($call) && $call->app_user_join == 1) {
            $lastRecord = AppUserLiveLocation::where('app_user_id', $call->app_user_id)
                    ->orderBy('created_at', 'desc')
                    ->first();

            return \Response::json(array(
                        'success' => true,
                        'appUser' => $lastRecord
            ));
        }

        return \Response::json(array('success' => false));
    }

    public function getLocationData() {
        $dashboardListLimit = 5;
        $getData = new VideoCall;
        if (Auth::user()->role == 'operator') {
            $operatorId = Auth::user()->id;
            $getData = $getData->where('operator_id', $operatorId);
        } elseif (Auth::user()->role != 'Top Manager') {
            $managerId = Auth::user()->id;
            $getOperaterIds = Managers::where('parent_manager_id', $managerId)->where('type', 'operator')->pluck('id')->toArray();
            $getData = $getData->whereIn('operator_id', $getOperaterIds);
        }
        $startDate = Carbon::now()->startOfMonth()->toDateString(). " 00:00:00";
        $endDate = Carbon::now()->endOfMonth()->toDateString() . " 23:59:59";
        $getLocationData = $getData->whereNull('video_call.deleted_at')
                        ->whereNotNull('latitude')
                        ->whereNotNull('longitude')->orderBy('video_call.id', 'desc')
                        ->groupBy('room_sid')
                        ->select('video_call.latitude', 'video_call.longitude', 'video_call.id')->
                        where('created_at',">=",$startDate)->where('created_at','<=',$endDate)->get();
        $location = [];
        if ($getLocationData) {
            foreach ($getLocationData as $key => $val) {
                $location[$key][] = $val->latitude;
                $location[$key][] = $val->longitude;
                $location[$key][] = $val->id;
            }
        }
        return \Response::json(array(
                    'success' => true,
                    'getData' => $location
        ));
    }

    private function getDatesFromRange($date_time_from, $date_time_to) {
        $date_from = strtotime($date_time_from);
        $date_to = strtotime($date_time_to);
        $range = [];
        for ($i = $date_from; $i <= $date_to; $i += 86400) {
            $range[] = date("Y-m-d", $i);
        }
        return $range;
    }

    public function getSessionGraphData(Request $request) {
        $getData = new VideoCall;
        $first_day_this_month = $request['startDate'];
        $last_day_this_month = $request['endDate'];

        $firstDay = explode('/', $first_day_this_month);
        $lastDay = explode('/', $last_day_this_month);
        $monthlyDateArray = [];
        $d = 1;
        $monthlyDateArray = $this->getDatesFromRange(str_replace('/', '-', $first_day_this_month), str_replace('/', '-', $last_day_this_month));
        if (Auth::user()->role == 'operator') {
            $operatorId = Auth::user()->id;
            $getData = $getData->where('operator_id', $operatorId);
        } elseif (Auth::user()->role != 'Top Manager') {
            $managerId = Auth::user()->id;
            $getOperaterIds = Managers::where('parent_manager_id', $managerId)->where('type', 'operator')->pluck('id')->toArray();
            $getData = $getData->whereIn('operator_id', $getOperaterIds);
        }
        $getLocationData = $getData->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
                        ->whereDate('created_at', '>=', date("Y-m-d", strtotime(str_replace('/', '-', $first_day_this_month))))
                        ->whereDate('created_at', '<=', date("Y-m-d", strtotime(str_replace('/', '-', $last_day_this_month))))
                        ->whereNull('deleted_at')
                        ->groupBy('date','room_sid')
                        ->pluck('views', 'date')->toArray();

        $chartData = [];
        $labels = [];
        $data =[];
        if (count($monthlyDateArray) > 0) {
            foreach ($monthlyDateArray as $key => $val) {
                $chartData[$key]['x'] = date("d/m", strtotime($val));
                $labels[] = $chartData[$key]['x'];
                if ($getLocationData && array_key_exists($val, $getLocationData)) {
                        if(Auth::user()->role == 'Top Manager') {
                            $viewsCount = $this->getTopManTotSession($val);
                            $chartData[$key]['y'] = $viewsCount;
                        }else if(Auth::user()->role == 'operator'){
                            $viewsCount = $this->getOperatorTotSession($val);
                            $chartData[$key]['y'] = $viewsCount;
                        }else if(Auth::user()->role == 'manager'){
                            $viewsCount = $this->getManagerTotSession($val);
                            $chartData[$key]['y'] = $viewsCount;
                        }else{
                            $chartData[$key]['y'] = $getLocationData[$val];
                        }
                        
                } else {
                    $chartData[$key]['y'] = 0;
                }
                $data[] = $chartData[$key]['y'];
            }
        }

        $jsonEncode = '';
        if (count($chartData) > 0) {
            $chartData =  
            [
             'data'=>$data,
             'labels' => $labels,
             'label' => __('Sessions')
            ];
            //$chartData['labels'] = $labels;
            return json_encode($chartData);
        }
    }

    public function getManagerTotSession($date){
        $viewsCount = 0;
        $manOpeIds = Managers::where('parent_manager_id',Auth::user()->id)->pluck('id')->toArray();

        foreach($manOpeIds as $id){
            $getData = new VideoCall;
            $getLocationData = $getData->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
                            ->where('operator_id', '=', $id)
                            ->whereDate('created_at', '=', $date)
                            ->whereNull('deleted_at')
                            ->orderBy('video_call.id','desc')
                            ->groupBy('date','room_sid')
                            ->get();


            if(count($getLocationData) > 0){
                foreach($getLocationData as $value){
                    $viewsCount = $viewsCount + (($value->views > 1)?1:$value->views);
                }
            }
        }

        return $viewsCount;
    }

    public function getOperatorTotSession($date){
        $getData = new VideoCall;
        $getLocationData = $getData->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
                        ->where('operator_id', '=', Auth::user()->id)
                        ->whereDate('created_at', '=', $date)
                        ->whereNull('deleted_at')
                        ->groupBy('date','room_sid')
                        ->orderBy('video_call.id','desc')
                        ->get();


        $viewsCount = 0;
        if(count($getLocationData) > 0){
            foreach($getLocationData as $value){
                $viewsCount = $viewsCount + (($value->views > 1)?1:$value->views);
            }
        }


        return $viewsCount;
    }

    public function getTopManTotSession($date){
        $getData = new VideoCall;
        $getLocationData = $getData->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
                        ->whereDate('created_at', '=', $date)
                        ->whereNull('deleted_at')
                        ->groupBy('date','room_sid')
                        ->orderBy('video_call.id','desc')
                        ->get();

        $viewsCount = 0;
        if(count($getLocationData) > 0){
            foreach($getLocationData as $value){
                $viewsCount = $viewsCount + (($value->views > 1)?1:$value->views);
            }
        }

        return $viewsCount;
    }

    public static function geolocationaddress($lat, $long) {
        $geocode = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=AIzaSyDVoI-SJ-B0RsVcF6uEAYTeyq-CFnVEOTc";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $geocode);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($response);
        
        $dataarray = get_object_vars($output);

        if ($dataarray['status'] != 'ZERO_RESULTS' && $dataarray['status'] != 'INVALID_REQUEST') {
            if (isset($dataarray['results'][0]->formatted_address)) {
                $address = $dataarray['results'][0]->formatted_address;
            } else {
                $address = 'Not Found';
            }
        } else {
            $address = 'Not Found';
        }

        return $address;
    }

    public function sessionDataExport($searchVal = NULL) {
        $data = array();
        if (Auth::user()->type == 'Top Manager') {
            $sessions = VideoCall::leftJoin('app_user', 'video_call.app_user_id', '=', 'app_user.id')
                    ->leftJoin('managers','managers.id','video_call.operator_id')
                    ->leftJoin('accounts','accounts.id','video_call.account_id')
                    ->leftJoin('status_settings','status_settings.id','video_call.status_id')
                    ->when($searchVal, function ($query, $searchVal){
                        return $query->where('accounts.name', 'like','%'.$searchVal.'%')
                            ->orWhere('app_user.name', 'like','%'.$searchVal.'%')
                            ->orWhere('status_settings.status_name', 'like','%'.$searchVal.'%')
                            ->orWhere('app_user.number', 'like','%'.$searchVal.'%')
                            ->orWhere('managers.first_name', 'like','%'.$searchVal.'%')
                            ->orWhere('managers.last_name', 'like','%'.$searchVal.'%')
                            ->orWhere('video_call.address', 'like','%'.$searchVal.'%');
                    })
                    ->groupBy('video_call.room_sid')
                    ->orderBy('video_call.id', 'desc')
                    ->get(['video_call.*', 'app_user.name as app_user_name',
                'app_user.number as app_user_number']);
        } else if (Auth::user()->type == 'manager') {
            $operatorIds = Managers::where('parent_manager_id', Auth::user()->id)->pluck('id')->toArray();
            $sessions = VideoCall::leftJoin('app_user', 'video_call.app_user_id', '=', 'app_user.id')
                    ->leftJoin('managers','managers.id','video_call.operator_id')
                    ->leftJoin('accounts','accounts.id','video_call.account_id')
                    ->leftJoin('status_settings','status_settings.id','video_call.status_id')
                    ->when($searchVal, function ($query, $searchVal){
                        return $query->where('accounts.name', 'like','%'.$searchVal.'%')
                            ->orWhere('app_user.name', 'like','%'.$searchVal.'%')
                            ->orWhere('status_settings.status_name', 'like','%'.$searchVal.'%')
                            ->orWhere('app_user.number', 'like','%'.$searchVal.'%')
                            ->orWhere('managers.first_name', 'like','%'.$searchVal.'%')
                            ->orWhere('managers.last_name', 'like','%'.$searchVal.'%')
                            ->orWhere('video_call.address', 'like','%'.$searchVal.'%');
                    })
                    ->whereIn('video_call.operator_id', $operatorIds)
                    ->groupBy('video_call.room_sid')
                    ->orderBy('video_call.id', 'desc')
                    ->get(['video_call.*', 'app_user.name as app_user_name',
                'app_user.number as app_user_number']);
        } else {
            $operatorId = Auth::user()->id;
            $sessions = VideoCall::leftJoin('app_user', 'video_call.app_user_id', '=', 'app_user.id')
                    ->leftJoin('managers','managers.id','video_call.operator_id')
                    ->leftJoin('accounts','accounts.id','video_call.account_id')
                    ->leftJoin('status_settings','status_settings.id','video_call.status_id')
                    ->when($searchVal, function ($query, $searchVal){
                        return $query->where('accounts.name', 'like','%'.$searchVal.'%')
                            ->orWhere('app_user.name', 'like','%'.$searchVal.'%')
                            ->orWhere('status_settings.status_name', 'like','%'.$searchVal.'%')
                            ->orWhere('app_user.number', 'like','%'.$searchVal.'%')
                            ->orWhere('managers.first_name', 'like','%'.$searchVal.'%')
                            ->orWhere('managers.last_name', 'like','%'.$searchVal.'%')
                            ->orWhere('video_call.address', 'like','%'.$searchVal.'%');
                    })
                    ->where('video_call.operator_id', $operatorId)
                    ->groupBy('video_call.room_sid')
                    ->orderBy('video_call.id', 'desc')
                    ->get(['video_call.*', 'app_user.name as app_user_name',
                'app_user.number as app_user_number']);
        }
        foreach ($sessions as $key => $session) {
            $manager = Managers::find($session->operator_id);
            $statusSettings = StatusSettings::find($session->status_id);
            $session->satus_name = ($statusSettings) ? $statusSettings->status_name : '';
            $session->operatorName = ($manager) ? $manager->first_name . ' ' . $manager->last_name : '';
            $data[$key]['id'] = $session->id;
            $data[$key]['date'] = $session->created_at->format('d/m/Y H:i');
            $data[$key]['user'] = $session->app_user_name;
            $data[$key]['status'] = $session->satus_name;
            $data[$key]['user_phone'] = $session->app_user_number;
            $data[$key]['Operator'] = $session->operatorName;
            $data[$key]['total_time'] = (new \App\Helpers\CommonHelper)->convertSecondToMinute($session->duration);
            $data[$key]['Location'] = $session->address;
        }

        return \Excel::download(new SessionExport($data), Now() . '_session.xlsx');
    }
    public function save_snap(Request $request)
    {
        $val = $request->image;
        $dir = public_path('videocallAttachments/');
        $image_parts = explode(";base64,", $val);

        $image_base64 = base64_decode($image_parts[1]);

        $filename = "OperatorSnap".uniqid().'_' . date('Ymd') .'.png';

        $file = $dir . $filename;

        file_put_contents($file, $image_base64);

        $path = public_path() . '/videocallAttachments/' . $filename;
        $thumbnailpath = public_path() . '/thumbnail/';
        $thumbnailname = "thumbnail_".uniqid().'_' . date('Ymd') .'.png';


        // $file = File::get($path);
        // $type = File::mimeType($path);
        $img = imagecreatefromjpeg($path);
        $size = filesize($path);
        $width = imagesx($img);
        $height = imagesy($img);
        // calculate thumbnail size
        $new_width = 100;
        $new_height = 100;
        // create a new temporary image
        $tmp_img = imagecreatetruecolor( $new_width, $new_height );

          // copy and resize old image into new image
        imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

        // save thumbnail into a file
        imagejpeg($tmp_img, $thumbnailpath.$thumbnailname);

            $videoAttach = new VideoAttachments();
            $videoAttach->room_sid = $request->roomSID;
            $videoAttach->operator_id = (Auth::user())?Auth::user()->id:'';
            $videoAttach->app_user_id = (Auth::user())?'':$request->appUserId;
            $videoAttach->uploader_type = (Auth::user())?'operator':'customer';
            $videoAttach->is_snap = 1;
            $videoAttach->file = $filename;
            $videoAttach->file_size = $size;
            $videoAttach->thumbnail=$thumbnailname;
            //$videoAttach->file_size = $size;
            $videoAttach->save();
            $videoCall = VideoCall::where('room_sid', $request->roomSID)->get()->toArray();
            $i = 0;
            foreach($videoCall as $call){
                $videoAttachmentsStatus = new videoAttachmentsStatus();
                $videoAttachmentsStatus->video_attachments_id = $videoAttach->id;
                //if(Auth::user()){
                    $videoAttachmentsStatus->operator_id = '';
                    $videoAttachmentsStatus->app_user_id = $call['app_user_id'];
                // }else{
                //     $videoAttachmentsStatus->operator_id = ($i == 0)?$call['operator_id']:'';
                //     if($call['app_user_id'] != $videoAttach->app_user_id){
                //         $videoAttachmentsStatus->app_user_id = ($i == 0)?'':$call['app_user_id'];
                //         if($i == 0) {
                //             $videoAttachmentsStatusForAppUserr= new videoAttachmentsStatus();
                //             $videoAttachmentsStatusForAppUserr->app_user_id = $call['app_user_id'];
                //             $videoAttachmentsStatusForAppUserr->operator_id = '';
                //             $videoAttachmentsStatusForAppUserr->attachment_status = 0;
                //             $videoAttachmentsStatusForAppUserr->video_attachments_id = $videoAttach->id;
                //             $videoAttachmentsStatusForAppUserr->save();
                //         }
                //     }


                // }
                $videoAttachmentsStatus->attachment_status = 0;
                $videoAttachmentsStatus->save();
                break;
                   // $i++;
            }
            return \Response::json(array(
                    'succes' => true,
            ),200);

        //$val->move($dir, $filename);
    }
    public function addVideoAttachment(Request $request) {
        try{
            $extension = $request->file('file');
            $dir = public_path('videocallAttachments');
            $videoCall = VideoCall::where('room_sid', $request['roomSID'])->get()->toArray();
            foreach ($extension as $val) {
                $extension = $val->getClientOriginalExtension();
                $size = $val->getClientSize();
                $filename = uniqid() . '_' . date('Ymd') . '.' . $extension;
                $val->move($dir, $filename);

                $videoAttach = new VideoAttachments();
                $videoAttach->room_sid = $request['roomSID'];
                $videoAttach->operator_id = (Auth::user())?Auth::user()->id:'';
                $videoAttach->app_user_id = (Auth::user())?'':$request['appUserId'];
                $videoAttach->uploader_type = (Auth::user())?'operator':'customer';
                $videoAttach->file = $filename;
                $videoAttach->file_size = $size;
                $videoAttach->save();

                $i = 0;
                foreach($videoCall as $call){
                    $videoAttachmentsStatus = new videoAttachmentsStatus();
                    $videoAttachmentsStatus->video_attachments_id = $videoAttach->id;
                    if(Auth::user()){
                        $videoAttachmentsStatus->operator_id = '';
                        $videoAttachmentsStatus->app_user_id = $call['app_user_id'];
                    }else{
                        $videoAttachmentsStatus->operator_id = ($i == 0)?$call['operator_id']:'';
                        if($call['app_user_id'] != $videoAttach->app_user_id){
                            $videoAttachmentsStatus->app_user_id = ($i == 0)?'':$call['app_user_id'];
                            if($i == 0) {
                                $videoAttachmentsStatusForAppUserr= new videoAttachmentsStatus();
                                $videoAttachmentsStatusForAppUserr->app_user_id = $call['app_user_id'];
                                $videoAttachmentsStatusForAppUserr->operator_id = '';
                                $videoAttachmentsStatusForAppUserr->attachment_status = 0;
                                $videoAttachmentsStatusForAppUserr->video_attachments_id = $videoAttach->id;
                                $videoAttachmentsStatusForAppUserr->save();
                            }
                        }


                    }
                $videoAttachmentsStatus->attachment_status = 0;
                $videoAttachmentsStatus->save();
                    $i++;
            }


            }
            //$this->LogIncidents("",0, 'Jastok  file uploading Testing Room id'.$request['roomSID'],"success");

            return \Response::json(array(
                        'success' => true,
            ),200);
            $roomInfo['roomSID']=$roomSid;
            $roomInfo['operatorId']=Auth::user()->id;

        }
        catch(\Exception $e)
        {
            $this->LogIncidents($roomInfo,1,'Jastok file uploading Testing Room id', $e->getMessage());
        }
    }
    public function composVideos() {
        $today = date("Y-m-d"). " 00:00:00";
        $session   = VideoCall::where('allow_recording',1)->whereNull('composition_sid')
                        ->whereNull('composition_status')->whereNull('deleted_at')
                        ->where('call_end_date_time','>',$today)->get();
        $keys = API::get()->first();
        $client = new Client($keys->twilio_account_sid, $keys->twilio_account_token);
        
        foreach($session as $val){
            if($val->allow_recording == 1){
                // For Video Start
                $uri = "https://video.twilio.com/v1/Rooms/" . $val->room_sid . "/Participants";
                $response = $client->request("GET", $uri);
                $mediaLocation = $response->getContent();
                $participants = $mediaLocation['participants'];
                foreach ($participants as $key => $participant) {
                    $participantDetails = ParticipantDetails::where('room_sid', $val->room_sid)
                                    ->where('identity', $participant['identity'])
                                    ->first();
                    if (!empty($participantDetails) && $participantDetails) {
                        $participantDetails->participant_id = $participant['sid'];
                        $participantDetails->save();
                    }
                }
                if(is_null($val->composition_sid)){
                    $audio = $video = array();
                    $parDetail = ParticipantDetails::where('room_sid',$val->room_sid)
                                ->pluck('participant_id','type')->toArray();

                    if(!empty($parDetail['operator']) && !empty($parDetail['customer'])){
                        foreach($parDetail as $key => $value){
                            $recordings = $client->video->v1->recordings
                                                ->read(["groupingSid" => [$value]],20 );

                            foreach ($recordings as $record) {
                                if($record->type == 'audio'){
                                    $audio[$key] = $record->sid;
                                }else if($record->type == 'video'){
                                    $video[$key] = $record->sid;
                                }
                            }
                        }
                    }
                    if(count($audio) == 2 && count($video) == 2 && !empty($audio['operator']) &&
                    !empty($audio['customer']) && !empty($video['customer']) &&
                    !empty($video['customer']) ){
                        DB::beginTransaction();
                        try{
                            $composition = $client->video->compositions->create($val->room_sid, [
                                            'audioSources' => array($audio['operator'],$audio['customer']),
                                            // 'videoLayout' =>  array(
                                            //     'main' => array (
                                            //       'z_pos' => 1,
                                            //       'video_sources' => array($video['operator'])
                                            //     ),
                                            //     'pip' => array(
                                            //       'z_pos' => 2,
                                            //       'width' => 240,
                                            //       'height' => 180,
                                            //       'video_sources' => array($video['customer'])
                                            //     )
                                            //   ),
                                'statusCallback' => url('getVideoCallBack'),
                                'resolution' => '640x480',
                                'format' => 'mp4'
                            ]);
                            VideoCall::where('room_sid',$val->room_sid)->update(['composition_sid'=>  $composition->sid, 'composition_status'=>$composition->status]);
                            // $videoCall   = VideoCall::find($val->id);
                            // $videoCall->composition_sid    = $composition->sid;
                            // $videoCall->composition_status = $composition->status;
                            // $videoCall->save();
                            DB::commit();
                        }catch (Exception $e) {
                            DB::rollback();
                            $this->LogIncidents("",1, 'Jastok Video Log Testing2'.$val->id, $e->getMessage());
                        }

                    }
                }

            }
        }
    }

    public function storeVideos() {
        $session   = VideoCall::where('allow_recording',1)->whereNotNull('composition_sid')
                        ->where('composition_status','enqueued')
                        ->whereNull('deleted_at')
                        ->get();
        $keys = API::get()->first();
        $client = new Client($keys->twilio_account_sid, $keys->twilio_account_token);
        foreach($session as $val){
            if(is_null($val->file) && !is_null($val->composition_sid)){
                $uri = "https://video.twilio.com/v1/Compositions/".$val->composition_sid."/Media/?Ttl=3600";
                $response = $client->request("GET", $uri);
                $mediaLocation = $response->getContent();

                if(count($mediaLocation) == 1 && isset($mediaLocation['redirect_to'])){
                    $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.mp4';
                    $redirect = $mediaLocation['redirect_to'];

                    $videoCall = VideoCall::find($val->id);
                    $videoCall->file               = $fileName;
                    $videoCall->composition_status = 'Completed';
                    $videoCall->save();

                    Storage::disk('s3')->put($fileName, file_get_contents($redirect));
                    $client->video->compositions($val->composition_sid)->delete();
                    $recordings = $client->video->v1->rooms($val->room_sid)->recordings->read([], 20);
                    foreach ($recordings as $record) {
                        $client->video->v1->recordings($record->sid)->delete();
                    }
                }
            }
        }
    }  

    /** 
     * this function check max participant for room 
     * @param  userId, $roomId
     * @return Bool
     */
    public function checkMaxParticipant($roomId,$userId = 0) 
    {
        $userList = VideoCall::where('room_sid',$roomId)->whereNotNull('added_by')->pluck("added_by")->toArray();
        $valueCount = array_count_values($userList);
        if(count($userList) >= 3 || (isset($valueCount[$userId]) && $valueCount[$userId] >=1  )) {
            return false;
        }
        return true;
    }

    /** 
     * disconnect participants 
     * @return status
     */
    public function disConnectParticipants(Request $request) 
    {
        $roomId = $request->roomSID; 
        $userId = $request->userId;
        
        $this->updateDuration($request);
        $participant = ParticipantDetails::where('room_sid',$roomId)->where('app_user_id',$userId)->get()->last();
        $room = VideoCall::where('room_sid',$roomId)->first();
        if(empty($participant) || empty($room)) {
            return \Response::json(['error'=>"no record found"],404);
        }
        $status = 'disconnected';
        try {
            $client = new Client($this->sid, $this->token);
            $response =  $client->video->v1->rooms($room->room_name)
                ->participants($participant->identity)
                ->update(array("status" => "disconnected"));
           // $this->LogIncidents("",0, 'Jastok participants disconnected of  Room id'.$request->roomSID,"success");

        } catch(\Exception $e) {
            $this->LogIncidents("",1, 'Jastok participants disconnected of  Room id'.$request->roomSID, $e->getMessage());
            
        }
        $redirect="";
        $meeting = \App\Meeting::where('video_call_id',$room->id)->where('app_user_id',$request->userId)->first();
        if(!empty($meeting)) {
            $redirect  = route('success-meeting-screen',['meeting_id'=>$meeting->id]);
        }
        return \Response::json(['success'=>$status,'url'=>$redirect],200);
    }

    /** 
     * update duration of app user
     */
    public function updateDuration(Request $request,$type = 'cust') 
    {
        $roomId = $request->roomSID; 
        $userId = auth()->check() ? auth()->user()->id : $request->userId;
        $duration = $request->calDur;
        $participant = ParticipantDetails::where('room_sid',$roomId); 
        if($type == 'cust') {
            $participant->where('app_user_id',$userId);
        } else {
            $participant->where('type',"operator");
        }
        $participant = $participant->get()->last();
        if(empty($participant)) {
            return ;
        }
        $leaveTime = Carbon::now()->subSeconds(7);
        $participant->leave_time = $leaveTime;
        try{
            $start  = new Carbon($participant->join_time);
            $end    = new Carbon($leaveTime);
            $participant->duration = $end->diffInSeconds($start);
        } catch(\Exception $e) {

        }
      //  $participant->duration = $this->convertTimeToSecond($duration); 
        if($participant->save()) {
            
        }
        if($type == 'op') {
            try {
                $allParticipants = ParticipantDetails::where('room_sid',$roomId)->where('duration',0)->get();
                if($allParticipants->count() > 0) {
                    foreach($allParticipants as $participantA) {
                        $leaveTime = empty($participantA->leave_time) ? Carbon::createFromFormat('Y-m-d H:i:s', now()) : $participantA->leave_time;
                        $start  = new Carbon($participantA->join_time);
                        $end    = new Carbon($leaveTime);
                        $participantA->duration = $end->diffInSeconds($start);
                        $participantA->save();
                    }
                }
            } catch(\Exception $e) {

            }
        }
        return false;
    }

    /** 
     * start a meeting
     * @param meetingId
     */
    public function startMeeting(Request $request,$id) 
    {
        try{
            $meetingData = \App\Meeting::findOrFail($id);
            $getAccountData = (new \App\Helpers\CommonHelper)->getAccountData();
            $this->middleware('auth');
            $host = $request->getHttpHost();
            $randomRoomName = 'room_meeting_' . str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT) . "_" . base64_encode($meetingData->phone);
            
            $client = new Client($this->sid, $this->token);
            $exists = $client->video->rooms->read(['uniqueName' => $randomRoomName, 'status' => 'completed', 'in-progress']);
            $recordParticipantsOnConnect = true;
            if($getAccountData){
                $recordParticipantsOnConnect = ($getAccountData->require_rec == 1)?true:false;
            }
            if (empty($exists)) {
                $createdRoom = $client->video->rooms->create([
                    'uniqueName' => $randomRoomName,
                    'type' => 'group',
                    'recordParticipantsOnConnect' => $recordParticipantsOnConnect,
                    "type" => 'group-small'
                ]);
                $roomSid = $createdRoom->sid;
                $type = 'operator';
                if($roomSid != ''){
                    $appUserId = $meetingData->app_user_id;
                    $roomId = $this->addCallLog($appUserId, $roomSid,Auth::user()->id,$randomRoomName, $meetingData->default_lang,"op_".Auth::user()->id);
                    $joinSessionOption = array();
                    $meetingData->video_call_id = $roomId;
                    $meetingData->status = 2;
                    $meetingData->save();
                    $userEmail = '';
                    // Cookie::forget('locale' . Auth::user()->id);
                    // Cookie::queue('locale' . Auth::user()->id, $request->app_user_lang, 43800);
                    return redirect()->action('VideoRoomController@joinRoom', [
                        'type' => $type,
                        'roomSID' => $roomSid,
                        'customerID' => $appUserId
                    ]);

                    }
                } else {
                    return redirect()->back()->with('duplicate', 'Enter Name is already exists,Please try with different!');
                }
        } catch (\Exception $e) {
            if ($e->getCode() == 21211) {
                \Session::flash('error', __('Phone Number is not valid, Please enter valid Phone Number ...'));
                VideoCall::where('room_sid',$roomSid)->where('app_user_id',$appUserId)->delete();
               // AppUser::find($appUserId)->delete();
                return redirect()->route('sessionListing');
            } else {
                \Session::flash('error', __('Oppps! something went wrong while start new session, please try again ...'));
                return redirect()->route('sessionListing');
            }
        }
    }
    /** 
    * create new room and update last room 
    */
    public function createUpdateRoom($roomSID)
    {
        $existingRoom = VideoCall::where('room_sid',$roomSID)->first();
        
        if(empty($existingRoom)) {
            return false;
        }
        $client = new Client($this->sid, $this->token);
        $randomRoomName = 'room_switch_' . str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT) . "_" . base64_encode(time());
        $createdRoom = $client->video->rooms->create([
            'uniqueName' => $randomRoomName,
            'type' => 'group',
            'recordParticipantsOnConnect' => (bool)$existingRoom->allow_recording,
            "type" => 'group-small'
        ]);
        $roomSid = $createdRoom->sid;
        $updatedData = [    
            "switched_room_sid"=> $roomSid,
            'is_room_switch' => 1
        ];
        if(VideoCall::where('room_sid',$roomSID)->update($updatedData)) {
            $obj = new VideoCall();
            $obj->operator_id = $existingRoom->operator_id;
            $obj->room_sid = $roomSid;
            $obj->room_name = $randomRoomName;
            $obj->app_user_id = $existingRoom->app_user_id;
            $obj->call_status = 1;
            $obj->allow_recording = $existingRoom->allow_recording ;
            $obj->allow_location = $existingRoom->allow_location;
            $obj->account_id = $existingRoom->account_id;
            $obj->app_user_lang = $existingRoom->app_user_lang;
            $obj->added_by = $existingRoom->added_by;
            $obj->latitude = $existingRoom->latitude; 
            $obj->longitude = $existingRoom->longitude;
            $obj->address = $existingRoom->address; 
            $obj->save();
            $roomId =  $obj->id;
            \App\Meeting::where('video_call_id',$existingRoom->id)->update(["video_call_id"=>$roomId]); 
            return response()->json(['switched'=>1,'msg'=>__("Switching a room wait...")], 200);
        }
        return response()->json(['switched'=>0], 404);
    }
    public function userMediaError(Request $request)
    {
        $videocall=VideoCall::where('room_sid',$request->roomid)->first();
        $videocall->is_media_error=$request->mediaerror;
        $videocall->is_ios=$request->isios;
        $videocall->save();
      
            // if(($request->mediaerror==0) && ($request->mediatype=="audio"))
            // {
            //     $this->LogIncidents($request->roomid,0,"user allowed microphone permission","success");

            // }
            // elseif (($request->mediaerror==1) && ($request->mediatype=="audio")) {
            //     $this->LogIncidents($request->roomid,0,"user denied microphone permission","success");
               
            // }
            // elseif (($request->mediaerror==0) && ($request->mediatype=="video")) {
            //     $this->LogIncidents($request->roomid,0,"user allowed camera permission","success");
            // }
            // else{
            //     $this->LogIncidents($request->roomid,0,"user denied camera permission","success");

            // }
        
     
        echo response()->json(["message"=>$request->mediaerror]);
    }
}
