@extends('app_user.app_user_header')

@section('app_user_content')

<section class="main_section bg_color scheduler_screen">
    
<div class="join_session final_class">
    <div class="loader-container">
        <div class="loader loader-join"></div>
    </div>
        <input type="hidden" name="meetingId" class="meetingId" id="meetingId" value="{{@$meetingId}}">
        <a href="#" class="logo custom-col">
            @if(isset($account_data->logo) && $account_data->logo != '')
                <img class="img-fluid px-3" src="{{url('uploads/'.$account_data->logo)}}"></a>
            @else
                <img class="img-fluid px-3" src="{{url((new \App\Libraries\GeneralSetting)->getSettingInfo()['app_logo'])}}"></a>
            @endif
        <div class="join_Session_text custom-col">
            {!!(@$page_content->value)!!}
        </div>
        <style type="text/css" media="screen">
            body, html, .main_section{
                height: 100vh;
            }
            .main_section{
                padding: 0;
            }
        </style>
        
        <button type="button" class="btn btn-info btn-lg " ><a class="edit_meeting" href="{{route('bookingChangeDate',['schedule_id' => @$scheduleId,'meeting_id' => @$meetingId])}}" style="color:white !important;">{{__('Change Date')}}</a></button>
        <button type="button" class="btn btn-danger btn-lg cancel_meeting" data-toggle="modal" data-target="#cancelModal">{{__('Cancel Meeting')}}</button>
    </div>
</section>

<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                {{__("Are you sure you want to cancel a meeting?")}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><a href="{{route('cancelMeeting',@$meetingId)}}" style="color:white !important;">{{__("Yes")}}</a> </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__("No")}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('app_user_script')
    <script type="text/javascript">
        $(document).ready(function(){
            const urlParams = new URLSearchParams(window.location.search);
            const myParam = urlParams.get('action');
            console.log(myParam);
            if(myParam == 'edit') {
               redirectTo = $(".edit_meeting").prop("href");
               window.location.href = redirectTo;
            }
            if(myParam == 'cancel') {
                $(".cancel_meeting").trigger("click");
            } 

            setInterval(function() {
                ajaxCall();
            },3000);

            function ajaxCall(operatorId , accountId){
                var meetingId = $(".meetingId").val();
                var route = "{{ route('meeting-status', ':id') }}";
                route = route.replace(':id', meetingId);
                console.log(route);
                $.ajax({
                    url: route,
                    type: "GET",
                    dataType: 'json',
                    success: function(resp){
                        console.log('------------')
                        console.log(resp.redirect_url)
                        if(typeof resp.html != 'undefined' && resp.html != "" ) {
                            $(".join_Session_text").html(resp.html);
                        }else if(typeof resp.redirect_url !='undefined' && resp.redirect_url != '' && location.href != resp.redirect_url ){
                            console.log(resp.redirect_url);
                            window.location.href = resp.redirect_url;
                        }
                    }
                });
            }
        });
    </script>
@endsection