@extends('layouts.header')
@section('content')
 <!-- Main section-->
<!---Side Bar End-->
      <!-- Main section-->
      <section  id="sectionManager" class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
          <form id="addAccount" name="addAccount" action="{{route('accounts.store')}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="content-heading">
               <div class="heading">{{__('Accounts')}}/{{__('New Account')}}</div>
               <div class="ml-auto">
                    <button class="btn btn-success btn-lg  " type="submit">{{__('Add')}}</button>
                    <button class="btn btn-danger btn-lg back-btn" type="button" onclick="goBack()">{{__('Back')}}</button>
               </div>
            </div>
            <div class="card" role="tabpanel">
               <div class="tab-content p-0 bg-white">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="row p-4">
                           <div class="col-sm-6">
                            <!-- START card-->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Name')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control @error('name') is-invalid @enderror borderLeft"  name="name" type="text"  value="{{old('name')}}" required>
                                                    @if ($errors->has('name'))
                                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>                                              
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Default Language')}} <sup class="text-danger">*</sup></label>
                                                    <select class="login_lang_dropdown_list form-control  borderLeft" required name="language">
                                                        @foreach($languages as $lang)
                                                           <option {{$lang->is_default == 1 ? 'selected' : ''}}  value="{{$lang->slug}}">{{__($lang->name)}}</option>
                                                        @endforeach 
                                                     </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Account Type')}} <sup class="text-danger">*</sup></label>
                                                    <select class="account_type form-control  borderLeft" required name="account_type">
                                                      <option  value="health_care">{{__('Health Care')}}</option>
                                                      <option  value="insurance">{{__('Insurance' )}}</option>
                                                      <option  value="hls">{{__('HLS')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Email')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control @error('email') is-invalid @enderror borderLeft"  name="email" type="email"  value="{{old('email')}}" required>
                                                    @if ($errors->has('email'))
                                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>                                              
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="checkbox-inline"><input name="require_rec" type="checkbox" value="1">{{__('Allow Recording')}}</label>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="checkbox-inline"><input name="require_loc" type="checkbox" value="1">{{__('Allow Location')}}</label>
                                                </div>                                            
                                            </div>
                                            
                                            <div class="col-sm-12 ">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Logo')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control dropify @error('logo') is-invalid @enderror borderLeft"  type="file" name="logo" id="input-file-now" data-allowed-file-extensions="jpg png jpeg" >
                                                    
                                                    @if ($errors->has('logo'))
                                                        <span class="text-danger">
                                                            {{ $errors->first('logo') }}
                                                        </span>
                                                    @endif
                                                </div>                                            
                                            </div>
                                        </div>                                           
                                    </div>                                           
                               </div><!-- END card-->
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
              </form>
         </div>
      </section>
      
@endsection

@section("script") 
      <script >
          $(document).ready(function(){
                $("#addAccount").validate({
                    errorClass:'text-danger'
                });
          });
      </script>
@endsection