@extends('layouts.header') 
@section('content')

<!-- Main section-->
<section id="sectionManager" class="section-container">
    <!-- Page content-->
    <div class="content-wrapper">
        <div class="content-heading">
            <div class="heading">{{__('Accounts')}}</div>
            
            <div class="ml-auto">
                <a href="{{route('accounts.create')}}" class="mb-1 btn btn-info">{{__('Add New Account')}}</a>
            </div>
        </div>

        <!-- START cards box-->
        <div class="card m-3">
            @if (Session::has('status') && session('status') == 200 )
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{session('msg')}}
                </div>
            @endif
            @if (Session::has('status') && session('status') == 404 )
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{session('msg')}}
                </div>
            @endif
            <div class="card-body p-0">
                <!-- Datatable Start-->
                <table class="table table-striped my-4 w-100" id="accountTable">
                    <thead>
                    <tr>
                        <th>{{__('Id')}}</th>
                        <th>{{__('Name')}}</th>
                        <th>{{__('Email')}} </th>
                        <th>{{__('Allow Recording')}}</th>
                        <th>{{__('Allow Location')}}</th>
                        <th>{{__('Status')}}</th>
                        <th>{{__("Created DateTime")}}</th>
                        <th></th>
                    </tr>
                    </thead>
                    
                </table>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $("#accountTable").DataTable({
            serverSide: true,
            ajax: "{{ route('accountsListing') }}",
            columns: [
                 { name: 'id', data:'id' }, 
                 { name: 'name', data:'name' },
                 { name: 'email',data:'email' },
                 { name: 'require_rec',data:'require_rec', "render": function(data){
                        return (data == 1) ? 'Yes' : 'No';
                    } 
                 },
                 { name: 'require_loc', data:'require_loc',"render": function(data){
                        return (data == 1) ? 'Yes' : 'No';
                    }
                 },
                 { name: 'status',data:'status' ,  orderable: false,"render": function(data){
                        return (data == 1) ? 'Active' : 'Inactive';
                    }
                 },
                 { name: 'created_at',data:'created_date' },
                 {name:'edit_account', data:'edit_account',orderable: false,    "render": function(data, type, full, meta){
                    return '<a class="btn-icon btn btn-info" href="'+data+'" ><i class="fa fa-edit"></i></a>';
                }
            },
            ],
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            'order': [[0,'asc'],[5,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
             "infoCallback": function( settings, start, end, max, total, pre ) {
              return "{{__('Showing')}} "+start +" {{__('to')}} "+ end+ " {{__('of')}} " +total+ " {{__('entries')}}";
            },
            processing: true,
            oLanguage: {
               sEmptyTable: "{{__('Empty Data Dable')}}",
               sZeroRecords: "{{__('No records')}}",
                sLengthMenu: '_MENU_ {{__("records per page")}}',
                sSearch: "{{__('Search')}}",
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: "{{__('No records available')}}",
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                },
                sProcessing: "{{__('Loading...')}}",
            }
        });
    }); 
</script>
@endsection