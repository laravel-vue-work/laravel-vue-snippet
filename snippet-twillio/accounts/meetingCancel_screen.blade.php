@extends('app_user.app_user_header')

@section('app_user_content')

<section class="main_section bg_color scheduler_screen">
    
<div class="join_session final_class">
    <div class="loader-container">
        <div class="loader loader-join"></div>
    </div>
        <a href="#" class="logo custom-col">
            @if($account_data->logo != '')
                <img class="img-fluid px-3" src="{{url('uploads/'.$account_data->logo)}}"></a>
            @else
                <img class="img-fluid px-3" src="{{url((new \App\Libraries\GeneralSetting)->getSettingInfo()['app_logo'])}}"></a>
            @endif
        <div class="join_Session_text custom-col">
            <h4>{!!(@$page_content->value)!!}</h4>
        </div>
        <style type="text/css" media="screen">
            body, html, .main_section{
                height: 100vh;
            }
            .main_section{
                padding: 0;
            }
            .cmn-btn {
                width: 100%;
                border-radius: 50px;
                height: 70px;
                font-size: 30px;
                font-weight: 300;
                margin-top: 50px;
                cursor: pointer;
                margin-bottom: 60px;
                text-align: center;
                display: inline-flex;
                justify-content: center;
                align-items: center;
            }
        </style>
           <div name="join session" class="cmn-btn btn-success" id="joinSession"><a class="text-white" href="{{route('accountSessionManagerList',@$account_data->url_name)}}">{{__('BOOK A SESSION')}}</a></div>
    </div>
</section>
@endsection
@section('app_user_script')
@endsection