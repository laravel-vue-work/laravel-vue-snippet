@extends('layouts.header')
@section('content')
 <!-- Main section-->
 <section  id="sectionManager"class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-heading">
               <div class="heading">{{__('Account')}} / {{__('Status Settings')}} </div><!-- START Delete Btn -->
               <div class="ml-auto">
                  <a href="{{route('accountInsertStatusSettings')}}"><button class="btn btn-info btn-lg" type="button">{{__('New')}}</button></a>
               </div><!-- END  Delete Btn-->
            </div><!-- START cards box-->
            <!-- Table Card Start-->
            <div class="card m-3">                                      
                  <div class="card-body p-0">
                     <!-- Datatable Start-->
                     <div class="table-responsive">
                     <table class="table table-striped my-4 w-100" id="datatable1">
                        <thead>
                           <tr>
                              <th data-priority="1">{{__('Id')}}</th>
                              <th>{{__('Status name')}}</th>
                              <th>{{__('Entity')}}</th>
                              <th>{{__('Text color')}}</th>
                              <th style="width:160px"></th>
                           </tr>
                        </thead>
                        <tbody>
                         @foreach($statusData as $status)
                           <tr class="gradeX">
                              <td  class="text-capitalize">{{$status->id}}</td>
                              <td class="text-capitalize font-weight-bold" style="color:{{$status['color_code']}};">{{$status['status_name']}}</td>
                              <td class="text-capitalize">{{__($status->entity)}}</td>
                              <td class="text-capitalize"><div class="p-3 rounded" style="width: 20px; background-color: {{$status['color_code']}};"></div></td>
                              <td class="text-right" style="white-space: nowrap;">
                                    <button class="mb-1  px-2 py-1 btn  ml-2  btn-danger deleteOnElement" data-route="{{route('accountDeleteStatusSettings',$status->id)}}" type="button">
                                        <i class="fa fa-trash"></i> 
                                    </button>
                                    <a href="{{route('accountEditStatusSettings',$status->id)}}">
                                      <button class="mb-1 px-2 py-1 btn btn-info" data-route="" type="button">
                                          <i class="fa fa-edit"></i> 
                                      </button>
                                    </a>
                                   </td>
                           </tr>
                           @endforeach 
                        </tbody>
                     </table>
                     </div>
                       <!-- Datatable Start-->
                  </div>
               </div>
                 <!-- Table Card End-->
         </div>
      </section>
      
@endsection