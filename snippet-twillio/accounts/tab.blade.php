@extends('layouts.header')
@section('content')
 <!-- Main section-->
<!---Side Bar End-->
      <!-- Main section-->
      <section  id="sectionManager" class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
          <div class="content-heading">
               <div class="heading">{{__('Account')}}/<span class="dynamic_title">{{__('Edit Account')}}</span></div>
            </div>
            <div class="" role="tabpanel">
                
                <input type="hidden" id="accountId" value="{{$id}}" >
                <ul class="nav nav-tabs nav-fill pt-3 pr-3 pl-3 bg-white">
                    <li class="account_tab nav-item" data-action="account"><a class="nav-link bb0 active" data-toggle="tab" aria-controls="account" role="tab" data-toggle="tab" href="#account">{{__('General Settings')}}</a></li>
                    @if(!isset($statusChange))
                        <li class="account_tab nav-item" data-action="permission"><a class="nav-link bb0" data-toggle="tab" aria-controls="peremissions" role="tab" data-toggle="tab" href="#peremissions">{{__('Permissions')}}</a></li>
                    @endif
                    <li class="account_tab nav-item" data-action="app_content"><a class="nav-link bb0" data-toggle="tab" aria-controls="app_content" role="tab" data-toggle="tab" href="#app_content">{{__("App Content")}}</a></li>
                    <li class="account_tab nav-item" data-action="session_status"><a class="nav-link bb0" data-toggle="tab" aria-controls="session_status" role="tab" data-toggle="tab" href="#session_status">{{__("Session Status")}}</a></li>
                    @if($account->require_sch == 1)
                      <li class="account_tab nav-item" data-action="schedule_management"><a class="schedule_management nav-link bb0" data-toggle="tab" aria-controls="schedule_management" role="tab" data-toggle="tab" href="#schedule_management">{{__('Schedule Management')}}</a></li>
                    @endif
                    <li class="account_tab nav-item" data-action="operator_types"><a class="nav-link bb0" data-toggle="tab" aria-controls="operator_types" role="tab" data-toggle="tab" href="#operator_types">{{__("Operator Types")}}</a></li>
                    
                </ul>
               <div class="tab-content p-0 ">
                    <div class="tab-pane active" id="accountTabContent" role="tabpanel">
                       
                     </div>
                  </div>
         </div>
      </section>
      
@endsection

@section("script") 
      <script >
          $(document).ready(function(){
                //$("#editAccount").validate();
                $(document).on("click",".require_sch",function(){
                    if($(this).prop("checked")) {
                        $(".scheduler_chk").removeClass("hide");
                    } else {
                        $(".scheduler_chk").addClass("hide");
                    }
                    $(".allow_sms_email").prop("checked",false);
                });
                $(".account_tab").on('click',function(){
                    $(".account_tab").removeClass('active');
                    $(this).addClass('active');
                    $("#accountTabContent").html("");
                    var tabName = $(this).attr('data-action');
                    var id = $("#accountId").val();
                    var postData = {
                        tabName,id
                    }
                    $.post("{{route('accountTabContent')}}",postData,
                    function(data){
                        console.log($("#accountTabContent"));
                        $(".dynamic_title").html(data.page_title);
                        $("#accountTabContent").html(data.html);
                    });
                });
                $(".account_tab").first().trigger("click");

        $(document).on("click",".editStatus",function(){
            var url = $(this).attr("data-action");
            var reqData = {
                account_id: $("#accountId").val()
            };
            $.get(url,reqData,function(data){
                $(".status_edit_add_model_content").html(data.html);
                $("#statusAddEdit").modal('show');
            });
        });

        $(document).on("click",".newStatus",function(){
            console.log(this);
            var url = $(this).attr("data-action");
            var reqData = {
                account_id: $("#accountId").val()
            };
            $.get(url,reqData,function(data){
                $(".status_edit_add_model_content").html(data.html);
                $("#statusAddEdit").modal('show');
            });
        });

        $(document).on("click",".newOperatorTypes, .editOperatorTypes",function(){
            console.log(this);
            var url = $(this).attr("data-action");
            var reqData = {
                account_id: $("#accountId").val()
            };
            $.get(url,reqData,function(data){
                $(".operator_types_edit_add_model_content").html(data.html);
                $("#operatorTypeAddEdit").modal('show');
            });
        });

        $("body").on('submit',"#operatorTypeEditForm, #operatorTypeAddForm",function(event){
            event.preventDefault();
            var url = $(this).prop('action'); 
            var postData = $(this).serialize();
            $.post(url,postData,
                function(data){
                $("#operatorTypeAddEdit").modal('hide');
                $(".account_tab .active").trigger('click');
                swal(data.msg);
            })
            .fail(function(error){
                swal(__("Something went wrong."));
            });
        });

        $("body").on('submit',"#statusEditForm, #statusAddForm",function(event){
            event.preventDefault();
            var url = $(this).prop('action'); 
            var postData = $(this).serialize();
            $.post(url,postData,
                function(data){
                $("#statusAddEdit").modal('hide');
                $(".account_tab .active").trigger('click');
                swal(data.msg);
            })
            .fail(function(error){
                swal(__("Something went wrong."));
            });
        }); 
        $("body").on('submit',"#editAccount",function(event){
              event.preventDefault();
              var url = $(this).prop('action'); 
              var postData = $(this).serialize();
              var formData = $("#editAccount").submit(function (e) {
                return;
              });
              var data = new FormData(formData[0]);
              console.log(data);
                $.ajax({
                    url: url, // NB: Use the correct action name
                    type: "POST",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        swal(data.msg);
                        
                    },
                    error: function(response) {
                        swal(__("Something went wrong."));
                    }
                });

            //   $.post(url,postData,
            //       function(data){
            //         swal(data.msg);
            //   })
            //   .fail(function(){
            //     swal(__("Something went wrong."));
            //   });
          })
          
          $("body").on('submit',"#editPermission",function(event){
                  event.preventDefault();
                  var url = $(this).prop('action'); 
                  var postData = $(this).serialize();
                  $.post(url,postData,function(data){  
                        swal(data.msg);
                        if(($("input[name='require_sch']").prop("checked") && $(".schedule_management").length == 0) || (!$("input[name='require_sch']").prop("checked") && $(".schedule_management").length == 1) ) {
                            window.location.reload();
                        }
                   })
                .fail(function(){
                        swal(__("Something went wrong."));
                 });
                })
          });

          $("body").on('submit',"#editSchedule",function(event){
              event.preventDefault();
              var url = $(this).prop('action'); 
              var postData = $(this).serialize();
              var formData = $("#editSchedule").submit(function (e) {
                return;
              });
              var data = new FormData(formData[0]);
              console.log(data);
                $.ajax({
                    url: url, 
                    type: "POST",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        swal(data.msg);
                    },
                    error: function(response) {
                        swal(__("Something went wrong."));
                    }
                });
          })
      </script>
@endsection