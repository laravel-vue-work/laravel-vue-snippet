<form id="operatorTypeAddForm" action="{{route('operator-types.store')}}" method="post">
  @csrf
  <div class="row">
      <div class="col-sm-12">
          <div class="col-sm-12">
              <div class="form-group">
                  <label class="col-form-label">{{__('Title')}} </label>
                  <input class="form-control @error('title') is-invalid @enderror borderLeft" value="{{old('title')}}" type="text" name="title" required />
                  @error('title')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>
          <input type="hidden" value="{{$account_id}}" name="account_id" />

          <div class="ml-auto pull-right">
              <button class="btn btn-success btn-lg" type="submit">{{__('Save')}}</button>
          </div>
      </div>
  </div>
</form>
