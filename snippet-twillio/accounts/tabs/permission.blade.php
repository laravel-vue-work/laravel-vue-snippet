<div class="content-wrapper">
    <form  id="editPermission" name="editPermission" action="{{route('accounts.update',$account->id)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
    <div class="row p-4">
        <div class="col-sm-6">
         <!-- START card-->
             <div class="card">
                 <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="checkbox c-checkbox">
                                    <label class="pl-0">
                                        <input name="require_rec" type="checkbox" value="1" {{$account->require_rec  == 1 ? 'checked' : ''}}>
                                        <span class="fa fa-check"></span> 
                                        {{__('Allow Recording')}}
                                    </label>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="checkbox c-checkbox">
                                    <label class="pl-0">
                                        <input name="require_loc" type="checkbox" value="1" {{$account->require_loc == 1? 'checked' : ''}}>
                                        <span class="fa fa-check"></span> 
                                        {{__('Allow Location')}}
                                    </label>
                                </div>
                            </div>     
                            <div class="form-group">
                                <div class="checkbox c-checkbox">
                                    <label class="pl-0">
                                        <input class="require_sch" name="require_sch" type="checkbox" value="1" {{$account->require_sch == 1? 'checked' : ''}}>
                                        <span class="fa fa-check"></span> 
                                        {{__('Schedule Management')}}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group scheduler_chk {{($account->require_sch == 1) ? '' : 'hide'}}" >
                                <div class="checkbox c-checkbox">
                                    <label class="pl-0">
                                        <input class="allow_sms_email" name="allow_email" type="checkbox" value="1" {{$account->allow_email == 1? 'checked' : ''}}>
                                        <span class="fa fa-check"></span> 
                                        {{__('Send verification OTP code for schedule login page via Email')}}
                                    </label>
                                </div>
                            </div>       
                            <div class="form-group scheduler_chk {{($account->require_sch == 1) ? '' : 'hide'}}"">
                                <div class="checkbox c-checkbox">
                                    <label class="pl-0">
                                        <input class="allow_sms_email" name="allow_sms" type="checkbox" value="1" {{$account->allow_sms == 1? 'checked' : ''}}>
                                        <span class="fa fa-check"></span> 
                                        {{__('Send verification OTP code for schedule login page via SMS')}}
                                    </label>
                                </div>
                            </div>  
                        </div>
                         <div class="col-sm-12">
                            <button class="btn btn-success btn-lg  pull-right" type="submit">{{__('Save')}}</button>
                         </div>
                     </div>                                           
                 </div>                                           
            </div><!-- END card-->
              </div>
           </div>
     </div>
    </form>
    </div>