<section class="section-container dashboard">
    
    @if(session('permission'))
        <div class="alert alert-danger" role="alert">
            {{ session('permission') }}
        </div>
    @endif

   <div class="content-wrapper">
      <div class="content-heading">
         <div>{{__('Dashboard')}}  </div>
      </div>
      @if (Session::has('change'))
      <div class="alert alert-{{session('class')}} alert-dismissible text-center">
         <button type="button" class="close" data-dismiss="alert">&times;</button>
         {{ session('change') }}
      </div>
      @endif
        <section class="content">
            <div class="p-3">
                <div class="row">
                    <div class="col-lg-3 col-6 call-time-card pb-3">
                      <div class="small-box bg-info">
                        <div class="row no-gutters overflow-hidden card-part">
                            <div class="col-4 p-3 d-flex align-items-center justify-content-center">
                             <div class="icon">
                                    <i class="far fa-clock"></i>
                              </div>
                            </div>
                            <div class="col-8 py-3 d-flex flex-column justify-content-center">
                                <h3 class="mb-3"> {{$dataArray['totalCallTime'] != 0? cnvStoH($dataArray['totalCallTime']):'00:00'}} H</h3>
                                <p class="mb-0">{{__('Total Call Time')}}</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-6 operators-card pb-3">
                      <div class="small-box bg-info">
                        <div class="row no-gutters overflow-hidden card-part">
                            <div class="col-4 p-3 d-flex align-items-center justify-content-center">
                             <div class="icon">
                             <i class="fas fa-headset"></i>
                              </div>
                            </div>
                            <div class="col-8 py-3 d-flex flex-column justify-content-center">
                                <h3 class="mb-3">{{$dataArray['totalOperator'] != 0? $dataArray['totalOperator']:0}}</h3>
                                <p class="mb-0">{{__('Total Operators')}}</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-6 users-card pb-3">
                      <div class="small-box bg-info">
                        <div class="row no-gutters overflow-hidden">
                            <div class="col-4 p-3 d-flex align-items-center justify-content-center">
                             <div class="icon">
                             <i class="fas fa-user-friends"></i>
                              </div>
                            </div>
                            <div class="col-8 py-3 d-flex flex-column justify-content-center">
                                <h3 class="mb-3">{{$dataArray['totalUsers'] != 0? $dataArray['totalUsers']:0}}</h3></h3>
                                <p class="mb-0">{{__('Total Users')}}</p>
                            </div>
                        </div>
                      </div>
                    </div>
                     <div class="col-lg-3 col-6 session-card pb-3">
                      <div class="small-box bg-info">
                        <div class="row no-gutters overflow-hidden card-part">
                            <div class="col-4 p-3 d-flex align-items-center justify-content-center">
                             <div class="icon">
                                <i class="fas fa-video"></i>
                              </div>
                            </div>
                            <div class="col-8 py-3 d-flex flex-column justify-content-center">
                                <h3 class="mb-3">{{$dataArray['totalSession'] != 0? $dataArray['totalSession']:0}}</h3>
                                <p class="mb-0">{{__('Sessions')}}</p>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="row">
                <div class="{{ (!empty($show_map)) ? 'col-md-6' : 'col-md-12'}}">
                    <div class="latestSessionCard">
                        <div class="card-body">
                            <h4 class="card-title mb-0">{{__('Latest Sessions')}}</h4>
                            <div class="ml-auto">
                                <a href="{{route('sessionListing')}}">
                                    <button class="btn btn-info btn-sm" type="button">{{__('View All')}}</button>
                                </a>
                            </div>
                        </div>
                        @if(count($latestSession)>0)
                        <div class="table-responsive">
                        <table class="table table-striped my-4 w-100" id="latestSessionList">
                          <thead>
                            <tr  class="gradeX">
                               <th class="text-center border-right">{{__('Date')}}</th>
                               <th class="text-center border-right">{{__('User')}}</th>
                               <th class="text-center border-right">{{__('Status')}}</th>
                               <th class="text-center">{{__('Operator')}}</th>
                            </tr>
                         </thead>
                            <tbody>
                                @foreach($latestSession as $latestSessionVal)
                                 <tr class="gradeX">
                                    <td class="border-right">{{(new \App\Helpers\CommonHelper)->formatDate($latestSessionVal->videoDate)}}</td>
                                    <td class="border-right">{{$latestSessionVal->name}}</td>
                                    <td class="border-right" style="color:{{$latestSessionVal->color_code}}">{{$latestSessionVal->status_name}}</td>
                                    <!--<td>{{$latestSessionVal->address}}</td>-->
                                    <td>
                                        <div class="d-flex justify-content-between p-2">
                                            <span>{{$latestSessionVal->operator_name}}</span>
                                            <a href="{{url('edit/session/'.$latestSessionVal->session_id)}}" class="dashboard-edit-session btn btn-icon btn-info">
                                                <i class="fa fa-edit" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        @else
                        <span class="p-4"><h5><center>{{__('No record found')}}!</center></h5></span>
                        @endif
                    </div>
                    <div class="session_chart_card"> 
                        <div class="card-body justify-content-between p-3">
                            <h4 class="card-title mb-0">{{__('Session graph')}}</h4>
                                <div class='input-group date' id='monthStartDate'>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    <input type='text' class="form-control datepicker" name="startDate" class="startDate"/>
                                </div>
                                <div class='input-group date' id='monthEndDate'>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    <input type='text' class="form-control datepicker" name="endDate" class="endDate" />
                                </div>
                                <button class="btn btn-info btn-sm" type="button" id="showMonthlySessionChart">{{__('Show')}}</button>
                        </div>
                        <div class="card-body">
                            <canvas dir="rtl" id="session-chart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 dashboard_card {{ (!empty($show_map)) ? '' : 'hide'}}">
                    <div id="dashboardMap" style="width: 100%; height: 100%;"></div>
                </div>
            </div>
            </div>
        </section>
    </div>
</section>