@extends('layouts.header')
@section('content')
 <!-- Main section-->
<!---Side Bar End-->
      <!-- Main section-->
      <section  id="sectionManager" class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
           <!-- Start ManagerInfo Form --->
          <form action="{{route('addOperators')}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="content-heading">
               <div class="heading">{{__('Operators')}}/{{__('New Operators')}}</div><!-- START Language list-->
               <div class="ml-auto">
                      <button class="btn btn-success btn-lg  " type="submit">{{__('Save')}}</button>
                        <button class="btn btn-danger btn-lg back-btn" type="button" onclick="goBack()">{{__('Back')}}</button>
               </div><!-- END Language list-->
            </div><!-- START cards box-->

               <div class="card" role="tabpanel">
                     <!-- Nav tabs-->
                     <ul class="nav nav-tabs nav-fill" role="tablist">
                        <li class="nav-item" role="presentation">
                           <a class="nav-link bb0 active" href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">
                              {{__('Operators Info')}}
                           </a>
                        </li>                      
                     </ul><!-- Tab panes-->

                     <div class="tab-content p-0 bg-white">
                        <div class="tab-pane active" id="home" role="tabpanel">
                              <div class="row p-4">
                                 <div class="col-sm-6">
                                      <!-- START card-->
                               <div class="card">
                                  <div class="card-body">
                                      <div class="row">
                                          <div class="col-sm-6">
                                              <div class="form-group">
                                                 <label class="col-form-label">{{__('First Name')}}</label>
                                                 <input class="form-control @error('first_name') is-invalid @enderror borderLeft"  name="first_name" type="text"  value="{{old('first_name')}}" required>
                                                 @if ($errors->has('first_name'))
                                                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                                  @endif
                                              </div>                                              
                                          </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Last Name')}}</label>
                                                 <input class="form-control @error('last_name') is-invalid @enderror borderLeft"  name="last_name" type="text" value="{{old('last_name')}}" required>
                                                 @if ($errors->has('last_name'))
                                                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>
                                          @if (Auth::user()->role == 'Top Manager')
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                          <label class="col-form-label">{{__('Manager')}}</label>
                                                          
                                                            <select class="form-control @error('manager') is-invalid @enderror borderLeft {{$roleClass}}" name="manager" required>
                                                              <option value="">{{__('Choose')}}....</option>
                                                              @foreach($manager as $manager)
                                                              <option value="{{$manager->id}}" {{ old('manager_type') == $manager->id ?"selected":""  }} >{{$manager->first_name .' '.$manager->last_name}} </option>
                                                              @endforeach
                                                          </select>
                                                         @if ($errors->has('manager'))
                                                               <span class="text-danger">{{ $errors->first('manager') }}</span>
                                                         @endif
                                                        </div>
                                                      </div>
                                                      @endif
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Phone Number')}}</label>
                                                 <input class="form-control @error('phone') is-invalid @enderror borderLeft"  name="phone" type="phone" value="{{old('phone')}}"  required>
                                                 @if ($errors->has('phone'))
                                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Email Address')}}</label>
                                                 <input class="form-control @error('email') is-invalid @enderror borderLeft"  name="email" type="email" autocomplete="new-email" value="{{old('email')}}" required>
                                                 @if ($errors->has('email'))
                                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>

                                          <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Password')}}</label>
                                                 <input class="form-control @error('password') is-invalid @enderror borderLeft"  name="password" type="password" autocomplete="new-password" required>
                                                 @if ($errors->has('password'))
                                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>
                                         
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Confirm Password')}}</label>
                                                 <input class="form-control @error('confirm_password') is-invalid @enderror borderLeft"  name="confirm_password" type="password" required>
                                                 @if ($errors->has('confirm_password'))
                                                        <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Operator Type')}}</label>
                                                    <select class="form-control op-type @error('op_type') is-invalid @enderror borderLeft" name="op_type" required>
                                                        <option value="">{{__('Choose')}}....</option>
                                                        @foreach($types as $type)
                                                            <option value="{{$type->id}}" {{ old('op_type') == $type->id ?"selected":""  }} >{{$type->title}} </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('op_type'))
                                                        <span class="text-danger">{{ $errors->first('op_type') }}</span>
                                                    @endif
                                            </div>
                                          </div>
                                          <div class="col-sm-6 text">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Avatar Image')}}</label>
                                                 <input class="form-control dropify @error('image') is-invalid @enderror borderLeft"  type="file" name="image" id="input-file-now" data-allowed-file-extensions="jpg png jpeg" >
                                                  @if ($errors->has('image'))
                                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>
                                         
                                      </div>                                           
                                  </div>                                           
                               </div><!-- END card-->
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
              </form>
              <!-- End ManagerInfo Form --->
         </div>
      </section>
      
@endsection

@section('script')
    <script>    
        $(document).ready(function(){
            $(".top_manager_class").on("change",function(){
                managerId  = $(this).val();
                var url  = "{{route('operator-type-by-manager',['id'=>':id'])}}";
                url = url.replace(':id', managerId);
                $(".op-type").html("");
                $.get(url,(response,status)=> {
                    $(".op-type").html(response.html)
                }).fail(function(response) {
                   
                });
            });
        });
    </script>
@endsection