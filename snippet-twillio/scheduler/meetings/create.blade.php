@extends('layouts.header')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment-with-locales.min.js"></script>
@section('content')
<section  id="sectionManager" class="section-container">
    <div class="content-wrapper">
        <span class="extra-data" data-dates="{{json_encode($dateSlots)}}" data-datesuser="{{json_encode($dateSlotsUser)}}" selected_date="{{$meetingDate}}"></span>
        <form name="createMeeting" id="createMeeting" method="post">
        <div class="content-heading">
            <div class="heading">{{__('Schedule Management')}}/{{__('New Meeting')}}</div><!-- START Language list-->
            <div class="ml-auto">
                <button class="btn btn-success btn-lg  " type="submit">{{__('Save')}}</button>
                <a href="{{route('schedulerListing')}}" class="btn btn-danger btn-lg back-btn" >{{__('Back')}}</a>

            </div><!-- END Language list-->
        </div><!-- START cards box-->
        <div class="card" role="tabpanel">
            <div class="tab-content p-0 bg-white">
                <div class="tab-pane active" id="schedule_mail" role="tabpanel">
                    <div class="row p-4">
                        <div class="col-md-8">
                            <div class="ajax_res_msg alert alert-success alert-dismissible hide" role="alert">
                                <strong class="msg_text"></strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="meeting_title font-weight-light mb-4">{{__("Meeting info")}}</h3>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row align-items-center">
                                                <div class="col-md-4">
                                                    <div class="user-info-schedule">
                                                        <img src="{{$operator_profile}}" height="50" width="50"/>
                                                        <div class="content">
                                                            <div >{{$account_name}}</div>
                                                            <div >{{$operator_name}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            <div class="col-md-4">
                                            <div class="">{{_("Meeting time")}}: {{$operator_timezone}}</div>
                                                <div class="">
                                                    <div class="meeting-date"><span class='changeable-date'>{{$meeting_date}}</span>, 
                                                        <span class="meeting-time">
                                                            <select class="start_time_select" required name="start_time">
                                                                @foreach($slots as $k=>$slot) 
                                                                <option value="{{$k}}">{{$slot}}
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-md-4"> 
                                                <div class="form-group ">
                                                    <label for="">{{__("User's Timezone")}}</label>
                                                    <select id="inputState" class="form-control" name="user_timezone">
                                                        @foreach($timezones as $timezone)
                                                            <option {{($operator_timezone == $timezone->timezone) ? "selected" :'' }} value="{{$timezone->timezone}}">{{$timezone->timezone}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="meeting_title font-weight-light mb-4">{{__("User info")}}</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Full Name')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control borderLeft"  name="full_name" type="text"  value="{{old('full_name')}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class=" col-sm-4 ">
                                                        <div class="form-group">
                                                        <label class="col-form-label">{{__('Country Code')}} <sup class="text-danger">*</sup></label>
                                                        <select id="countryCode" name="country_code" class="form-control" required>
                                                            @foreach($country as $co)
                                                            <option value="{{"+".$co->country_code}}" {{$co->country_name_en=="Israel"?"selected":" "}}>{{"+".$co->country_code}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="form-group ">
                                                            <label class="col-form-label">{{__('Phone')}} <sup class="text-danger">*</sup></label>
                                                            <input class="form-control borderLeft"  name="phone" type="text"  value="{{old('phone')}}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Email')}} <sup class="text-danger">*</sup></label>
                                                        <input class="form-control borderLeft"  name="email" type="email"  value="{{old('email')}}" required>
                                                    </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Note')}}</label>
                                                    <textarea rows="5" name="note" class="form-control borderLeft"></textarea>
                                                </div>
                                                </div>
                                            </div> 
                                        </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $(".start_time_select").on("change",function(){
            var val = $(this).val();
            var dateSlot = $(".extra-data").attr("data-dates"); 
            var dateSlotUser = $(".extra-data").attr("data-datesuser");
            dateSlot = JSON.parse(dateSlot);
            dateSlotUser = JSON.parse(dateSlotUser);
            $(".changeable-date").html(dateSlotUser[val]);
            $(".extra-data").attr("selected_date",dateSlot[val]);
        })

        $("#createMeeting").validate({
            errorClass:"text-danger"
        });
        $("#createMeeting").on("submit",function(e){
            e.preventDefault();
            if(!$(this).valid()) {
                return false;
            }
            var data  = $(this).serialize();
            $('.customLoader').show();
            var url = "{{route('meeting.store',['scheduler_id'=>$schedulerId,'date'=>':date'])}}"; 
            date = $(".extra-data").attr('selected_date');
            url = url.replace(':date', date);
            console.log(url);
            $.post(url,data,(response,status)=> {
               $(".msg_text").html(response.msg);
               $(".ajax_res_msg").removeClass("alert-danger").addClass("alert-success").removeClass("hide");
               setTimeout(function(){
                   window.location.href = response.redirect;
               },2000);
               $('.customLoader').hide();
               //$("")
            }).fail(function(response) {
                console.log(response);
                $('.customLoader').hide();
                var msg = response.responseJSON.msg;
                $(".msg_text").html(msg);
                $(".ajax_res_msg").removeClass("alert-success").addClass("alert-danger").removeClass("hide");
                //$(".operator_list").html('');
            });
        })
    });
</script>
@endsection