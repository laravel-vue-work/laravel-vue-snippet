@extends('layouts.header')
@section('content')
<section  id="sectionManager" class="section-container">
    <div class="content-wrapper">
        <form name="createScheduler" id="createScheduler" method="post" autocomplete="off">
        <div class="content-heading">
            <div class="heading">{{__('Schedule Management')}}/{{__('Edit Schedule')}}</div><!-- START Language list-->
            <div class="ml-auto">
                    <button class="btn btn-danger btn-lg delete-schedule" data-toggle="modal" data-target="#deleteScheduleFormModal"   type="button">{{__('Delete')}}</button>
                    <button class="btn btn-success btn-lg  " type="submit">{{__('Save')}}</button>
                    <a href="{{route('schedulerListing')}}" class="btn btn-danger btn-lg back-btn" >{{__('Back')}}</a>

            </div><!-- END Language list-->
        </div><!-- START cards box-->
        <div class="card" role="tabpanel">
            <div class="tab-content p-0 bg-white">
                <div class="tab-pane active" id="schedule_mail" role="tabpanel">
                    <div class="row p-4">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="ajax_res_msg alert alert-success alert-dismissible hide" role="alert">
                                        <strong class="msg_text"></strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                            @if($login_as == 'top_manager')
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Account')}} <sup class="text-danger">*</sup></label>
                                                    <select class="login_lang_dropdown_list     form-control  borderLeft account_change" required name="account_id" >
                                                        @foreach($accounts as $account)
                                                           <option {{($account->id == $scheduler->account_id ? 'selected': '')}}  value="{{$account->id}}">{{__($account->name)}}</option>
                                                        @endforeach 
                                                     </select>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Operator')}} <sup class="text-danger">*</sup></label>
                                                    <select class="login_lang_dropdown_list     form-control  borderLeft operator_list" required name="operator_id">
                                                        
                                                        @foreach($operators as $operator)
                                                           <option  {{$operator->id == $scheduler->operator_id ? 'selected' : ''}} value="{{$operator->id}}">{{$operator->first_name}} {{$operator->last_name}}</option>
                                                        @endforeach 
                                                     </select>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('From Date')}} <sup class="text-danger">*</sup></label>
                                                    <div class='input-group date' id='from_datepicker'>
                                                    <input name="from_date"  type='text' class="form-control" value="{{$scheduler->from_date}}" data-value="{{$scheduler->from_date}}" 
                                                    required
                                                    />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Start Time')}} <sup class="text-danger">*</sup></label>
                                                    <div class='input-group date' id='start_time'>
                                                        <input name="start_time"  type='text' class="form-control" required value="{{$scheduler->start_time}}" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Until Time')}} <sup class="text-danger">*</sup></label>
                                                    <div class='input-group date' id='end_time'>
                                                        <input name="end_time"  type='text' class="form-control" required value="{{$scheduler->end_time}}"  />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Repeat')}}</label>
                                                    <div class="input-group mb-3 new_view_btn">
                                                    <select class="login_lang_dropdown_list     form-control  borderLeft" required name="is_repeat" id="is_repeat">
                                                        <option {{$scheduler->is_repeat == 0 ? 'selected' : ''}} value="0">{{__("No Repeat")}}</option>
                                                        <option {{($scheduler->is_repeat == 1 || $scheduler->is_repeat == 2 ) ? 'selected' : ''}} value="1">{{__("Custom")}} </option>
                                                     </select>
                                                     <div class="input-group-append custom_view_btn  {{($scheduler->is_repeat == 1 || $scheduler->is_repeat == 2 ) ? '' : 'hide'}}">           
                                                        <!-- <label class="col-form-label"></label> -->
                                                            <button type="button" class="btn-sm btn-primary " data-toggle="modal" data-target="#schedule_repeate_modal"><i class="fa fa-eye"></i>
                                                            </button>
                                                      </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" value="{{$custom_repeat}}" name="custom_repeat" id="custom_repeat">
                                            </div>
                                           
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Meeting slot time')}}</label>
                                                    <select class="login_lang_dropdown_list     form-control  borderLeft" required name="time_slot">
                                                        @for($initTime = 5; $initTime <=120; $initTime = $initTime+5 ) 
                                                            <option {{$scheduler->time_slot == $initTime ? 'selected': '' }}  value="{{$initTime}}">{{$initTime}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @method("PUT")
        </form>
        
        <form method="post" name="deleteSchedulerForm" id="deleteSchedulerForm" action="{{route('schedule.destroy',$scheduler->id)}}" >
            @csrf
            @method('DELETE')
        </form>
    </div>
</section>
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        now = moment()
        $(".end_on").first().prop("checked",true);
        $('#from_datepicker').datetimepicker({
            format: 'L'
        });
        $('#from_datepicker').on('dp.change', function(e){ $("#is_repeat").val(0).trigger('change'); $("#repeat_every").val(1).trigger('change') });
        $("#from_date").inputmask("99/99/9999",{"placeholder": "mm/dd/yyyy",
            onincomplete: function() {
                $(this).val('');
        }
        });
        var startTime = $('#start_time').datetimepicker({
            format: 'HH:mm',
            stepping: 10,
        });

        $('#end_time').datetimepicker({
            format: 'HH:mm',
            stepping: 10
        });
        $("#is_repeat").on("change",function(){
           // $(".custom_view_btn").toggleClass("hide");
            if($(this).val() == 1) {
                $(".custom_view_btn").removeClass("hide");
                $("#schedule_repeate_modal").modal("show");
            } else {
                $(".custom_view_btn").addClass("hide");
            }
        }); 
        $("input[name='start_time']").inputmask("99:99",{"placeholder": "HH:MM",
            onincomplete: function() {
                $(this).val('');
        }
        })
        $("input[name='end_time']").inputmask("99:99",{"placeholder": "HH:MM",
            onincomplete: function() {
                $(this).val('');
        }
        })
        $("#repeat_every").on("change",function(){
            if($(this).val() == 2) {
                $(".month-select").removeClass('hide');
                $(".week-select").addClass('hide');
                try{ 
                    var date = $("#from_datepicker").find("input").val();
                } catch(e) {
                    var date = $("#from_date").val();
                }
                console.log("Here"+ date);
                console.log(moment(date,"MM/DD/YYYY"));
                var day = moment(date,"MM/DD/YYYY").format("D");
                console.log(moment(date,"MM/DD/YYYY").format("d"))
                var week = moment(date,"MM/DD/YYYY").weeks() - moment(date,"MM/DD/YYYY").add(0, 'month').startOf('month').weeks() ;
                var dayThree = moment(date,"MM/DD/YYYY").format("ddd");
                var fullDayName = moment(date,"MM/DD/YYYY").format("dddd");
                var displayField = ""; 
                var databaseValue = "";
                var week  = parseInt(day/7);
                var modulo = day%7;
                week = week+((modulo == 0) ? 0 : 1);
                if(week == 1)  {
                    displayField = "First "+fullDayName; 
                    databaseValue = "1_"+dayThree; 
                }
                if(week == 2)  {
                    displayField = "Second "+fullDayName; 
                    databaseValue = "2_"+dayThree; 
                }

                if(week == 3)  {
                    displayField = "Third "+fullDayName; 
                    databaseValue = "3_"+dayThree; 
                }
                if(week == 4)  {
                    displayField = "Fourth "+fullDayName; 
                    databaseValue = "4_"+dayThree; 
                } 

                if(week == 5)  {
                    displayField = "Fifth "+fullDayName; 
                    databaseValue = "5_"+dayThree; 
                }
                var day = moment(date,"MM/DD/YYYY").format("DD");
                var dropDownHtml = "<option value='"+day+"'>Monthly on day "+day+ "</option>"; 
                dropDownHtml += "<option value='"+databaseValue+"'>Monthly on the "+displayField+ "</option>"; 
                console.log(dropDownHtml);
                $("#month_days").html(dropDownHtml);
            } else {
                $(".week-select").removeClass('hide');
                $(".month-select").addClass('hide');
            }
            
        });

        $(".end_on").on("click",function(){
            console.log($(this).val());
            if($(this).val() == 0) {
                var disable1 = $(this).attr("data-disable1");
                var disable = $(this).attr("data-disable");
                $("#"+disable).prop("disabled",true);
                console.log($("#"+disable))
                $("#"+disable1).prop("disabled",true); 
              //  return false;
            } else {
                var enable = $(this).attr("data-enable");
                var disable = $(this).attr("data-disable");
                $("#"+enable).prop('disabled',false);
                $("#"+disable).prop('disabled',true);
            }
        });

        $('#schedule_repeate_modal').on('hide.bs.modal', function (event) {
            // var customRepeat = getFormData();
            // $("#custom_repeat").val(JSON.stringify(customRepeat));
        });

        $(".account_change").on("change",function(){
            var accountId = $(this).val();
            var data = {
                accountId
            };
            $.post("{{route('scheduler.get.operators')}}",data,(response,status)=> {
                $(".operator_list").html(response.html)
            }).fail(function(response) {
                $(".operator_list").html('');
            });
        });
        $("#createScheduler").validate({
              errorClass:'text-danger custom-error'
        });
        jQuery.extend(jQuery.validator.messages, {
            required: "",
        });
        $("#createScheduler").on("submit",function(e){
            e.preventDefault();
            console.log("here");
            if(!$(this).valid()) {
                return false;
            }
            var data = $(this).serialize();
            $('.customLoader').show();
            $.post("{{route('schedule.update',$scheduler->id)}}",data,(response,status)=> {
               $('.customLoader').hide();
               $(".msg_text").html(response.msg);
               $(".ajax_res_msg").removeClass("alert-danger").addClass("alert-success").removeClass("hide");
               // $(".operator_list").html(response.html)
               setTimeout(function(){
                  window.location.href = response.redirect;
               },2000);
               //$("")
            }).fail(function(response) {
                $('.customLoader').hide();
                var msg = response.responseJSON.msg;
                $(".msg_text").html(msg);
                $(".ajax_res_msg").removeClass("alert-success").addClass("alert-danger").removeClass("hide");
                //$(".operator_list").html('');
            });
        });
        setData();

        $(".revert_repeat").on("click",function(){
           // $(".custom_recurrence").trigger("reset");
            //$("select[name='is_repeat']").val(0).trigger("change")
            $("#schedule_repeate_modal").modal("hide");
        });
       
        $('.custom_recurrence').validate({
            rules: {
                repeat_count: {
                    required:true
                }, 
                end_date: {
                    required: ".end_date:checked"
                }, 
                occurrences: {
                    required:".occurrences:checked"
                },
                is_repeat: {
                    required: true
                },
                repeat_on_months: {
                    required: function(){
                        return ($("#repeat_every").val() == 2) ? true: false;
                    }
                }
            }
        });
        jQuery.validator.addClassRules('.weekday', {
            required: function(){
                return ($("#repeat_every").val() == 1) ? true: false;
            }
        });
        $(".save_repeat").on("click",function(){
            if( $('.custom_recurrence').valid())  {
                $("#schedule_repeate_modal").modal("hide");
                var customRepeat = getFormData();
                $("#custom_repeat").val(JSON.stringify(customRepeat));
            }

            console.log( $('.custom_recurrence').valid());
        });
    }); 
    function getFormData($form){
        var unindexed_array = $(".custom_recurrence").serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

    function setData() 
    {
        var customRepeat = $("#custom_repeat").val();
        if(customRepeat.trim() == '') {
            return false;
        } 
        var field = {
            repeat_count: 'text', 
            is_repeat: 'select',
            is_end: 'radio',
            when_end: 'text', 
            repeat_on_months: 'select'
        }
        customRepeat = JSON.parse(customRepeat);
        for (var key of Object.keys(customRepeat)) {
            if(key == 'repeat_on_weeks' && customRepeat['is_repeat'] == 1) {
                var valArr = customRepeat[key].split(",");
                console.log(valArr);
                $.each( valArr, function( inZ, week ){
                    var name = "repeat_on_weeks["+week+"]";
                    $("input[name='"+name+"']").val(week).prop("checked",true); 
                });
               
            } else if(customRepeat[key] != null) {
                console.log(customRepeat[key]);
                console.log("field = "+key);
                    console.log($("select[name="+key+"]"));
                if(field[key] == 'text') {
                    $("input[name="+key+"]").val(customRepeat[key]);
                }else if(field[key] == 'select') {
                    $(".custom_recurrence select[name="+key+"]").val(customRepeat[key]).trigger("change");
                } else if(field[key] == 'select') {
                    console.log($("select[name="+key+"]").val(customRepeat[key]));
                    $("select[name="+key+"]").val(customRepeat[key]).trigger("change");
                }else if(field[key] == 'radio') {
                //    /s console.log($("input[name="+key+"]").val(customRepeat[key]));
                  //  $("input[name="+key+"]").val(customRepeat[key]).prop("checked",true);
                } 
                if(key == 'is_end') {
                    if(customRepeat[key] == 2 ) {
                        $(".occurrences").prop("checked",true).trigger('change');
                        $("#occurrences").val(customRepeat['when_end'])
                        $("#occurrences").prop("disabled",false);
                    } else if(customRepeat[key] == 1) {
                        $(".end_date").prop("checked",true).trigger('change');;
                        $("#end_date").val(customRepeat['when_end']);
                        $("#end_date").prop("disabled",false);
                    } else {
                        $(".nothing").prop("checked",true).trigger('change');;
                    }
                }
            } 
            // if(key == 'is_repeat' && customRepeat['is_repeat'] == 2 ) {
            //     $("select[name="+key+"]").val(1).trigger("change");
            // }
            
        }
        
    }

    function LetMeDeleteSchedule()  {
        $("#deleteSchedulerForm").submit();
    }

</script>
@endsection