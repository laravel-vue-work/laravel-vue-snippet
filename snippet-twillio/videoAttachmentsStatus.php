<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class videoAttachmentsStatus extends Model
{
    use SoftDeletes;

    protected $table = 'video_attachments_status';
    protected $dates = ['deleted_at'];
}
