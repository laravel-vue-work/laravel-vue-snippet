@extends('layouts.header')

@section('style')
<style type="text/css" media="screen">
body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{
    font-family: 'Roboto', sans-serif;
}
    .map {
        position: relative;
    }

    .map .card {
        position: absolute;
        width: calc(100% - 55px);
        box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
        border: none;
        top: 10px;
        left: 30px;
    }

    .map .card .item {
        padding-right: 10px;
        border-right: 1px solid #CFDBE2;
        margin-right: 10px;
    }

    .map .card .item img {
        width: 16px;
        height: auto;
    }

    .map .card .item:last-child {
        border-right: none;
    }
    .large-video-image{

    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    object-fit: initial;
    height: 100%;
    float: left;
    background-color: #000

    }
    .large-video-image-expand{
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        object-fit: contain;
        height: 100%;
        float: left;
        background-color: #000
    }

    .small-video-image,
    .small-video-image-expand{
        position: absolute;
        top: 10px;
        right: 10px;
        z-index: 99;
        height: 100px;
        width: auto;
        border: 1px solid #fff;
        border-radius: 5px;
        margin-right: 10px;

    }
    .map-box{
    position: absolute;
    width: calc(100% - 10px);
    background-color: #fff;
    display: flex;
    top: 2px;
    z-index: 999;
    list-style: none;
    margin: 0;
    padding: 3px;
    left: 5px;
    border: 1px solid #CFDBE2;
    box-sizing: border-box;
    box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
    border-radius: 3px;
    }
    #operatorComment{
        height: 119px;
    }
    .call-screenshot{
        position: absolute;
        z-index: 9;
        height: 100%;
        width: 100%;
    }
    .object-fit-style{
        object-fit:contain;
    }
</style>
@endsection

@section('script')
@if(empty($allowLocation)) 
    <script>
        $("body").addClass("big-video");
    </script>
@endif
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>  
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>   -->
<script src="https://media.twiliocdn.com/sdk/js/video/releases/2.13.1/twilio-video.min.js?".time()></script>   
{{-- <script type="text/javascript" src="{{'https://maps.googleapis.com/maps/api/js?key='.(new \App\Libraries\GeneralSetting)->getApiInfo()['google_maps_api_key']}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5.1/jquery.googlemap.js">
</script> --}}

<link rel="stylesheet" href="{{asset('css/leaflet.css')}}"/>
<script type="text/javascript" src="{{ asset('js/leaflet.js?')}}"></script>
<script type="text/javascript" src="{{ asset('js/ckeditor.js?'.time())}}"></script>

<script>
  
    var tempRoom = '';
    var count = 0;

     function   startVideo() {
        videoTrack =  Twilio.Video.createLocalTracks({
            audio: true,
            video: {  
                // mirrored: true,
                width: { ideal: 1280 },
                height: { ideal: 720 } 
            }
        }).then(function(localTracks) {
            // videoTrack = localTracks.find(track => track.kind === 'video');
            // var divContainer = document.getElementById('small-video');
            // var videoElement = videoTrack.attach();
            // divContainer.appendChild(videoElement);
            return Twilio.Video.connect('{{ $accessToken }}', {
                name: '{{ $roomName }}',
                tracks: localTracks,
                region:"gll",
                video: { 
                    // mirrored: true,
                    width: { ideal: 1280 },
                    height: { ideal: 720 }  
                }
        });
        }).then(function(room) {
            tempRoom = room;
            participantConnected(room.localParticipant,1);
        room.participants.forEach(participantConnected);

        var previewContainer = document.getElementById(room.localParticipant.sid);
        if (!previewContainer || !previewContainer.querySelector('video')) {
           // participantConnected(room.localParticipant);
         }

        room.on('participantConnected', function(participant) {
            participantConnected(participant);
        });

        room.on('participantDisconnected', function(participant) {
            participantDisconnected(participant);
        });
        count++;
        room.on('disconnected', room => {
            room.localParticipant.tracks.forEach(publication => {
                const attachedElements = publication.track.detach();
                    attachedElements.forEach(element => element.remove());
            });
        });
        });
    }
    
    var tempIndex = 0;
    var operatorID = '';
    function participantConnected(participant,isLocal = 0) {
       const div = document.createElement('div');
       div.id = participant.sid;
       appUserIdentity = $("#appUserIdentity").val();


      // div.innerText = participant.identity;
    //   if((count == participant.sid && tempIndex == 0) || (count == 0 && tempIndex == 0)){
    //         div.setAttribute("class", "operator-large-screen");
    //         operatorID = div.id;
    //    }else{
    //        if(tempIndex == 1){
    //             div.setAttribute("class", "user-large-screen");
    //             jQuery('#'+operatorID).removeClass("operator-large-screen");
    //             jQuery('#'+operatorID).detach().appendTo('#small-video');
    //        }else{
    //            var appUserID = div.id;
    //            $('#'+appUserID).addClass('particVideoDiv');
    //        }
    //    }
        
        participant.tracks.forEach(publication => {
            if(isLocal == 1) {
                if(typeof publication.track != null && publication.track != null ) {
                    div.appendChild(publication.track.attach());
                    var video = div.getElementsByTagName("video")[0];   
                }
            } else {
                if(publication.isSubscribed) {
                    trackSubscribed(div, publication.track);
                }
            }
        });
        participant.on('trackSubscribed', track => trackSubscribed(div, track));
        participant.on('trackUnsubscribed', trackUnsubscribed);

    //    participant.on('trackAdded', function(track) {
    //        trackAdded(div, track)
    //    });
    //    participant.on('trackRemoved', trackRemoved);


        if($('#operatorVideoDiv').is(':empty')){
            document.getElementById('operatorVideoDiv').appendChild(div);
            $('#callLoad').show();
        }
        console.log('yesy');
        console.log(participant);
        console.log(appUserIdentity +"=="+ participant.identity)
      //  if(!$('#operatorVideoDiv').is(':empty') ){
           // debugger;
            if(appUserIdentity == participant.identity ){ 
                div.setAttribute("class", "user-large-screen");
                document.getElementById('appUserVideoDiv').appendChild(div);
             }else{ 
              //  if(tempIndex == 1){
                  // document.getElementById('appUserVideoDiv').appendChild(div);
                //}else{
                    document.getElementById('small-video').appendChild(div);    
                //}
             }
            
            $('#callLoad').show();
        //}
        
        /*if($('#small-video').is(':empty')){
            document.getElementById('small-video').appendChild(div);
            $('#callLoad').show();
        }*/

        $('.call_option').show();
       // document.body.appendChild(div);
        tempIndex++;
        
    }
    
    function trackSubscribed(div, track) {
        div.appendChild(track.attach());
        var video = div.getElementsByTagName("video")[0];
        if (video) {
            video.setAttribute("style", "max-width:400;");
        }
    }
    function participantDisconnected(participant) {
        console.log('Participant "%s" disconnected', participant.identity);
        document.getElementById(participant.sid).remove();
    }

    function trackUnsubscribed(track) {
         track.detach().forEach(element => element.remove());
    }

    // function participantDisconnected(participant) {
    //    participant.tracks.forEach(trackRemoved);
    //    document.getElementById(participant.sid).remove();
    // }

    function trackAdded(div, track) {
    //    div.appendChild(track.attach());
    //    var video = div.getElementsByTagName("video")[0];
    //    if (video) {
    //        video.setAttribute("style", "max-width:400px;");
    //    }
    }

    function trackRemoved(track) {
    //   track.detach().forEach( function(element) { element.remove() });
    }

    $('#mute').click(function(){
        //$(this).css({opacity: "0"});
        //$('#unmute').css({opacity: "1"});
        $(this).hide()
        $('#unmute').show();
        try{
        tempRoom.localParticipant.audioTracks.forEach(publication => {
            publication.track.disable();
        });
        incidentLog("operator call muted successfully!","success",0);

        }
        catch(er)
        {
           var error=er;
           console.log(error);
           incidentLog("operator call mute testing",error,1);
        }
    });

    $('#unmute').click(function(){
       // $(this).css({opacity: "0"})
        //$('#mute').css({opacity: "1"});
        $(this).hide()
        $('#mute').show();
        try{
        tempRoom.localParticipant.audioTracks.forEach(publication => {
            publication.track.enable();
        });
        incidentLog("operator call Unmuted successfully!","success",0);

        }
        catch(er)
        {
           var error=er;
           console.log(error);
           incidentLog("operator call Unmute testing",error,1);
        }
    });

    $('#hide_video').click(function(){
        $(this).hide('')
        $('#show_video').show();
        muteVideo();
    });

    $('#show_video').click(function(){
        $(this).hide('')
        $('#hide_video').show();
        unMuteVideo();
    });
    
    $('#bigvideo').click(function(){
        $('body').toggleClass('big-video');
        $('#bigvideo i').toggleClass('fa-expand fa-compress');
    });

    function muteVideo(){
        try{
            tempRoom.localParticipant.videoTracks.forEach(publication => {
                publication.track.disable();
            });
            incidentLog("operator hides video successfully!","success",0);

        }
        catch(er)
        {
            var error=er;
            console.log(error);
            incidentLog("operator hide video",error,1);
        }
    }

    function unMuteVideo(){
        try{
            tempRoom.localParticipant.videoTracks.forEach(publication => {
                publication.track.enable();
            });
            incidentLog("operator video unhide successfully!","success",0);

        }
        catch(er)
        {
            var error=er;
            console.log(error);
            incidentLog("operator unhide video",error,1);
        }
    }
//     window.onbeforeunload = function(){
//   return 'Are you sure you want to leave?';
// };

    $('.endCall').click(function(){
        $('#clickOnEndCall').val('1');
        incidentLog("operator has ended the call","success",0);

        swal({
            title: "{{__('Are you sure you want to end the call?')}}",
            icon: 'warning',
            buttons: {
                cancel: {
                    text: "{{__('No')}}",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true
                },
                confirm: {
                    text: "{{__('Yes')}}",
                    value: true,
                    visible: true,
                    className: "bg-danger",
                    closeModal: false
                }
            }
        }).then(function(isConfirm) {
            if (typeof check_status_interval !== 'undefined') {
                clearInterval(check_status_interval);
            }
            if (typeof app_user_live_location !== 'undefined') {
                clearInterval(app_user_live_location);
            }
            if (isConfirm) {
                try {
                    if($("#operatorComment").val().trim() != '') {
                        $("#saveOperatorComment").trigger("click");
                    }
                } catch(err) {
                    console.log(err)    
                }

                var roomSID = $('#roomSID').val();
                var calDur = $('.countdown').text();
                var operatorId = $('#operatorId').val();
                var endCall = 1;

                $.ajax({
                    type: "GET",
                    url: getsiteurl()+'/get/call_duration',
                    data: {roomSID:roomSID,calDur:calDur,endCall:endCall,operatorId:operatorId},
                    success: function(response) {

                        window.location.replace(getsiteurl()+'/session');
                    }
                });
            }
        });
    });
    $('#copy_link').on('click',function(){
        
incidentLog("operator copied join session URL !","success",0);

// Create an auxiliary hidden input
var aux = document.createElement("input");

// Get the text from the element passed into the input
aux.setAttribute("value",$('#copy_textbox').val());

// Append the aux input to the body
document.body.appendChild(aux);

// Highlight the content
aux.select();

// Execute the copy command
document.execCommand("copy");

// Remove the input from the body
document.body.removeChild(aux);


        
    })
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
 function screenshot(){

        var token =  $('input[name="_token"]').attr('value');
        var roomSID = $('#roomSID').val();
        var appUserId = $('#operatorId').val();
        /*small video canvas*/
        var canvas1 = document.getElementById('canvas1') //
        canvas1.getContext('2d').drawImage($('#small-video video')[0],0,0,120.94,98);
        var img=canvas1.toDataURL();

        document.getElementById("call-screenshot").style.display = "block";
        var classList = $("#toggle-check").attr("class");
       
        var image = new Image();
        image.src=img;
        if(classList=="fas fa-expand")
        {
        image.className="small-video-image";
        }
        else{
        image.className="small-video-image-expand";
        }

        $("#but_screenshot").css('background-color',"#383838");
        document.getElementById('but_screenshot').disabled = true;

        $('#appUserVideoDiv video').width("auto");

        var width=$('#appUserVideoDiv video').width();
        var height=$('#appUserVideoDiv video').height();
        console.log(width);
        console.log(height);
        
        var canvas2 = document.getElementById('canvas2') // declare a canvas element in your html
        canvas2.width=width;
        canvas2.height=height;
        canvas2.getContext('2d').drawImage($('#appUserVideoDiv video')[0],0,0,width,height);
        var img2=canvas2.toDataURL();


        var image2 = new Image();
        image2.src=img2;
        if(classList=="fas fa-expand"){
        image2.className="large-video-image";
        }
        else{
        image2.className="large-video-image-expand";
        }
        

        var base64URL = canvas2.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream');
        topFunction();
         $.ajax({
            url: getsiteurl()+'/save_snap',
            type: 'POST',
            headers: {
                    'X-CSRF-Token': token
            },
            data: {image: base64URL,roomSID:roomSID,appUserId:appUserId},
            success: function(data){
               //console.log('Upload successfully');
              incidentLog("Operator took a snap !","success",0);

            }
         });
        $('#appUserVideoDiv video').width("100%");
       // $('#appUserVideoDiv video').height("100%");
       
        setTimeout( function(){
        $("#but_screenshot").css('background-color',"rgba(121, 121, 121, 0.7)");
        document.getElementById('but_screenshot').disabled = false
    },2000);
        

    }


</script>

<script type="text/Javascript" src="{{asset('js/videocall/room.js?'.time())}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        incidentLog("operator started call","success",0);

    });
</script>
@if(!empty($startTime)) {
    <script >
        $(document).ready(function(){
        incidentLog("operator video call started","success",0);

            startVideo();
            startTimer("{{$callTime}}");
        })
    </script>
@endif

@if(!empty($roomCreationTime)) {
    <script>
        $(document).ready(function(){
            startIdolTimer("{{$callIdolTime}}");
        })
    </script>
@endif
@endsection
 <script>
        $(document).ready(function(){
            //$(".snap").css("background-color", rgba(121, 121, 121, 0.7));
            $(".snap").click(function(){
            $(".snap").css("background-color", "blue");

            
            });

        })
    </script>
    <script type="text/javascript">
        // window.onbeforeunload = function () {
        //     return "Are you sure?";
        // }   

        window.onunload= function () {
        // e.preventDefault();
        incidentLog("operator has left the session page or browser","success",0)
        
        $('#clickOnEndCall').val('1');

        var roomSID = $('#roomSID').val();
        var calDur = $('.countdown').text();
        var operatorId = $('#operatorId').val();
        var endCall = 1;
        var closetab=1;
        $.ajax({
            type: "GET",
            url: getsiteurl()+'/get/call_duration',
            data: {roomSID:roomSID,calDur:calDur,endCall:endCall,operatorId:operatorId,closetab:closetab},
            success: function(response) {

                window.location.replace(getsiteurl()+'/session');

            }
        });
        
        // added the delay otherwise database operation will not work
      for (var i = 0; i < 100000; i++) {
                setTimeout(window.onbeforeunload, 10);

       }
        //return "Are you sure";
    };
    </script>
    

@section('content')
<input type="hidden" value="{{$user_identity}}" id="appUserIdentity">
<input type="hidden" value="{{$startTime}}" id="sessionStartTime">
<input type="hidden" value="{{$roomCreationTime}}" id="roomCreationTime">
<input type="hidden" value="{{$appUserJoin}}" id="appUserJoin">
@if(Auth::user())
<input type="hidden" name="isLogin" id="isLogin" value="1" />
@else
<input type="hidden" name="isLogin" id="isLogin" value="0" />
@endif

<input type="hidden" name="clickOnEndCall" id="clickOnEndCall" value="0" />
<div id="loading-bg" style="opacity: 1;z-index: 9999999;top: 0;left: 0;visibility: visible;"><div class="loading"><div class="effect-1 effects"></div><div class="effect-2 effects"></div><div class="effect-3 effects"></div></div></div>

<div class="section-container" id="section-container">
           

    <div class="multi_blog_section">

        <div class="top">
            <div class="left">
                <h1>{{__('Phone call session')}}</h1>
                <span class="idolTime">
                    {{__('Idol Time')}}: <span class="countdownIdolTime" style="color:white">00:00
                </span>
                <br>
                <span>{{__('Call will be auto disconnected after 5 minutes of idol time.')}}</span>
                </span>
                <span class="sessionTime" style="display: none;">
                    {{__('Session Time')}}: <span class="countdown" style="color:white">00:00</span>
                </span>
            </div>
            <div class="right">
                <div class="button-gruop">
                    <?php
                    /*
                    <button>{{__('Chat')}}<span><img src="{{asset('image/app_user/chat.svg')}}" alt=""></span></button>
                    */
                    ?>
                    <form method="POST" enctype="multipart/form-data" id="upload_video_attach" action="javascript:void(0)" >
                        @csrf
                        <button id="operatorFileUploadBtn" type="button">{{__('Upload file')}}
                                <span><img src="{{asset('image/app_user/file-up.svg')}}" alt=""></span>
                        </button>
                        <input type="file" id="operatorFileUpload" name="operatorFileUpload[]" style="display: none" multiple="multiple"/>
                    </form>
                    <button  id="startNewSession"  data-toggle="modal" data-target="#addParticipantModal">{{__('Add participant')}}<span><img src="{{asset('image/app_user/user-plus.svg')}}" alt=""></span></button>
                    <?php
                    /*
                    <button>{{__('Share Screen')}}<span><img src="{{asset('image/app_user/desktop-solid.svg')}}" alt=""></span></button>
                    */
                    ?>
                    <button class="end_call endCall">{{__('End Call')}}</button>
                    <input type="hidden" name="roomSID" id="roomSID" value="{{$roomSID}}" />
                    <input type="hidden" name="operatorId" id="operatorId" value="{{$customerID}}" />
                    <input type="hidden" value="" id="operatorIdArray" value=""/>
                </div>
            </div>
        </div>
        <div class="blog_section">
            <div class="map-calling">
                @if(empty($allowLocation))
                <div class="top-map room-view ">
                    <ul class="map_list">
                        <li>
                            <div class="man_content-1">
                                <div class="user-name" id="appUserName">{{$appUser->name}}</div>
                                <a class="user-number" href="tel:{{$appUser->number}}" id="callAppUser">
                                    <div style="direction: ltr" id="appUserPhone">{{$appUser->number}}</div>
                                </a>
                            </div>
                        </li>
                        <li >
                            <div class="man">
                                <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                            </div>
                            <div class="man_content">
                                <div id="appUserDeviceType"></div>
                                <div class="more" data-toggle="modal" data-target="#moreDeviceInformation">
                                    {{__('More')}}                                    
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="man_content-1">
                                <div class="call-title">
                                    {{__('Call Url')}}
                                </div>
                                <div class="copy-url">
                                    <input type="button" id="copy_link" name="copy_link" value="{{__('Copy URL')}}">
                                </div>
                                <div class="call-url-container">
                                    <div class="call-url"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endif
                
                <div class="calling" id="calling">
                    <div class="call-screenshot" id="call-screenshot"></div>
                    <!-- <img src="" id="small-image" class="small-video-image-expand">
                    <img src="" id="big-image" class="big-video-image-expand"> -->

                    <div id="call_screen">
                        <canvas id="canvas1" width="120.94" height="98" style="display: none" class="small-video-image" ></canvas>
                        @if(!empty($allowLocation))
                        <div class="referesh" id="bigvideo">
                            <i class="fas fa-expand" id="toggle-check"></i>
                            {{-- <img src="{{asset('image/app_user/2.png')}}" alt=""> --}}
                        </div>
                        @endif
                        <div class="small-video" id="small-video">
                            
                        </div>
                        <!-- Display App User in Big Screen and Operator in Small Screen -->
                        <div id="appUserVideoDiv" class="appUserVideoDiv"> 
                        
                             <canvas id="canvas2" style="display: none"  class="large-video-image" ></canvas>
                         </div>
                        <!-- <div id="operatorVideoDiv" class="operatorVideoDiv"></div> -->
                    </div>
                    <footer id ="myfooter">
                        <div class="call_option" style="display: none">
                            <button class="button-5 snap" id="but_screenshot"  onclick='screenshot();'>
                            <span name="screen_capture">
                                    <img src="{{asset('image/app_user/screen_capture.png')}}" alt="" style="height: 30px;width:30px"  id="snapicon">
                            </span>
                            </button>
                            <button class="button-5">
                                <span name="mute" id="mute">
                                    <img src="{{asset('image/app_user/unmute.svg')}}" alt="" >
                                </span>
                                <span name="unmute" id="unmute" style="display:none">
                                    <img src="{{asset('image/app_user/audio.svg')}}" alt="">
                                </span>
                            </button>
                            <!-- <input type='button' id='but_screenshot' value='Take screenshot' onclick='screenshot();'> --><br/>
                          
    
                            <button class="button-3 endCall">
                                <span><img src="{{asset('image/app_user/call.svg')}}" alt=""></span>
                            </button>
                            
                            <button class="button-4">
                                <span id="hide_video"><img src="{{asset('image/app_user/video-solid.svg')}}" style="height:27px" alt=" "></span>
                                <span style="display:none" id="show_video"><img src="{{asset('image/app_user/video.svg')}}" style="height:27px" alt=""></span>
                            </button>

                           
                    </footer>
                </div>
              
                <div id="map_blog">
                    <div class="map_border" id="map_border">
                    @if(!empty($allowLocation))

                        <div class="top-map room-view ">
                            <ul class="map_list" id="map_list">
                                <li>
                                    <div class="man_content-1">
                                        <div class="user-name" id="appUserName">{{$appUser->name}}</div>
                                        <a class="user-number" href="tel:{{$appUser->number}}"  id="callAppUser">
                                            <div style="direction: ltr" id="appUserPhone">{{$appUser->number}}</div>
                                        </a>
                                    </div>
                                </li>
                                <li >
                                    <div class="man">
                                        <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                    </div>
                                    <div class="man_content">
                                        <div id="appUserDeviceType"></div>
                                        <div class="more" data-toggle="modal" data-target="#moreDeviceInformation">
                                            {{__('More')}}                                    
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="man_content-1">
                                        <div class="call-title">
                                            {{__('Call Url')}}
                                        </div>
                                        <div class="copy-url">
                                            <input type="button" id="copy_link" name="copy_link" value="{{__('Copy URL')}}">
                                        </div>
                                        <div class="call-url-container">
                                            <div class="call-url"></div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endif
                    <div class="map {{empty($allowLocation) ? 'hide' : ''}}" id="map">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="form_blog">
               
                <input type="text" id="copy_textbox" value="{{url('/joinsession/customer/'.$roomSID.'/'.$customerID)}}"
                    style="display:none">
                <input type="hidden" name="roomSID" id="roomSID" value="{{$roomSID}}" />
                <input type="button" class="btn btn-success" id="saveOperatorComment" value="{{__('Save')}}" />

                <div class="">
                    <div id="successComment" class="text text-success" style="font-size: larger;"></div>
                    <div id="errorComment" class="text text-danger"  style="font-size: larger;"></div>
                </div>
                 <div class="form-group">
                   
                    <div class="text text-danger" id="mediaError" style="display: none">User is facing permission issue</div>
                </div>
                <div class="form-group">
                    <label for="status">{{__('Status')}} <span class="text-danger">*</span></label>
                    <select name="status" id="status" class="input-group">
                        @foreach($status as $statusValue)
                        <option value="{{$statusValue->id}}">{{ $statusValue->status_name }}</option>
                        @endforeach
                    </select>
                    <div class="text text-danger" id="statusError"></div>
                </div>

                <div class="form-group operator_command">
                    <h3>{{__('Operator comments')}}</h3>
                    <div class="field_option">
                    <textarea class="form-control" name="operatorComment" id="operatorComment" rows="5" cols="5"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="userName">{{__('User Full name')}} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="userName" id="userName" value="{{$appUser->name}}">
                    <div class="text text-danger" id="userNameError"></div>
                </div>
                <div class="form-group">
                    <label for="userName">{{__('Incoming files')}}</label>
                    <div class="oprator_incoming_file">
                       
                    </div>
                </div>
                <div style="display: none" id="iossteps">
                    <b>{{__('User has Microphone or Camera issue and we are showing him the following steps to fix it.')}}</b></br><p style='margin-right:280px'>{{__('Steps')}}</p><ul><ol style='text-align:left'><li>{{__('Go to Setting App in your mobile.')}}</li><li>{{__('Click on Safari App in the Setting.')}}</li><li>{{__('Click on Camera and Microphone.')}}</li><li>{{__('Click on allow.')}}</li><li>{{__('Reload the webpage or Re-enter the link.')}}</li></ol></ul>
                </div>
                <div style="display: none" id="androidsteps">
                    <b>{{__('User has Microphone or Camera issue and we are showing him the following steps to fix it.')}}</b></br><p style='margin-right:280px'>{{__('Steps')}}</p><ul><ol style='text-align:left'><li>{{__('At the top right, click on 3 dots then go to Settings.')}}</li><li>{{__('Click on Site settings.')}}</li><li>{{__('Click on Camera and Microphone.')}}</li><li>{{__('Turn on Ask before accessing.')}}</li><li>{{__('Reload the page in order to apply changes or Re-enter the link.')}}</li><li>{{__('If the permission is already "Ask Before" - open the "Blocked" list, select admin.jastok.com, press the camera and microphone to Allow.')}}</li></ol></ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal" id="moreDeviceInformation">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title d-block align-center">{{__('Device Info')}}</h4>
            </div>

            <div class="modal-body">
                <div id="deviceMoreInfo">{{__('No Information Found')}}</div>
            </div>
        </div>
    </div>
</div>
@endsection
