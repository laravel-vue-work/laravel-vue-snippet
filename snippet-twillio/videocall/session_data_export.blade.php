@if(count($sessions)>0)
    <table class="table table-striped my-4 w-100">
       <thead>
            <tr>
                <th data-priority="1">{{__('Id')}}</th>
                <th>{{__('Date')}}</th>
                <th>{{__('User')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('User Phone')}}</th>
                <th>{{__('Operator')}}</th>
                <th>{{__('Total Time (Minute)')}}</th>
                <th>{{__('Location')}}</th>
            </tr>
        </thead>

        <tbody>
            @foreach($sessions as $key => $session)
                <tr class="gradeX">
                    <td>{{$session['id']}}</td>
                    <td class="text-capitalize">{{(new \App\Helpers\CommonHelper)->formatDate($session['date'])}}</td>
                    <td class="text-capitalize">{{$session['user']}}</td>
                    <td class="text-capitalize">{{$session['status']}}</td>
                    <td class="text-capitalize">{{($session['user_phone'])}}</td>
                    <td class="text-capitalize">{{$session['Operator']}}</td>
                    <td class="text-capitalize">{{$session['total_time']}}</td>
                    <td class="text-capitalize" style="width:20%;">{{$session['Location']}}</td>
            @endforeach
        </tbody>
    </table>
@else
    <span class="p-4"><h5><center>{{__('No record found')}}!</center></h5></span>
@endif