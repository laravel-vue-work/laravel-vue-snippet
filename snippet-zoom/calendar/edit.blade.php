@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="#">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="#">{{'Session'}}</a></li>
    <li class="breadcrumb-item active"><a href="#">{{'Edit Session'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Edit Session'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{ route('calendar.update') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="calendar_id" value="{{$calendar->id}}">

                                    <div class="form-group">
                                        <label class="col-form-label" for="institute_logo">{{'Course'}}<span class="text text-danger">*</span></label>
                                        <select name="course_id" id="course_id" class="form-control">
                                            <option value="">Select</option>
                                            @if(!empty($course))
                                            @foreach($course as $c)
                                                <option value="{{$c->id}}" @if($calendar->course_id == $c->id){{'selected'}}@endif>
                                                    {{$c->title}}
                                                </option>
                                            @endforeach
                                            @endif
                                        </select>
                                            @if ($errors->has('course_id'))
                                                <span class="text text-danger">
                                                    <strong>{{'The Course field is required.'}}</strong>
                                                </span>
                                            @endif

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Tutor/Native Speaker'}}<span class="text text-danger">*</span></label>
                                       <select name="speaker_id" id="speaker_id" class="form-control">
                                            <option value="">Select</option>
                                            @if(!empty($speaker))
                                            @foreach($speaker as $s)
                                                <option value="{{$s->id}}" @if($calendar->speaker_id == $s->id){{'selected'}}@endif >
                                                    {{$s->name}}
                                                </option>
                                            @endforeach
                                            @endif
                                        </select>
                                            @if ($errors->has('speaker'))
                                                <span class="text text-danger">
                                                    <strong>{{'The Tutor/Native Speaker field is required.'}}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Session Title'}}<span class="text text-danger">*</span></label>
                                      <select name="session_title" id="session_title" class="form-control">
                                            <option value="">Select</option>
                                            @if(!empty($session))
                                            @foreach($session as $s)
                                                <option value="{{$s->id}}" @if($calendar->session_id == $s->id){{'selected'}}@endif >
                                                    {{$s->session_title}}
                                                </option>
                                            @endforeach
                                            @endif
                                        </select>
                                            @if ($errors->has('session_title'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('session_title') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-form-label">{{'Session Number'}}<span class="text text-danger">*</span></label>
                                         <input type="text" name="session_number" class="form-control" id="session_number" placeholder="Session Number" value="" readonly="">
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Session Date'}}<span class="text text-danger">*</span></label>
                                         <div class="custom-file">
                                           <input type="text" class="form-control" placeholder="session date" name="session_date" value="{{date('m/d/Y',strtotime($calendar->session_date))}}" id="session_date">
                                        </div>

                                        @if ($errors->has('session_date'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('session_date') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Slot Title'}}<span class="text text-danger">*</span></label>
                                         <select name="slot_title" id="slot_title" class="form-control">
                                            <option value="">Select</option>
                                            @if(!empty($slot))
                                            @foreach($slot as $s)
                                                <option value="{{$s->id}}"  @if($calendar->slot_id == $s->id){{'selected'}}@endif >
                                                    {{$s->title}}
                                                </option>
                                            @endforeach
                                            @endif
                                        </select>
                                            @if ($errors->has('slot_title'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('slot_title') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Slot Time'}}<span class="text text-danger">*</span></label>
                                          <input type="text" name="slot_time" class="form-control" id="slot_time" placeholder="slot time" value="" readonly="">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{__('Status')}}<span class="text text-danger">*</span></label>
                                         <select name="status" id="status" class="form-control">
                                                <option value="">Select</option>
                                                @foreach(config('constant.status') as $key => $value)

                                                @if(isset($calendar->status) && $calendar->status == $key)
                                                    <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                                 
                                               
                                                @endforeach
                                            </select>
                                            @if ($errors->has('status'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('calendar.index') }}" class="btn btn-warning btn-dark">{{'Back'}}</a>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
@endsection
@section('page_level_js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
@include('admin.calendar.calendarJs')
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
@endsection

