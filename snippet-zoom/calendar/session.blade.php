@if(isset($session))
	<div class="form-group">
        <label for="session_title">{{'Session Title'}}<span class="text text-danger">*</span></label>
       <select name="session_title" id="session_title" class="form-control">
        <option value="">Select</option>
        @if(!empty($session))
        @foreach($session as $s)
            <option value="{{$s->id}}">
                {{$s->session_title}}
            </option>
        @endforeach
        @endif
    </select>
    <label for="session_title" class="error"></label>
        @if ($errors->has('session_title'))
            <span class="helpBlock alert">
                <strong>{{ $errors->first('session_title') }}</strong>
            </span>
        @endif
    </div>
@else
<div class="form-group">
    <label for="session_title">{{'Session Title'}}<span class="text text-danger">*</span></label>
   <select name="session_title" id="session_title" class="form-control">
    <option value="">Select</option>
</select>
<label for="session_title" class="error"></label>
    @if ($errors->has('session_title'))
        <span class="helpBlock alert">
            <strong>{{ $errors->first('session_title') }}</strong>
        </span>
    @endif
</div>
	
@endif