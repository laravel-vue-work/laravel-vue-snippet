@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('notification') }}">{{'Notification'}}</a></li>
    <li class="breadcrumb-item active"><a href="#">{{'Add Notification'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Add Notification'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{ route('saveNotification') }}" enctype="multipart/form-data">
                                    @csrf
                                    

                                    <div class="form-group">
                                        <label class="col-form-label" for="insName">{{'Institutions'}}<span class="text text-danger">*</span></label>
                                        <select name="insName" id="insName" class="form-control">
                                                <option value="">Select</option>
                                                @foreach($institutions as $institution)
                                                    <option value="{{$institution->id}}">
                                                        {{$institution->name}}
                                                    </option> 
                                                @endforeach
                                            </select>
                                            @if ($errors->has('insName'))
                                                <span class="helpBlock alert">
                                                    <strong>{{'The institution name field is required.'}}</strong>
                                                </span>
                                            @endif

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Institute Co-Ordinator Name'}}</label>

                                         <select name="insCoordName" id="insCoordName" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($insCoordinations as $insCoordination)
                                                <option value="{{$insCoordination->id}}">
                                                    {{$insCoordination->name}}
                                                </option> 
                                               
                                            @endforeach
                                        </select>
                                        @if ($errors->has('insCoordName'))
                                            <span class="helpBlock alert">
                                                <strong>{{'The institution Co ordinator name field is required.'}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Course'}}<span class="text text-danger">*</span></label>
                                            
                                        <select name="courseTitle" id="courseTitle" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($courses as $course)
                                                <option value="{{$course->id}}">
                                                    {{$course->title}}
                                                </option> 
                                            @endforeach
                                        </select>
                                        @if ($errors->has('courseTitle'))
                                            <span class="helpBlock alert">
                                                <strong>{{'The Course Name field is required.'}}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">{{'Template'}}<span class="text text-danger">*</span></label>
                                         <select name="templateName" id="templateName" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($templates as $template)
                                                <option value="{{$template->id}}">
                                                    {{$template->template_title}}
                                                </option> 
                                            @endforeach
                                        </select>
                                        @if ($errors->has('templateName'))
                                            <span class="helpBlock alert">
                                                <strong>{{'The Template Name field is required.'}}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>

                                    
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('notification') }}" class="btn btn-dark">{{'Back'}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')
    @include('admin.notification.notificationJs')
@endsection
