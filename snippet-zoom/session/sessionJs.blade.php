<script type="text/javascript">
$(document).ready(function(){
	$('#createFrm').validate({
	   rules: {
	    	course_id: {
	        	required: true
	      	},
	      	session_title: {
	        	required: true
	      	},
	      	
	      	session_number: {
	        	required: true
	      	},
	      	
	      	status: {
	        	required: true
	      	}
	    },messages: {
	    	course_id: {
	        	required: "Please select Course"
	      	},
	      	session_title: {
	        	required: "Please enter session title"
	      	},
	      	
	      	session_number: {
	        	required: "Please enter session number"
	      	},
	      	
	      	status: {
	        	required: "Please Select Status"
	      	}
	    }
  }); 

  $('#searchSessionTitle').select2({
    theme : "bootstrap"
  });
  $('#searchSessionNumber').select2({
    theme : "bootstrap"
  });
  $('#searchCourseTitle').select2({
    theme : "bootstrap"
  });
  $('#searchCourseNumber').select2({
    theme : "bootstrap"
  });  
  $('#searchStatus').select2({
    theme : "bootstrap"
  });  

	$(document).on("change", "#documents", function(){
		if(typeof $('#sessionId').val() == 'undefined'){
			$("#selectedDocuments").empty();
		}

	    for (var i = 0; i < $(this).get(0).files.length; ++i) {
	        $("#selectedDocuments").append('<li class="list-group-item">'+$(this).get(0).files[i].name+'</li>');
	    }
	});

  $('#course_id').select2({
    theme : "bootstrap"
  });	
  $(document).on("click", ".delete", function(){
            var id = $(this).attr('id');

            Swal.fire({
              title: 'Are you sure you want to delete this session?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                  $.ajax({
                   url: getsiteurl()+'/admin/session/delete/'+id,
                   type: 'GET',
                   data: {}, 
                   dataType: "json",
                   success: function(data){
                    if(data.flag == 0){
                         Swal.fire({
                            title: 'You can not delete this session. Dependency Found!',
                            type: 'warning',
                         }).then((result) => {
                            window.location.replace(getsiteurl()+'/admin/session')
                         });
                      }else{
                         Swal.fire({
                            title: 'Session deleted successfully',
                            type: 'Success',
                         }).then((result) => {
                            window.location.replace(getsiteurl()+'/admin/session')
                         });
                      }
                      //window.location.replace(getsiteurl()+'/admin/session');
                   }
                });
                   // window.location.replace(getsiteurl()+'/admin/session/delete/'+id);
                }
            });
        });


  $(".transaction-history").DataTable({
      responsive: true,
      autoWidth: false,
      order: [],
      columnDefs: [{ 
         'orderable': false, 'targets': [5,6]
      }],
      oLanguage: {
         sEmptyTable: "{{__('No data available in table')}}",
         sZeroRecords: "{{__('No records')}}",
         sSearch: '<em class="fas fa-search"></em>',
         sLengthMenu: '_MENU_ {{__("records per page")}}',
         sInfo: '{{_("Showing")}} _START_ {{_("to")}} _END_ {{_("of")}} _TOTAL_ {{_("entries")}}',
         zeroRecords: "{{__('No records')}}",
         infoEmpty: "{{__('No records')}}",
         infoFiltered: '(filtered from MAX total records)',
         oPaginate: {
            sNext: '<em class="fa fa-caret-right"></em>',
            sPrevious: '<em class="fa fa-caret-left"></em>'
         }
      }
   });

});
$(document).on("change", ".status", function(){
      var id = $(this).data('id');
      var _token   = $('meta[name="csrf-token"]').attr('content');
      var statusValue = $(this).prop('checked');
      Swal.fire({
         title: 'Are you sure you want to change status of this session?',
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes'
      }).then((result) => {
         if (result.value) {
            $.ajax({
               url: getsiteurl()+'/admin/updateSessionStatus',
               type: 'POST',
               data: { id: id , _token: _token, statusValue: statusValue}, 
               dataType: "json",
               success: function(data){
                if(data.flag == 0){
                     Swal.fire({
                        title: 'You can not deactivate this session. Dependency Found!',
                        type: 'warning',
                     }).then((result) => {
                        window.location.replace(getsiteurl()+'/admin/session')
                     });
                  }else{
                     Swal.fire({
                        title: 'Session Status successfully Changed!',
                        type: 'Success',
                     }).then((result) => {
                        window.location.replace(getsiteurl()+'/admin/session')
                     });
                  }
                  //window.location.replace(getsiteurl()+'/admin/session');
               }
            });
         }else{
            window.location.replace(getsiteurl()+'/admin/session');
         }
      });
});
$(document).on("click", ".view", function(){
      var id = $(this).attr('id');
      $.ajax({

                url: 'view/session/'+id,
                type: 'GET',
                success: function(oData){
                $('#modal-body').html(oData.html);
                $("#myModal").modal('show');

               },
            });
   });
</script>