<script type="text/javascript">
$(document).ready(function(){
	$('#createFrm').validate({
	    rules: {
	    	facebook: {
	        	url: true
	      	},
	      	twitter: {
	        	url: true
	      	},
	      	instagram: {
	        	url: true
	      	},
	      	linkedin: {
	        	url: true
	      	}
	    },messages: {
	    	facebook: {
	        	url: "Please enter valid facebook url"
	      	},
	      	twitter: {
	        	url: "Please enter valid twitter url"
	      	},
	      	instagram: {
	        	url: "Please enter valid instagram url"
	      	},
	      	linkedin: {
	        	url: "Please enter valid linkedin url"
	      	}
	    }
  	});
});
</script>