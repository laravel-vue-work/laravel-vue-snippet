@extends('admin.final')

@section('title')
    <title>{{'Active Session'}}</title>
@endsection

@section('main_content')
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
            <h3 class="page-title">{{'Active Session'}}</h3>
        </div>
    </div>

    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
            {{ session()->get('success') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
            {{ session()->get('error') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    @if(session()->has('warning'))
        <div class="alert alert-warning alert-dismissible fade show m-0" role="alert">
            {{ session()->get('warning') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
    
    <table class="transaction-history d-none" id="listing">
        <thead>
            <tr>
                <th>{{'No'}}</th>
                <th>{{'Name'}}</th>
                <th>{{'Email'}}</th>
                <th>{{'Nationality'}}</th>
                <th>{{'Status'}}</th>
                <th>{{'Action'}}</th>
            </tr>
        </thead>    
    </table>
@endsection

@section('page_level_js')
    @include('admin.speaker.js')
@endsection
