@extends('admin.layouts.app')

@section('title')
    {{'Add Tutor/Native Speaker'}}
@endsection

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('speaker')}}">{{'Tutor/Native Speaker'}}</a></li>
    <li class="breadcrumb-item active"><a href="#">{{isset($speaker)?'Edit':'Add'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{isset($speaker)?'Edit Tutor/Native Speaker':'Add Tutor/Native Speaker'}}</b>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{route('saveSpeaker')}}" enctype="multipart/form-data">
                                    @csrf
                                     <input type="hidden" name="speakerId" id="speakerId" value="{{isset($speaker)?$speaker->id:''}}">

                                    <div class="form-group">
                                        <label class="col-form-label" for="institute_logo">{{'Name'}}<span class="text text-danger">*</span></label>
                                        
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" autocomplete="off" value="{{isset($speaker)?$speaker->name:old('name')}}" />

                                        @if ($errors->has('name'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Email'}}<span class="text text-danger">*</span></label>

                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" value="{{isset($speaker)?$speaker->email:old('email')}}" />

                                        @if ($errors->has('email'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Country'}}<span class="text text-danger">*</span></label>
                                            
                                        <select name="country" id="country" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($countries as $countrie)
                                                @if(isset($speaker) && $speaker->country_id == $countrie->id)
                                                    <option selected="selected" value="{{$countrie->id}}">
                                                        {{$countrie->name}}
                                                    </option>
                                                @else
                                                    <option value="{{$countrie->id}}" {{ old('country') == $countrie->id ? "selected" : "" }}>
                                                        {{$countrie->name}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('country'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Time Zone'}}<span class="text text-danger">*</span></label>
                                            
                                        <select name="zone" id="zone" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($timezone as $zone)
                                                 @if(isset($speaker) && $speaker->timezone_id == $zone->id)
                                                    <option value="{{$zone->id}}" selected="selected">
                                                        {{$zone->timezone_name}}
                                                    </option>
                                                 @else
                                                    <option value="{{$zone->id}}" {{ old('zone') == $zone->id ? "selected" : "" }}>
                                                        {{$zone->timezone_name}}
                                                    </option>
                                                 @endif

                                            @endforeach
                                        </select>

                                        @if ($errors->has('zone'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('zone') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Languages'}}<span class="text text-danger">*</span></label>
                                            
                                        <select name="languages" id="languages" class="form-control">
                                            <option value="">Select</option>
                                            @foreach(config('constant.languages') as $index => $language)
                                                @if(isset($speaker) && $speaker->language_id == $index)
                                                    <option selected="selected" value="{{$index}}">
                                                        {{$language}}
                                                    </option>
                                                @else
                                                    <option value="{{$index}}" {{ old('languages') == $index ? "selected" : "" }}>
                                                        {{$language}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('languages'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('languages') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Description'}}</label>
                                            
                                        <textarea class="form-control" name="desc" id="desc" rows="5" cols="5">{{isset($speaker)?$speaker->description:old('desc')}}</textarea>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Show In Front'}}<span class="text text-danger">*</span></label>

                                         <select name="showInFront" id="showInFront" class="form-control">
                                            <option value="">Select</option>
                                                @if(isset($speaker))
                                                    @foreach(config('constant.showInFront') as $index => $value)
                                                        @if(isset($speaker) && $speaker->show_in_front == $index)
                                                            <option value="{{$index}}" selected="selected">{{$value}}</option>
                                                        @else
                                                            <option value="{{$index}}">{{$value}}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option value="1">Yes</option>
                                                    <option value="0" selected="selected">No</option>
                                                @endif
                                        </select>
                                        
                                        @if ($errors->has('showInFront'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('showInFront') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Status'}}<span class="text text-danger">*</span></label>
                                        
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            @foreach(config('constant.status') as $key => $value)
                                                @if(isset($speaker) && $speaker->status == $key)
                                                    <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}" {{old('status') == $key?"selected" : "" }}>{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        
                                        @if($errors->has('status'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">
                                            {{'Save'}}
                                        </button>
                                        <a href="{{route('speaker')}}" class="btn btn-warning btn-dark">    {{'Back'}}
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_level_js')
    @include('admin.speaker.js')
@endsection
