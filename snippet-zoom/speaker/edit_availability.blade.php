@extends('admin.layouts.app')

@section('title')
    <title>{{'Edit Availability'}}</title>
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Admin</a></li>
        <li class="breadcrumb-item active">
            <a href="javascript:void(0);">Availability</a>
        </li>
    </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-10">
                @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show m-0 mb-1" role="alert">
                        {{ session()->get('success') }} 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show m-0 mb-1" role="alert">
                        {{ session()->get('error') }} 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header"> 
                       <b>{{'Availability'}}</b>
                    </div>

                    <form method="post" action="{{route('saveAvailability')}}" id="createFrm">
                    @csrf
                    <input type="hidden" name="availabilityId" value="{{$tutAvailability->id}}" />

                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-12">

                                <?php /*
                                <div class="form-row col-md-12">
                                    <div class="form-group col-md-12">
                                        <a href="javascript:void(0);" id="addMoreBreak">
                                            <strong>{{__('Add More')}}</strong>
                                        </a>
                                    </div>
                                </div>

                                <div class="form-row col-md-12">
                                    @foreach($dateData as $key => $value)
                                        <div class="form-group col-md-5">
                                            <label>{{__('Break Start Date')}}<span class="text text-danger">*</span></label>

                                            <div class="input-group">
                                                <input type="text" class="form-control break_start_date" name="break_start_date[]"  autocomplete="off" readonly="readonly" id="{{'brkStartDate'.$key}}" value="{{explode(',',$value)[0]}}" />

                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-5">
                                            <label>{{__('Break End Date')}}<span class="text text-danger">*</span></label>

                                            <div class="input-group">
                                                <input type="text" class="form-control break_end_date" name="break_end_date[]"  autocomplete="off" readonly="readonly" id="{{'brkEndDate'.$key}}" value="{{explode(',',$value)[1]}}" />

                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            @if($key != 0)
                                                <a href="{{route('deleteBreak',$key)}}">
                                                    Delete
                                                </a>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>

                                <div id="moreBreak"></div>

                                <div class="form-row col-md-12">
                                    <div class="form-group col-md-5">
                                        <label>{{__('Hoilday')}}</label>

                                        <div class="input-group">
                                            <input type="text" class="form-control" name="holiday" id="holiday" autocomplete="off" readonly="readonly" value="{{$tutAvailability->holiday}}"/>

                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                */ ?>

                                <div class="form-row col-md-12">
                                    <div class="form-group col-md-12">
                                        <a href="javascript:void(0);" id="addMoreBooking">
                                            <strong>{{__('Add More')}}</strong>
                                        </a>
                                    </div>
                                </div>

                                @if(count($blockData) > 0)
                                    <input type="hidden" name="storedAvai" id="storedAvai" value="{{count($blockData) - 1}}" />
                                @else
                                    <input type="hidden" name="storedAvai" id="storedAvai" value="{{count($blockData)}}" />
                                @endif

                                @foreach($blockData as $blockKey => $blockValue)
                                <div class="form-row col-md-12">
                                    
                                        <div class="col-md-3 col-lg-3">
                                            <div class="form-group">
                                                <label>{{'Time'}}</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control timeBlock" name="start_time_block[]" autocomplete="off" readonly="readonly" id="{{'strTimeBlock'.$blockKey}}" value="{{$blockValue['startTime']}}" />
                                                    
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-lg-3">
                                            <div class="form-group">
                                                <label>{{'Time'}}</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control timeBlock" name="end_time_block[]" autocomplete="off" readonly="readonly" value="{{$blockValue['endTime']}}" id="{{'endTimeBlock'.$blockKey}}" />

                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="weekDays-selector new d-flex">
                                                    @foreach(config('constant.Days') as $key => $weekDays)

                                                    @php $rndId = rand( 10000,99999 ); @endphp

                                                    @if(in_array($key,$blockValue['timedays']))
                                                        <input type="checkbox" name="{{'timedays['.$blockKey.'][]'}}" id="{{$rndId}}" value="{{$key}}" checked="checked" />
                                                    @else
                                                        <input type="checkbox" name="{{'timedays['.$blockKey.'][]'}}" id="{{$rndId}}" value="{{$key}}" />
                                                    @endif
                                                    
                                                    <label for="{{$rndId}}">{{$weekDays}}</label>

                                                    @endforeach
                                               

                                                     @if($blockKey != 0)
                                                    <a href="{{route('deleteBLock',$blockKey)}}" class="delete-btn">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                @endforeach

                                <div id="moreBooking"></div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <input type="submit" class="btn btn-success" value="Save" id="save" />
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_level_css')
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.multidatespicker.css') }}">
    <link href="{{ asset('css/mdtimepicker.css') }}" rel="stylesheet">
    <style type="text/css">
        .weekDays-selector input {
            display: none!important;
        }

        .weekDays-selector input[type=checkbox] + label {
            display: inline-block;
            border-radius: 6px;
            background: #dddddd;
            width: auto;
            margin-right: 3px;
            line-height: 1;
            text-align: center;
            cursor: pointer;
            padding:10px 15px;
        }

        .weekDays-selector input[type=checkbox]:checked + label {
            background: #D83A3A;
            color: #ffffff;
        }
    </style>
@endsection

@section('page_level_js')
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.multidatespicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/mdtimepicker.js') }}"></script>
    @include('admin.speaker.js_add_availability')
@endsection

