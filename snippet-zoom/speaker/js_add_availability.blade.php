<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("focus",".break_start_date",function() {
	  		$(this).datepicker({
	  			dateFormat: 'dd-mm-yy',
	    		autoclose: true,
	    		firstDay: 1
	  		});
	  	});

	  	$(document).on("focus",".break_end_date",function() {
	  		$(this).datepicker({
	  			dateFormat: 'dd-mm-yy',
	    		autoclose: true,
	    		firstDay: 1
	  		});
	  	});

	  	$(document).on("click","#addMoreBreak",function() {
			var length = $('input.break_start_date').length;
			var html   = '';
	    	
	    	html += '<div class="form-row col-md-12 align-items-end">';
		    	html += '<div class="form-group col-md">';
		            html += '<div class="input-group"><input type="text" class="form-control break_start_date" name="break_start_date[]" id="brkStartDate'+length+'" autocomplete="off" readonly="readonly" /><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar"></i></span></div></div>';
		        html += '</div>';

		        html += '<div class="form-group col-md">';
		            html += '<div class="input-group"><input type="text" class="form-control break_end_date" name="break_end_date[]" id="brkEndDate'+length+'" autocomplete="off" readonly="readonly" /><div class="input-group-append"><span class="input-group-text"><i class="far fa-calendar"></i></span></div></div>';
		        html += '</div>';

		        html += '<div class="form-group col-md-auto">';
		            html += '<button class="removeBreak btn btn-danger"><i class="fas fa-times"></i></button>';
		        html += '</div>';
		    html += '</div>';

	        $("#moreBreak").append(html);
	  	});

	  	$(document).on("click",".removeBreak",function() {
			$(this).parent().parent().remove();
	   	});

        $('#holiday').multiDatesPicker({
            dateFormat: "dd-mm-yy"
        });

        var bookingLength = 0;
	    if(typeof $('#storedAvai').val() != 'undefined'){
	    	bookingLength = $('#storedAvai').val();
		}

	    $(document).on("click","#addMoreBooking",function() {
	  		bookingLength++;

	  		$.ajax({
	            type:'GET',
	            url: getsiteurl()+'/admin/add_more/booking',
	            data:{bookingLength:bookingLength},
	            success: (response) => {
	                $('#moreBooking').append(response.html);
	    			initimeBlock();
	            }
	        });
	  	});

	    $(document).on("click",".removeBooking",function() {
	    	var id = $(this).attr('id');
	    	$('#'+id).remove();
	   	});

	    initimeBlock();
	    function initimeBlock(){
   			$('.timeBlock').mdtimepicker();
	    }

	    $(document).on("click","#save",function(e) {
	    	e.preventDefault();
	    	// var bsError = false;
	    	// var beError = false;

	    	// 
	    	// var bsFields = $('input.break_start_date');
		    // for(var i=0;i<bsFields.length;i++){
		    //     if($(bsFields[i]).val() == ''){
		    //         bsError = true;
		    //         break;
		    //     }
		    // }

		    // var beFields = $('input.break_end_date');
		    // for(var i=0;i<beFields.length;i++){
		    //     if($(beFields[i]).val() == ''){
		    //     	beError = true;
		    //         break;
		    //     }
		    // }

		    // if(beError && bsError){
		    	// Swal.fire({
	              	// type: 'error',
	             	// title: 'Please Select Break Start and Break End Date!'
	            // });
		    // }else if(bsError){
			    // Swal.fire({
	              	// type: 'error',
	             	// title: 'Please Select Break Start Date!'
	            // });
		    // }else if(beError){
	            // Swal.fire({
	              	// type: 'error',
	             	// title: 'Please Select Break End Date!'
	            // });
		    // }

		    // 
		    // var invalidDate = false;
		    // for(var i=0;i<=bsFields.length;i++){
		    // 	if($('#brkEndDate'+i).val() < $('#brkStartDate'+i).val()){
		    // 		invalidDate = true;
		    // 		$('#brkStartDate'+i).addClass('is-invalid');
		    // 		$('#brkEndDate'+i).addClass('is-invalid');
		    // 	}else{
		    // 		$('#brkStartDate'+i).removeClass('is-invalid');
		    // 		$('#brkEndDate'+i).removeClass('is-invalid');
		    // 	}
		    // }

		    // if(invalidDate){
		    	// Swal.fire({
	              	// type: 'error',
	             	// title: 'Please Select Valid Break Start and Break End Date!'
	            // });
		    // }

		    // ======== This code is already commented
		    // invalidTimeBlock = false;
	    	// var strTimeBlockFields = $('input.start_time_block');
	    	// for(var i=0;i<=strTimeBlockFields.length;i++){
	    	// 	if($('#endTimeBlock'+i).val() < $('#strTimeBlock'+i).val()){
	    	// 		invalidTimeBlock = true;
		    // 		$('#strTimeBlock'+i).addClass('is-invalid');
		    // 		$('#endTimeBlock'+i).addClass('is-invalid');
		    // 	}else{
		    // 		$('#strTimeBlock'+i).removeClass('is-invalid');
		    // 		$('#endTimeBlock'+i).removeClass('is-invalid');
		    // 	}
	    	// }

	    	// if(invalidTimeBlock){
		    	// Swal.fire({
	              	// type: 'error',
	             	// title: 'Please Select Valid Time!'
	            // });
		    // }
		    // if(!bsError && !beError && !invalidDate && !invalidTimeBlock){
		    // ===================

		    // if(!bsError && !beError && !invalidDate){
		    // 	$('#createFrm').submit();
		    // }
		    
		    $('#createFrm').submit();
	    });
	});
</script>