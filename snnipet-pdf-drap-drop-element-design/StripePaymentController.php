<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\SkipTracePayment;
use App\User;
use App\Subscriptions;
use Response;
use Session;
use Stripe;
use Redirect;

class StripePaymentController extends Controller
{
    public function index($wpUserId = null){
    	// if(is_null($wpUserId)){
    		// echo "Web View";
    	// }else{
    		return view('member.skip_trace.mobile.index')->with('wpUserId',$wpUserId);
    	// }
    }

    public function processPayment(Request $request){
        // $stripe = new \Stripe\StripeClient(
        //   config('constant.STRIPE_SECRET')
        // );

        // Stripe\Stripe::setApiKey(config('constant.STRIPE_SECRET'));
        // Stripe\Customer::createSource(
        //     'cus_JG1xFAZ7XE6y8h',
        //     ['source' => $request->input('stripeToken')]
        // );

        // $cu = Stripe\Customer::retrieve("cus_JG1xFAZ7XE6y8h");
        // $cu->default_source = "card_1IlYuMCHZJSvVmua8248Rdvy";
        // $cu->save();

        // echo "Done";
        // exit;
        
        DB::beginTransaction();

    	try{
			$chargeAmount = $request->input('amount');
			$wpUserId     = $request->input('wpUserId');
			$isMobile     = $request->input('isMobile');
			$purCent      = $chargeAmount;
			$traStatus    = false;
			$stripeCusId  = null;

			$user = User::where('wp_user_id',$wpUserId)->first();
			if(!$user){
				// toastr()->error('User Not Found!');
                return Redirect::back()->with('error', 'User Not Found!');
			}

			$currSub = Subscriptions::getCurrentSub($user);
    		if(!$currSub){
    			// toastr()->error('No any active subscription found!');
                return Redirect::back()->with('error', 'No any active subscription found!');
    		}

    		Stripe\Stripe::setApiKey(config('constant.STRIPE_SECRET'));

    		$stripeCus = Stripe\Customer::all(["email" => $user->email]);
    		if(isset($stripeCus->data) && is_array($stripeCus->data) && isset($stripeCus->data[0])){
    			$stripeCusId = $stripeCus->data[0]->id;
    		}

    		if(is_null($stripeCusId)){
                $newcus = \Stripe\Customer::create([
                    'name'  => $user->name,
                    'email' => $user->email
                ]);

                $stripeCusId = $newcus->id;
    		}

    		// cus_JKs1JXFbDtS3hS
    		// aprilnewuser7@gmail.com
    		Stripe\Customer::createSource(
			    $stripeCusId,
			    ['source' => $request->input('stripeToken')]
			);

	        $result = Stripe\Charge::create ([
				"customer" => $stripeCusId,
				"amount"   => $chargeAmount * 100,
				"currency" => config('constant.defaultCurrencyCode')
	        ]);

	        // $result = Stripe\Charge::create ([
					// "amount"      => $chargeAmount * 100,
					// "currency"    => config('constant.defaultCurrencyCode'),
					// "source"      => $request->input('stripeToken')
	        // ]);

	        if($result){
				$obj                         = new SkipTracePayment();
				$obj->user_id                = $user->id;
				$obj->subscription_id        = $currSub->id;
				$obj->purchase_cent          = $purCent;
				$obj->charge_id              = $result->id;
				$obj->price                  = $chargeAmount;
				$obj->amount                 = $result->amount;
				$obj->amount_captured        = $result->amount_captured;
				$obj->amount_refunded        = $result->amount_refunded;
				$obj->application            = $result->application;
				$obj->application_fee        = $result->application_fee;
				$obj->application_fee_amount = $result->application_fee_amount;
				$obj->balance_transaction    = $result->balance_transaction;
				$obj->created                = $result->created;
				$obj->currency               = $result->currency;
				$obj->customer               = $result->customer;
				$obj->receipt_url            = $result->receipt_url;
				$obj->status                 = $result->status;
				$obj->save();

                $traStatus = ($result->status == 'succeeded')?true:false;
            }

            $message = 'Opps!Something went wrong while process payment.';
            if($traStatus){
	            $message = 'Payment Processed Successfully!';
	            
	            $currSub->purchase_cent+= $purCent; 
	            $currSub->save();
            }

            if($traStatus){
            	DB::commit();
            	// toastr()->success($message);
                return Redirect::back()->with('success', $message);
            }else{
            	DB::rollback();
            	// toastr()->error($message);
                return Redirect::back()->with('error', $message);
            }

    		return Redirect::back();
    	}catch(Stripe\Exception\CardException $e) {
    		DB::rollback();
            // toastr()->error($e->getError()->message);
            // return Redirect::back()->with('error', $e->getError()->message);

            return Redirect::back()->with('error', $e->getHttpStatus().' | '.$e->getError()->type.' | '.$e->getError()->code.' | '.$e->getError()->message);
    	}catch(\Exception $e){
    		DB::rollback();
            // toastr()->error('Opps!Something went wrong while process payment.');
            return Redirect::back()->with('error', 'Opps!Something went wrong while process payment.');
    	}
    }
}
