<div class="receipt_div" id="{{$total_count+1}}">
<div class="card border-success">
					<div class="card-body">
					<div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name <span class="text text-danger">*</span></label>
                                <input type="text" class="form-control allowalphabet" name="name[]" id="name_{{$total_count+1}}" autocomplete="off" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email <span class="text text-danger">*</span></label>
                                <input type="text" class="form-control" name="email[]" id="email_{{$total_count+1}}" autocomplete="off" />
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Permission <span class="text text-danger">*</span></label>
								  <select class="custom-select" name="permission[]" id="permission_{{$total_count+1}}">
								 <option value="">Select Permission</option>
								@foreach(config('constant.docPermission') as $id => $permission)
                                   <!--<option value="{{$id}}" {{($id == 1)?'selected':''}}>{{$permission}}</option>-->
                                   <option value="{{$id}}">{{$permission}}</option>                          
                            @endforeach
                        </select>
                            </div>
                        </div>
						
                    </div>
					</div>
					</div>
					<div class="hover-btn" id="show_{{$total_count+1}}">
     <a href="javascript:" id="delete_recepient_{{$total_count+1}}" class="delete_recepient" data-id="" row_id = "{{$total_count+1}}">
        <i class="fa fa-trash"></i>
     </a>
  </div>
					</div>
					<style type="text/css">
    .form-group .select2-container{
        display: block;
    }
    .form-group .select2-selection--single .select2-selection__rendered{ 
        line-height: 34px !important;
    }
    .form-group .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-results__option[aria-selected],
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        position: relative;
        padding-left: 1rem;
    }

    .select2-container--default .select2-results__option[aria-selected]:before,
    .select2-container--default .select2-selection--single .select2-selection__rendered:before{
        content: '';   
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        line-height: 1;
        font-family: "Font Awesome 5 Pro";
        font-weight: 900;
    }   
    .select2-container--default .select2-results__option[aria-selected]:last-child:before,
    .select2-container--default .select2-selection--single .select2-selection__rendered[title~=Needs]:before{
            content: "\f305";
    }
    .select2-container--default .select2-results__option[aria-selected]:nth-child(2):before,
    .select2-container--default .select2-selection--single .select2-selection__rendered[title~=None]:before{
            content: "\f410";
    }
    .select2-container--default .select2-results__option[aria-selected]:nth-child(3):before,
    .select2-container--default .select2-selection--single .select2-selection__rendered[title~=Receives]:before{
            content: "\f20a";
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 30px !important;
    }

</style>
