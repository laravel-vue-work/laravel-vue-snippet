<div class="modal fade bd-example-modal-lg" id="assignFieldModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                    <h4 class="modal-title">Select People</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
                
                
                

            </div>

            <div class="modal-body">
                <div class="d-flex justify-content-between ">
                    
                     <button type="button" class="btn btn-success float-right add_people_modal"  modal_id="assignFieldModal">
                        Add People
                    </button>        
                </div>
                @if(isset($buyerSellers) && count($buyerSellers) > 0)
                    <div class="row margin-top-table mt-3">
                        
                        <div class="ml-1 mb-1 assFieldModMsg"></div>

                        <table class="table table-striped table-valign-middle" id="assign_table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <!-- <th>Role</th> -->
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($buyerSellers as $buyerSeller)
                                    <tr>
                                        <td>
                                            <input type="radio" name="buySeller" value="{{$buyerSeller->id}}" data-name="{{$buyerSeller->name}}">
                                        </td>
                                        <td>{{$buyerSeller->name}}</td>
                                        <!-- <td>
                                            @if($buyerSeller->role_id == 0)
                                                {{'Buyer'}}
                                            @else
                                                {{'Seller'}}
                                            @endif
                                        </td> -->
                                        <td>{{$buyerSeller->email}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="row">
                        <div class="text-center col-sm-12">
                            {{'No Record Found!'}}
                        </div>
                    </div>
                @endif
            </div>

            <div class="modal-footer">
                @if(isset($buyerSellers) && count($buyerSellers) > 0)
                    <button type="button" class="btn btn-success" id="saveAssignField">Save</button>
                @endif
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" id="assignFieldModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select People</h4>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                 <div class="d-flex justify-content-between ">
                    <button type="button" class="btn btn-success float-right add_people_modal"  modal_id="assignFieldModal2">
                    Add People
                </button>    
                </div>
                @if(isset($buyerSellers) && count($buyerSellers) > 0)
                    <div class="row margin-top-table mt-3">
                        
                        <div class="ml-1 mb-1 assFieldModMsg"></div>

                        <table class="table table-striped table-valign-middle" id="assign_table2">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <!-- <th>Role</th> -->
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($buyerSellers as $buyerSeller)
                                    <tr>
                                        <td>
                                            <input type="radio" name="buySeller" value="{{$buyerSeller->id}}" data-name="{{$buyerSeller->name}}">
                                        </td>
                                        <td>{{$buyerSeller->name}}</td>
                                        <!-- <td>
                                            @if($buyerSeller->role_id == 0)
                                                {{'Buyer'}}
                                            @else
                                                {{'Seller'}}
                                            @endif
                                        </td> -->
                                        <td>{{$buyerSeller->email}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="row">
                        <div class="text-center col-sm-12">
                            {{'No Record Found!'}}
                        </div>
                    </div>
                @endif
            </div>

            <div class="modal-footer">
                @if(isset($buyerSellers) && count($buyerSellers) > 0)
                    <button type="button" class="btn btn-success" id="saveAssignField2">Save</button>
                @endif
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="addPersonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addPerModTitle">Add Person</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="post" action="javascript:void(0)" id="addPersonFrm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="contractId" id="contractId" value="{{$document->contract_id}}" />
                <input type="hidden" name="byuSelId" id="byuSelId" value="" />

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name <span class="text text-danger">*</span></label>
                                <input type="text" class="form-control allowalphabet" name="name" id="name" autocomplete="off" />
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email <span class="text text-danger">*</span></label>
                                <input type="text" class="form-control" name="email" id="email" autocomplete="off" />
                            </div>
                        </div>
						<div class="col-sm-4">
                            <div class="form-group">
                                <label>Permission <span class="text text-danger">*</span></label>
                                	  <select class="custom-select" name="permission" id="permission">
								<option value="">Select Permission</option>
								@foreach(config('constant.docPermission') as $id => $permission)
                                    <option value="{{$id}}" {{($id == 1)?"selected":''}}>{{$permission}}</option>
									
                            @endforeach
							</select>
                        
                            </div>
                        </div>
                    </div>
                    
                 
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success mr-2">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
 