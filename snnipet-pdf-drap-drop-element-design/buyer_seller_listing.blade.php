<table class="table table-striped table-valign-middle">
    <thead>
        <tr>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Country</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if(count($buyerSellers) == 0)
            <tr>
                <td colspan="6" class="text-center">{{'No Data Found'}}</td>
            </tr>
        @else
            @foreach($buyerSellers as $buyerSeller)
                <tr>
                    <td>
                        @if(is_null($buyerSeller->photo))
                            <div>
                                <i class="fas fa-user-circle fa-2x mr-2"></i>
                            </div>
                        @else
                            <div>
                                <img class="rounded-circle" height="15%" width="27px" src="{{asset('buyer_seller/'.$buyerSeller->photo)}}" alt="" />
                            </div>
                        @endif
                    </td>
                    <td>{{$buyerSeller->name}}</td>
                    <td>{{$buyerSeller->email}}</td>
                    <td>{{$buyerSeller->country}}</td>
                    <td>
                        @if($buyerSeller->role_id == 0)
                            {{'Buyer'}}
                        @else
                            {{'Seller'}}
                        @endif
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="text-muted editPerson"  id="{{$buyerSeller->id}}">
                            <i class="fas fa-edit"></i>
                        </a>

                        <a href="javascript:void(0);" class="text-muted deletePerson" id="{{$buyerSeller->id}}">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>