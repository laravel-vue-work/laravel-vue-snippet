@if(!is_null($contract->status))
	<select class="custom-select status" name="status" id="status" data-id="{{$contract->id}}">
		<option value="">Select Status</option>
	  	@foreach($contract->status as $key => $value)
           	@if($contract->contract_status_id == $key)
	    		<option value="{{$key}}" selected="selected">{{$value}}</option>
			@else
	    		<option value="{{$key}}">{{$value}}</option>
			@endif
	  	@endforeach
	</select>
@else
	<select class="custom-select status" name="status" id="status" data-id="{{$contract->id}}">
		<option value="">Select Status</option>
	</select>
@endif