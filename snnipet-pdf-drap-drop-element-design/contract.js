$(document).ready(function() {
    localStorage.removeItem('draft_id')
    
    $('.select2').select2();

    $(document).on("change", "#filter", function(){
        $('#searchFilter').val('');
        var filterType = $('#filter :selected').val();
        var order      = $('#filter :selected').attr('data-order');

        $.ajax({
            type:'GET',
            url: getsiteurl()+'/filter/contract',
            data: {filterType:filterType,order:order},
            success: (response) => {
                $('#renderConLis').empty();
                $('#renderConLis').append(response.result);
            }
        });
    });

    $(document).on("blur", "#searchFilter", function(){
        var searchVal = $(this).val();
        var filterType = $('#filter :selected').val();
        var order      = $('#filter :selected').attr('data-order');

        $.ajax({
            type:'GET',
            url: getsiteurl()+'/search/contract',
            data: {searchVal:searchVal,filterType:filterType,order:order},
            success: (response) => {
                $('#renderConLis').empty();
                $('#renderConLis').append(response.result);
            }
        });
    });

    $(document).on("change", ".type", function(){
        var conTranId = $(this).val();
        var conId     = $(this).attr('data-id');

        if(conTranId == 6){
            $('#saleDiv_'+conId).css({'display':'none'});
        }else{
            $('#saleDiv_'+conId).css({'display':'block'});
        }

        if(conTranId != '' && conId != ''){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/get/contract_status',
                data: {conId:conId,conTranId:conTranId},
                success: (response) => {
                    $('#transStatusErrSucc').css({'display':'block'});
                    $('#transStatusErrSucc').removeClass('text-success');
                    $('#transStatusErrSucc').removeClass('text-danger');

                    if(response.status == 'success'){
                        $('#transStatusErrSucc').addClass('text-success');
                        $('#renderStatus_'+conId).empty();
                        $('#renderStatus_'+conId).append(response.result);
                    }else{
                        $('#transStatusErrSucc').addClass('text-danger');
                    }

                    $('#transStatusErrSucc').text(response.message);
                    setTimeout(function(){
                        $('#transStatusErrSucc').css({'display':'none'});
                    }, 3000);
                }
            });
         }
    });

    $(document).on("change", ".status", function(){
        var conStatId = $(this).val();
        var conId     = $(this).attr('data-id');

        if(conStatId != '' && conId != ''){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/save/contract_status',
                data: {conId:conId,conStatId:conStatId},
                success: (response) => {
                    $('#transStatusErrSucc').css({'display':'block'});
                    $('#transStatusErrSucc').removeClass('text-success');
                    $('#transStatusErrSucc').removeClass('text-danger');

                    if(response.status == 'success'){
                        $('#transStatusErrSucc').addClass('text-success');
                    }else{
                        $('#transStatusErrSucc').addClass('text-danger');
                    }

                    $('#transStatusErrSucc').text(response.message);
                    setTimeout(function(){
                        $('#transStatusErrSucc').css({'display':'none'});
                    }, 3000); 
                }
            });
        }
    });

    $(document).on("change", ".sale", function(){
        var conId = $(this).attr('id');
        var price = $(this).val();

        if(conId != ''){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/save/price',
                data: {conId:conId,price:price},
                success: (response) => {
                    $('#transStatusErrSucc').css({'display':'block'});
                    $('#transStatusErrSucc').removeClass('text-success');
                    $('#transStatusErrSucc').removeClass('text-danger');

                    if(response.status == 'success'){
                        $('#transStatusErrSucc').addClass('text-success');
                    }else{
                        $('#transStatusErrSucc').addClass('text-danger');
                    }

                    $('#transStatusErrSucc').text(response.message);
                    setTimeout(function(){
                        $('#transStatusErrSucc').css({'display':'none'});
                    }, 3000); 
                }
            });
        }
    });

    $(document).on("click", ".deleteCon", function(){
        var cid = $(this).attr('id');

        Swal.fire({
          title: 'Are you sure you want to delete this contract?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                window.location.href = getsiteurl()+'/delete/contracts/'+cid
            }
        });
    });
});
