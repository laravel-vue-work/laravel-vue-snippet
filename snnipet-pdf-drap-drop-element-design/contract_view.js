$(document).ready(function() {
    $('#propertyFrm').validate({
        rules: {
            cname: {
                required: true
            },
            pname: {
                required: true
            },
            add1: {
                required: true
            },
            country: {
                required: true
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            postalCode: {
                required: true
            },
            photo: {
                accept: "image/jpeg,image/jpg,image/bmp,image/svg,image/png"
            }
        },messages: {
            cname: {
                required: "Please Enter Contract Name"
            },
            pname: {
                required: "Please Enter Property Name"
            },
            add1: {
                required: "Please Enter Address Line 1"
            },
            country: {
                required: 'Please Enter Country'
            },
            state: {
                required: 'Please Enter State'
            },
            city: {
                required: 'Please Enter City'
            },
            postalCode: {
                required: 'Please Enter Postal Code'
            },
            photo: {
                accept: "Please select jpeg,jpg,bmp,svg or png image file"
            }
        },errorPlacement: function(error, element) {
            if (element.attr("name") == "photo") {
                 error.insertAfter("#photoValidation");
            }else{
                error.insertAfter(element);
            }
        }
    });

    $('#photo').change(function(){
        previewImage(this,'preview');
    });

    function previewImage(input, previewId){
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+previewId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    } 
});
