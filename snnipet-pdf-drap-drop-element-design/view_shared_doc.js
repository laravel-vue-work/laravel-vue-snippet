$(document).ready(function() {
    var documentId         = $('#documentId').val();
    var isAllSignEmailSend = $('#isAllSignEmailSend').val();
    var contractId         = $('#contractId').val();
    var recepientId        = $('#recepientId').val();
    var buySellId          = $('#buySellId').val();
    var path               = $('#path').val();
    var docName            = $('#docName').val();

    var target      = [];
    var eleOffset   = [];

    var elePanelHeight  = 0;
    var assignSignCount = 0;
    var chooseAdoptSign = false;

    var clickType = '';
    var clickId   = '';

    if(!$('.elePanel').length){
        elePanelHeight = 55; 
    }

    $(document).ajaxStart(function(){
        $("#loader").css("display","flex");
    });

    $(document).ajaxComplete(function(){
        $("#loader").css("display","none");
    });

    if($('#save').length > 0 && $('#isAllSign').val() == 'no'){
        displayModal();
    }else{
        $('.sticky_header').css("display","block");
        $('#holder').css("display","block");
        renderPDF(path, document.getElementById('holder'));
    }

    function displayModal(){
        $('#confModal').modal('show');
    }

    function displayAdoptSignModal(){
        $('#rndSignId').signature('clear');
        var canvasSign = $('#rndSignId'+' > canvas')[0];
        var ctxSign = canvasSign.getContext('2d');
        ctxSign.clearRect(0, 0, canvasSign.width, canvasSign.height);


        $('#rndIniId').signature('clear');
        var canvasIni = $('#rndIniId'+' > canvas')[0];
        var ctxIni = canvasIni.getContext('2d');
        ctxIni.clearRect(0, 0, canvasIni.width, canvasIni.height);

        $('#adoptSignModal').modal('show');
    }

    function hideAdoptSignModal(){
        $('#adoptSignModal').modal('hide');
    }
    
    $(document).on("click", "#continue", function(){
        $('.sticky_header').css("display","block");
        $('#holder').css("display","block");

        $('#confModal').modal('hide');
        renderPDF(path, document.getElementById('holder'));
    });

    var _PDF_HEIGHT  = 0;
    var _HTML_HEIGHT = 0;
    var _ENV_CODE    = '';

    function renderPDF(url, canvasContainer, options) {
        $("#loader").css("display","flex");
        options = options || { scale: 1.5 };
        
        function renderPage(page) {
            var viewport = page.getViewport(options.scale);
            var wrapper = document.createElement("div");
            wrapper.className = "canvas-wrapper";
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var renderContext = {
              canvasContext: ctx,
              viewport: viewport
            };
            
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            canvasContainer.style.width = parseInt(canvas.width)+parseInt(20)+"px"; // set container width same as PDF

            var new_div = document.createElement("div");
            new_div.id = "title_div";
            new_div.setAttribute("data-html2canvas-ignore", true);
            _ENV_CODE = "AnyDeal Envelope ID : "+window.btoa($('#documentId').val()+$('#created_at').val());
            new_div.innerHTML=_ENV_CODE;
            wrapper.appendChild(new_div);
            wrapper.appendChild(canvas);

            _HTML_HEIGHT = _HTML_HEIGHT + canvas.height + 25;
            _PDF_HEIGHT = canvas.height + 25;
            
            canvasContainer.width = viewport.width;
            canvasContainer.appendChild(wrapper);
            
            page.render(renderContext);
        }
    
        function renderPages(pdfDoc) {
            for(var num = 1; num <= pdfDoc.numPages; num++)
                pdfDoc.getPage(num).then(renderPage);
        }

        PDFJS.disableWorker = true;
        PDFJS.getDocument(url).then(renderPages);
        
        renderAssignField();
    }

    function renderAssignField(){
        setTimeout(function(){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/get/coordinates',
                data: {'docId':documentId},
                success: (response) => {
                    if(response.status == 'success' && response.data != null){
                        Object.values(response.data).map(point => {
                            var rndId             = btoa(Math.random()).substring(0,12);
                            var rndSignId         = btoa(Math.random()).substring(0,12);
                            var rndIniId          = btoa(Math.random()).substring(0,12);
                            var rndDragId         = btoa(Math.random()).substring(0,12);
                            var rndSigPnlId       = btoa(Math.random()).substring(0,12);
                            var buyer_seller_name = $('#buyer_seller_name').val();
                            
                            if(point.type == 'add_text'){
                                $("#holder").prepend('<div id="'+rndId+'" style="height: 40px; line-height:15px"></div>');
                                $("#"+rndId).append('<div class="mt-1"><p class="mb-0">'+point.lines+'</p></div>');
                                $("#"+rndId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                            }else if(point.type == 'assign_field_audio'){
                                if(point.path != 'PATH'){
                                    target.push(rndId);
                                    $("#holder").prepend('<div data-html2canvas-ignore class="ui-widget-content" id="'+rndId+'" style="width: auto;height: 40px"><div class="resizeBox float-left" data-html2canvas-ignore id="resize-'+rndId+'"></div></div>');
                                    $("#"+rndId).append('<div class="text-center mt-1 float-left ml-1"><p class="mb-0"> <i class="far fa-play-circle fa-2x text-success cp playAudio" data-recorded-audio-id="'+rndId+'"></i> <audio controls class="d-none" id="recordedAudio-'+rndId+'"> <source src="'+point.path+'" type="audio/wav"></audio></p></div>');
                                    $("#"+rndId).css({top: point.y, left: point.x, position:'absolute'});                                
                                    $("#resize-"+rndId).css({top: point.section_top, left: point.section_left, width:point.section_width, height:point.section_height});          
                                }
                            }else if((point.type == 'sign' || point.type == 'assign_field' || point.type == 'assign_field_initial') && point.isSign == true){
                                if(point.lines == null){
                                    $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                    '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                    '<div><p class="my-sign alpha" style="text-align:center">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                    $("#"+rndDragId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                                    $('#'+rndSignId).html(point.txtSigIni).addClass('named_sign').css({'font-family': point.font});
                                }else{
                                    $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                    '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                    '<div><p class="my-sign" style="text-align:center">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                    $("#"+rndDragId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                                    $('#'+rndSignId).signature();
                                    
                                    if(point.lines.indexOf('lines') == -1){
                                        var txt1 = "<textarea class='text-center' name='my_sign' id='my_sign' style='height:75px; width:100%; font-size:30px; resize: none;' disabled>"+point.lines+"</textarea>"; 
                                        $('#'+rndSignId).html(txt1);
                                    }else{
                                        $('#'+rndSignId).signature('draw', point.lines);
                                    }
                                    
                                    // $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border" style="width: 250px;height: 80px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                    // '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                    // '<div><p class="my-sign" style="text-align:center">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                    // $("#"+rndDragId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                                    // $('#'+rndSignId).signature();
                                    
                                    // if(point.lines.indexOf('lines') == -1){
                                    //     var txt1 = "<textarea class='text-center' name='my_sign' id='my_sign' style='height:75px; width:100%; font-size:30px; resize: none;' disabled>"+point.lines+"</textarea>"; 
                                    //     $('#'+rndSignId).html(txt1);
                                    // }else{
                                    //     $('#'+rndSignId).signature('draw', point.lines);
                                    // }
                                    
                                    $('#'+rndSignId).signature('disable');
                                }
                            }else if(point.type == 'assign_field' || point.type == 'assign_field_initial'){
                                if((point.buyer_seller_id == buySellId) || (point.type != 'assign_field_initial' && point.lines)){
                                    if(point.lines){
                                        target.push(rndDragId);

                                        $("#holder").prepend('<div id="'+rndDragId+'" class="ui-widget-content">'+
                                        '<div class="img-sign_view adoptInitial" data-id="'+rndIniId+'"><img class="resizebleImage" src="'+asset_url+'images/Initials_icon.png" height="75" width = "75">'+
                                        '<div id="'+rndIniId+'"></div></div>'+
                                        '</div>');
                                        
                                        $("#"+rndDragId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                                    }else{ 
                                        target.push(rndDragId);

                                        $("#holder").prepend('<div id="'+rndDragId+'" class="ui-widget-content">'+
                                        '<div class="img-sign_view adoptSignature" data-id="'+rndSignId+'"><img class="resizebleImage" src="'+asset_url+'images/sign_icon.png" height="75" width = "75">'+
                                        '<div id="'+rndSignId+'"></div></div>'+
                                        '</div>');
                                        
                                        $("#"+rndDragId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                                    }
                                    
                                    $('#rndSignId').signature();
                                    $('#signatureStyle').attr('data-uniqueId', point.uniqueId);
                                    $('#rndIniId').signature();

                                    assignSignCount++;
                                }else{
                                    var text = ' SIGN HERE';

                                    if(point.type == 'assign_field_initial'){
                                        text = ' INITIAL HERE';
                                    }
                                    
                                    var str = point.name;
                                    var matches = (str.match(/\b(\w)/g)).join('');

                                    $("#holder").prepend('<div class="ui-widget-content " id="'+rndId+'" style="width: auto;height: 45px"></div>');
                                    $("#"+rndId).append('<div class="text-center mt-1"><h6><b>'+matches.toUpperCase()+text+'</b></h6></div>');
                                    $("#"+rndId).css({top: point.y-elePanelHeight, left: point.x, position:'absolute'});
                                }
                            }else if(point.type == 'owner_name'){
                                $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border" style="width: 250px;height: 80px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                '<div><p class="my-sign alpha" style="text-align:center;margin-top:-4px;">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                $("#"+rndDragId).css({top: point.y, left: point.x, position:'absolute'});
                                $('#'+rndSignId).html(point.lines).addClass('named_sign').css({'font-family': 'alaskan-malamute-regular'});
                            }else if(point.type == 'owner_draw'){
                                $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border" style="width: 250px;height: 80px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                '<div><p class="my-sign" style="text-align:center">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                $("#"+rndDragId).css({top: point.y, left: point.x, position:'absolute'});
                                $('#'+rndSignId).signature();
                                $('#'+rndSignId).signature('draw', point.lines);
                                $('#'+rndSignId).signature('disable'); 
                            } 
                        });

                        if(target.length > 0){
                            $('.sticky_left').css('display','block');
                            $('#nevigate').addClass('blink-btn');

                            $.each(target,function(index,id) {
                                eleOffset.push({
                                    'id': id,
                                    'position': $("#"+id).offset().top
                                });
                            });
                        }

                        if($('#isAllSign').val() == 'yes' && isAllSignEmailSend == 0){
                            downloadDocument();
                        }

                        $(".playAudio").click(function(){
                            var mainElement = $(this);
                            var temp = $(this).attr('data-recorded-audio-id');
                            var audioElement = $("#recordedAudio-"+temp)[0];
                            if(!audioElement.paused){
                                audioElement.pause();
                                mainElement.removeClass('far fa-pause-circle text-danger pause');
                                mainElement.addClass('far fa-play-circle text-danger');
                            }else{
                                audioElement.play();
                                mainElement.removeClass('far fa-play-circle text-danger');
                                mainElement.addClass('far fa-pause-circle text-danger pause');
                                audioListen()
                            }

                            audioElement.addEventListener('ended', function() {
                                mainElement.removeClass('far fa-pause-circle text-danger pause');
                                mainElement.addClass('far fa-play-circle text-success');
                            }, false);

                            audioElement.addEventListener("timeupdate",function(){
                                // mainElement.removeClass('far fa-play-circle');
                                // mainElement.addClass('far fa-pause-circle text-danger pause');
                            });

                            audioElement.addEventListener("paused",function(){
                                mainElement.removeClass('far fa-play-circle');
                                mainElement.addClass('far fa-pause-circle text-danger pause');
                            });
                        });
                    }
                }
            });
        }, 3000);
    }

    $(document).on("click", ".adoptSignature", function(){
        if(chooseAdoptSign == false){
            clickType = 'signature';
            clickId   = $(this).attr('data-id');

            displayAdoptSignModal();
        }else{
            let divId = $(this).attr('data-id');
            
            if($('#sign_type').val() == 'name'){    
                $('#'+divId).parent().find('img').remove();
                $('#'+divId).html($('#signature_text').val()).addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});
            }else{                
                $('#'+divId).parent().find('img').remove();
                $('#'+divId).signature();
                $('#'+divId).signature('draw', $('#rndSignId').signature('toJSON'));
                $('#'+divId).signature('disable');
            }

            // var index = eleOffset.findIndex(x => x.id === $('#'+divId).parent().parent().attr('id'));
            // currNev = (eleOffset.length == index)?0:index+1;
            // $("#nevigate").trigger("click");

            scrollToNextSign();
            checkAllSigns();
        }
    });

    $(document).on("click", ".adoptInitial", function(){
        if(chooseAdoptSign == false){
            clickType = 'initial';
            clickId   = $(this).attr('data-id');

            displayAdoptSignModal();
        }else{
            let divId = $(this).children().last().attr('id');

            if($('#sign_type').val() == 'name'){    
                $('#'+divId).parent().find('img').remove();
                $('#'+divId).html($('#intial_text').val()).addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});
            }else{                
                $('#'+divId).parent().find('img').remove();
                $('#'+divId).signature();
                $('#'+divId).signature('draw', $('#rndIniId').signature('toJSON'));
                $('#'+divId).signature('disable');
            }

            // var index = eleOffset.findIndex(x => x.id === $('#'+divId).parent().parent().attr('id'));
            // currNev = (eleOffset.length == index)?0:index+1;
            // $("#nevigate").trigger("click");
            
            scrollToNextSign();
            checkAllSigns();
        }
    });

    function scrollToNextSign(){
        if(eleOffset.length){
            $.each(eleOffset,function(index,id) {
                if($("#"+eleOffset[index].id + ' img').length){
                    $('html, body').animate({
                        scrollTop: $("#"+eleOffset[index].id).offset().top-80
                    }, 700);
                    return false;
                }
            });
        }
    }

    function checkAllSigns(){
        var isValidSubmit = true;
        if(eleOffset.length){
            $.each(eleOffset,function(index,id) {
                if($("#"+eleOffset[index].id + ' img').length){
                    isValidSubmit = false;
                    return false;
                }
            });
        }

        $("#save").attr("disabled", !isValidSubmit);
        
        if(isValidSubmit){
            $('#nevigate').text('Finished');
            $('#nevigate').prop('disabled', true);

            $('#nevigate').removeClass('blink-btn');
            $('#save').addClass('blink-btn');
        }
    }

    // CODE FOR SET SIGNATURE FROM NAME
    $(document).on("keyup","#signature_text",function(e){
        var signature_text = $(this).val();
        $("#signatureStyle").html(signature_text);
    });
    $(document).on("keyup","#intial_text",function(e){
        var intial_text = $(this).val();
        $("#initialStyle").html(intial_text);
    });

    // STORE SIGNATURE TYPE
    $(document).on("click","#nav-adopt-tab",function(e){
        $("#sign_type").val('name');
    });
    $(document).on("click","#nav-draw-tab",function(e){
        $("#sign_type").val('draw');
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on("click", "#save", function(){
        Swal.fire({
            title:'Are You Sure?',
            text:'You consent to signing this document electronically.',
            type:'warning',
            showCancelButton:true,
            confirmButtonColor:'#3085d6',
            cancelButtonColor:'#d33',
            confirmButtonText:'Yes'
        }).then((result) => {
            if(result.value){
                var formData = new FormData();
                formData.append('documentId',documentId);
                formData.append('recepientId',recepientId);
                formData.append('signType',$('#sign_type').val());

                formData.append('recSignText',$('#signature_text').val());
                formData.append('recIniText',$('#intial_text').val());
                formData.append('recSignCoor',JSON.stringify($('#rndSignId').signature('toJSON')));
                formData.append('recIniCoor',JSON.stringify($('#rndIniId').signature('toJSON')));

                $.ajax({
                    type:'POST',
                    url: getsiteurl()+'/update/documents',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (response) => {
                        if(response.status == 'success'){
                            if(response.allsignDone == 0){
                                Swal.fire({
                                    type: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                            }else if(response.allsignDone == 1){
                                location.reload();
                            }
                        }else{
                            Swal.fire({
                                type: 'error',
                                title: response.message
                            });
                        }
                        
                        setTimeout(function(){ 
                            window.location.href = getsiteurl()+'/success/sign';
                        }, 2000);
                    }
                });
            }

        });
    });

    function downloadDocument(){
        $(window).scrollTop(0);

        // $("div").each(function() {
        //     if(!$(this).hasClass('draggable')){
        //         $(this).removeAttr("data-html2canvas-ignore");
        //     }
        // });

        setTimeout(function(){
            // var HTML_Width = $(".html-content").width();
            // var HTML_Height = $(".html-content").height();
            // var top_left_margin = 17;
            
            // var PDF_Width = HTML_Width+(top_left_margin*2);
            // var PDF_Height = (PDF_Width*1.2)+(top_left_margin*2);

            var HTML_Width = $(".html-content").width();
            var HTML_Height = _HTML_HEIGHT; // $(".html-content").height();
            var top_left_margin = 0;
            
            var PDF_Width = HTML_Width+(top_left_margin*2);
            var PDF_Height = (_PDF_HEIGHT)+(top_left_margin*2);

            //var PDF_Width = HTML_Width+30;
            //var PDF_Height = HTML_Height;

            var canvas_image_width = HTML_Width;
            var canvas_image_height = HTML_Height;
            
            var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
        
            html2canvas($(".html-content")[0],{scale:1.15, x:0,imageTimeout: 0,allowTaint: true,useCORS: true}).then(function(canvas) {
                canvas.getContext('2d');
                  
                var imgData = canvas.toDataURL("image/jpeg", 1.0);
                var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
                pdf.setFontSize(10);
                pdf.text(_ENV_CODE, 15,15);
                
                for (var i = 1; i <= totalPDFPages; i++) { 
                    pdf.addPage(PDF_Width, PDF_Height);
                    pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
                    pdf.text(_ENV_CODE, 15,15);

                }
                
                // pdf.save("HTML-Document.pdf");
                sendDocumentEmail(pdf);
            });

            // // $(".html-content")[0].ownerDocument.defaultView.innerWidth = $(".html-content").width();
            // var HTML_Width          = $(".html-content").width();
            // var HTML_Height         = $(".html-content").height();
            // var top_left_margin     = 0;

            // /*var PDF_Width           = HTML_Width;
            // var PDF_Height          = HTML_Height;
            // var canvas_image_width  = HTML_Width;
            // var canvas_image_height = HTML_Height;

            // var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;*/

            // var PDF_Width = HTML_Width
            // var PDF_Height = (PDF_Width*1.25);

            // var canvas_image_width  = HTML_Width;
            // var canvas_image_height = HTML_Height;

            // var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

            // var pdf;

            // // https://html2canvas.hertzen.com/configuration
            // html2canvas($(".html-content")[0],{scale: 1,imageTimeout: 0,allowTaint: true,useCORS: true}).then(function (canvas) {
            //     var ctx = canvas.getContext('2d');
            //     ctx.imageSmoothingQuality = 'high';

            //     // https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL
            //     var imgData = canvas.toDataURL("image/jpeg", 1.0);
            //     pdf     = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);

            //     pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
                
            //     for(var i = 0; i <= totalPDFPages; i++){ 
            //         pdf.addPage(PDF_Width, PDF_Height);
            //         pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
            //     }
                
            //     pdf.save(docName);
            //     // sendDocumentEmail(pdf);
            // });
        },5000);
    }

    function sendDocumentEmail(pdf){
        $('#emailNotification').css('display','block');

        var formData = new FormData();
        formData.append('contractId',contractId);
        formData.append('documentId',documentId);
        formData.append('docName',docName);
        formData.append('pdfFile',btoa(pdf.output()));

        $.ajax({
            type:'POST',
            url: getsiteurl()+'/all/rec/sign_done',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: (response) => {
                $('#emailNotification').removeClass();
                
                if(response.status == 'success'){
                    $('#emailNotification').addClass('text text-success');
                    $('#emailNotification').text(response.message);
                }else if(response.status == 'error'){
                    // $('#emailNotification').addClass('text text-danger');
                    // $('#emailNotification').text(response.message);
                }

                setTimeout(function(){ 
                    window.location.href = getsiteurl()+'/success/sign';
                }, 2000);
            }
        });
    }

    var currNev = 0;
    // var currentPosition = '';
    $(document).on("click", "#nevigate", function(){
        if(eleOffset.length > 0){
            $('#nevigate').text('Next');

            eleOffset.sort(function(a,b){
                return (a.position < b.position ? -1 : (a.position > b.position ? 1 : 0));
            });
            
            if(!eleOffset[currNev]){
                currNev = 0;
            }

            if($("#"+eleOffset[currNev].id + " img").length || $("#"+eleOffset[currNev].id + " .resizeBox").length){
                $('html, body').animate({
                    scrollTop: $("#"+eleOffset[currNev].id).offset().top-80
                }, 700);
            }

            currNev = currNev + 1;
            if(eleOffset.length == currNev){
                currNev = 0;
            }
        }else{
            $('#nevigate').text('Finished');
            $('#nevigate').prop('disabled', true);

            $('#nevigate').removeClass('blink-btn');
            $('#save').addClass('blink-btn');
        }
        checkAllSigns();
    });

    $(document).on("click", "#clearSign", function(){
        $('#rndSignId').signature('clear');
        var myCanvas = $('#rndSignId'+' > canvas')[0];
        var ctx = myCanvas.getContext('2d');
        ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    });

    $(document).on("click", "#clearIni", function(){
        $('#rndIniId').signature('clear');
        var myCanvas = $('#rndIniId'+' > canvas')[0];
        var ctx = myCanvas.getContext('2d');
        ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    });

    $(document).on("click", "#adoptSave", function(){
        if($(".adopt_tab .active").attr("href") == '#nav-adopt'){
            if($('#signature_text').val() == '' && $('#intial_text').val() == ''){
                $('#adoptError').text('Please enter signature and initial text!');
                return false;
            }else if($('#signature_text').val() == ''){
                $('#adoptError').text('Please enter signature text!');
                return false;
            }else if($('#intial_text').val() == ''){
                $('#adoptError').text('Please enter initial text!');
                return false;
            }else if($("#confirmAdopt").prop('checked') == false){
                $("#confirmAdopt").focus();
                $('#adoptError').text('Please Confirm Terms!');
                return false;
            }
        }else{
            if($('#rndSignId').signature('isEmpty') && $('#rndIniId').signature('isEmpty')){
                $('#adoptError').text('Please draw signature and initial!');
                return false;
            }else if($('#rndSignId').signature('isEmpty')){
                $('#adoptError').text('Please draw signature!');
                return false;
            }else if($('#rndIniId').signature('isEmpty')){
                $('#adoptError').text('Please draw initial!');
                return false;
            }else if($("#confirmAdopt").prop('checked') == false){
                $("#confirmAdopt").focus();
                $('#adoptError').text('Please Confirm Terms!');
                return false;
            }
        }

        $('#adoptError').text('');
        hideAdoptSignModal();
        chooseAdoptSign = true;

        if(clickType == 'initial'){
            $('#'+clickId).parent().find('img').remove();

            if($('#sign_type').val() == 'name'){
                $('#'+clickId).html($('#intial_text').val()).addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});
            }else if($('#sign_type').val() == 'draw'){
                $('#'+clickId).signature();
                $('#'+clickId).signature('draw', $('#rndIniId').signature('toJSON'));
                $('#'+clickId).signature('disable');
            }
        }else if(clickType == 'signature'){
            $('#'+clickId).parent().find('img').remove();

            if($('#sign_type').val() == 'name'){
                $('#'+clickId).html($('#signature_text').val()).addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});
            }else if($('#sign_type').val() == 'draw'){
                $('#'+clickId).signature();
                $('#'+clickId).signature('draw', $('#rndSignId').signature('toJSON'));
                $('#'+clickId).signature('disable');
            }
        }

        var index = eleOffset.findIndex(x => x.id === $('#'+clickId).parent().parent().attr('id'));
        currNev = (eleOffset.length == index)?0:index+1;
        $("#nevigate").trigger("click");
    });

    function audioListen(){
        $.ajax({
            type:'GET',
            url: getsiteurl()+'/audio/listen',
            data: {'recepientId':recepientId},
            success: (response) => {
                // 
            }
        });
    }
});
